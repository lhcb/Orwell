2018-05-26 Orwell v2r12
========================

v2r12 release prepared on the master branch for 2018 data

It is based on Gaudi v29r4, LHCb v44r2p1, Lbcom v22r0p3, Rec v23r2p1, Online v6r15 
<p>
This version is released on `master` branch. It is an update of v2r11 for 2018 data including all changes made for online monitoring so far.
- Merge branch 'update2018-2' into 'master'
    'prepare v2r12'
    See merge request lhcb/Orwell!7
- Merge branch 'update2018' into 'master'  
   'update Orwell for 2018 data taking' 
    See merge request lhcb/Orwell!6


