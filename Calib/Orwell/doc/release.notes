! Package: Orwell
! Package Coordinator : Olivier Deschamps
! Purpose : LHCb online Calibration Farm applications
!----------------------------------------------------------------------------

!=================== Orwell v2r11 2017-03-25 ==========================


! 2016-03-25 - OD : 
	- CMakeLists.txt :  move to Cmake

! 2016-09-09 - OD 
  - add online scripts

! 2016-06-17 - OD
  - Update Calo reconstruction sequence in CaloRecoSeq.opts 
  - add recoOpts/ directory with all the calo processing options


!=================== Orwell v2r10 2015-04-01 ==========================

! 2016-04-01 - OD
  - adapt to change in OMALib (Online >= v5r27)

! 2016-04-01 - OD
  - Prepare for 2016 first data

! 2016-04-01 - OD
  - Added :
    <= (A) => AutoCaloOccupancyAction.opts 
    <= (A) => AutoCaloOccupancyAnalysis.opts 
    <= (A) => AutoOccupancyUpdate.opts 
    <= (A) => CaloOccupancyAnalysisSeq.opts 
    <= (A) => OMAOccupancyAnalysisSeq.opts 
    <= (A) => SetOccupancyAnalysisManager.opts 

! 2015-09-21 - OD
  - add BankSize alg in CaloMoniSeq (BankSize for L0|Physics monitoring data)           


!=================== Orwell v2r9 2015-04-16 ==========================

! 2015-04-16 - OD
  - Prepare for 2015 first data
  - Main new feature :  add CaloOcuppancy and OMACaloOccupancy tasks


!=================== Orwell v2r8 2014-05-19 ==========================

! 2014-05-19 Olivier Deschamps
  - prepare for v2r8 - update options files as in the online version

! 2014-03-18 - Marco Clemencic
 - Added CMake configuration file.

!=================== Orwell v2r7 2013-10-31 ==========================

! 2013-01-14 Olivier Deschamps
  - SQLDDDB.opts : add Reader for 2013 OnlineDB


!=================== Orwell v2r6 2012-04-04 ==========================

! 2011-09-12 Olivier Deschamps
  - Monitor the average energy deposit for few 'candle channels' : trending for Ecal/Hcal/Prs using ParasiticDataMonitor

! 2011-09-12 Olivier Deschamps
  - add new options for new algos in CaloCalib

! 2011-04-24 Olivier Deschamps
  - update scripts

! 2011-04-08 Olivier Deschamps
  - AutoXXAnalysis : adapt to new MonitoringSvc
  - L0CaloSeq.opts : new options from P. Robbe

!=================== Orwell v2r4 2011-03-08 ==========================

! 2011-03-08 Olivier Deschamps
  - update options for v2r4

! 2010-10-16 Olivier Deschamps
  - job/caloMerged.sh : new feature from B. Viaud (daily sampling)

! 2010-10-15 Olivier Deschamps
  - fix OrwellApp.opts

!=================== Orwell v2r3 2010-10-12 ==========================

! 2010-10-12 Olivier Deschamps
  - prepare v2r3

! 2010-10-12 Olivier Deschamps
  - review options

! 2010-01-29 Olivier Deschamps
  - prepare v2r2

! 2009-12-11
  - L0DUMoniSeq    : split L0DU bank monitoring per BXType
  - new monitoring : CaloProtoElectronMonitor (e/p, ...)
  - CaloMoniAlg    : implement calo-side-splitting of histos ('SlitSides' property) 

!========================== Orwell v2r1 2009-12-03 ==========================
! 2009-11-17
  - options : remove MonitorSvc for OMA based analysis offline

! 2009-11-17
  - OrwellApp.opts : link to condDB tag = head_20091112 (incl. Ecal monitoring fix)

! 2009-11-13
  - SetMonManager : change EventSelector.REQ1 setting (exclude Lumi events from monitoring task)

!========================== Orwell v2r0 2009-11-09 ==========================
! 2009-10-27 - Olivier Deschamps
 - prepare for v2r0

! 2009-10-11 - Olivier Deschamps
 - new options : DumpCalibCoeff.opts
 - set VetoMask to 0x100 in SetMonManager.opts and SetCaliManager.opts

! 2009-09-03 - Olivier Deschamps
 - cleaning in AutoLEDAnalysis.opts
 - add missing XcalMonitor.opts/OMACaloSeq.opts
 - cleaning in CaloDAQMon.opts
 - update job/presenter.sh


! 2009-07-31 - Olivier Deschamps
 - update AutoLEDAnalysis.opts 


!========================== Orwell v1r8 2009-02-23 ==========================
 - new release v1r8
 - options/           : 
  - OrwellApp.opts : default condDB tag head-20090112


! 2008-10-20 - Olivier Deschamps
 - CaloRecMoni.opts : fix typo

!========================== Orwell v1r6 2008-10-16 ==========================
 - new release v1r6
 - options/           : remove obsolete options + cleaning
                      : adapt to change in Online > v4r15
 -
 - L0CaloMoniSeq.opts : add profile monitoring (<ADC>.vs.cellid)
 - L0CaloMoniSeq.opts : add options for emulator check 
 - L0CaloMoniSeq.opts : add new monitoring algorithms (CaloMatchSpdPrs & CaloTiming)
 - cmt/requirements   : use GaudiPoolDb v*


!========================== Orwell v1r5 2008-06-13 ==========================
 - new release v1r5
 - minor changes in options


!========================== Orwell v1r4 2008-05-30 ==========================
! 2008-01-31 - Olivier Deschamps
 - new release v1r4
 - re-organize options


!========================== Orwell v1r3 2008-01-31 ==========================
! 2008-01-31 - Olivier Deschamps
 - remove dependency to OnlineTask

! 2008-01-31 - Olivier Deschamps
 - new release v1r3
 - add dependency to LbcomSys
 - review options and scripts 


! 2007-05-15 - Olivier Deschamps
 - new script (job/mdf2mbm.sh) and options (MDF2MBM.opts)
 to emulate online data-stream from MDF file

! 2007-05-15 - Olivier Deschamps
 - requirements : add dependency to OnlineTask
 - job     : new scripts for online usage
 - options : new options for online usage

!========================== Orwell v1r1 2007-05-02 ==========================
! 2007-04-30 - Olivier Deschamps
 - requirements : replace SQLDDDB by DDDB
 - add options to write raw data format

! 2007-04-30 - Marco Cattaneo
 - Adapt requirements to LHCb v22r4/Gaudi v19r3

! 2007-04-18 Olivier Deschamps
 - new options file to run CaloNtpMonitor : OrwellNtp.opts

!========================== Orwell v1r0 2007-03-07 ==========================
! 2007-03-07 - Marco Cattaneo
 - Changed OrwellOnline.opts to just include Orwell.opts and add additional
! 2007-04-11 Olivier Deschamps
   online specific options
 - Modified Orwell.opts to use SQLDDDB instead of XmlDDDB
 - Removed obsolete ApplicationMgr.DLLs options
 - Updated requirements for LHCb v22r2

! 2007-03-03 - Olivier Deschamps
 - add OrwellOnline (Gaucho, MonitorSvc) 
 - to be completed when tested

! 2007-02-28 - Olivier Deschamps
 - complete the sequence (Prs/Spd)

! 2007-02-22 - Olivier Deschamps

 - clean options and add comments
 - comment Hbooking 

! 2007-02-22 - Olivier Deschamps

 - first version of the LHCb online applications for the Calibration Farm
