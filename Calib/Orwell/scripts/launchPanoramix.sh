vers=$1
if test "$1" = ""; then
echo 'No version is required ...'
echo 'Apply default value'
vers='v15r7'
fi
if test "$1" = "default"; then
echo 'Apply default value'
vers='v15r7'
fi
echo "Panoramix version = "${vers}

export User_release_area=/group/calo/sw/cmtuser
export CMTPROJECTPATH=${User_release_area}:${LHCb_release_area}:${LCG_release_area}

setenvPanoramix ${vers}
cd Vis/Panoramix/${vers}/work
source ../cmt/setup.sh ""
export LD_LIBRARY_PATH=/sw/lib/lhcb/ONLINE/ONLINE_v4r0/InstallArea/${CMTCONFIG}/lib:${LD_LIBRARY_PATH}
pwd
python myPanoramix.py -f online -x none -u Hcal_raw.opts&
