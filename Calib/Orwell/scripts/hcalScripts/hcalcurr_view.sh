#!/bin/bash

###export ROOTSYS=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_68/ROOT/5.34.18/x86_64-slc6-gcc48-opt
###export ROOTSYS=/sw/lib/lcg/external/ROOT/5.32.02/x86_64-slc5-gcc43-opt/root
export ROOTSYS=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/ROOT/6.04.02/x86_64-slc6-gcc49-opt
export DIM_DIR=/group/calo/sw/DIM/dim_v19r31
export PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/gcc/4.9.3/x86_64-slc6/bin:${DIM_DIR}/linux:${ROOTSYS}/bin:$PATH
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/gcc/4.9.3/x86_64-slc6/lib64:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/Python/2.7.9.p1/x86_64-slc6-gcc49-opt/lib:${DIM_DIR}/linux:${ROOTSYS}/lib:$LD_LIBRARY_PATH
export CPLUS_INCLUDE_PATH=/home/ygouz/withDb/calodb

cd /group/calo/hcal/currents

./hcalcurr_view $*
