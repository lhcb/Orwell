#!/bin/bash

#. ~dpereima/.bashrc

export ROOTSYS=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/ROOT/6.04.02/x86_64-slc6-gcc49-opt
export PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/gcc/4.9.3/x86_64-slc6/bin:${ROOTSYS}/bin:${PATH}
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/gcc/4.9.3/x86_64-slc6/lib64:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_79/Python/2.7.9.p1/x86_64-slc6-gcc49-opt/lib:${ROOTSYS}/lib:${LD_LIBRARY_PATH}

#export ROOTSYS=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_68/ROOT/5.34.18/x86_64-slc6-gcc48-opt
#export PATH=${ROOTSYS}/bin:$PATH
#export LD_LIBRARY_PATH=${ROOTSYS}/lib:$LD_LIBRARY_PATH

##cd /home/Dmitrii/soft/


cd /group/calo/hcal/HCALCs/hcalcs_view_pro/
./hcalcs_view $*
