

echo ''
echo '    +---------------------------------+'
echo '    | Calo saveset(s) analysis script |'
echo '    +---------------------------------+'
echo ''

if [ -d /group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn} ]; then
    export OrwellPath="/group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn}"
else
    export OrwellPath="/group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/"
fi


if [ "$1" = "fill" ]; then
 
    export task="CaloDAQCalib"
    export opt="myAnalysis"
    export how="run.list"

else
    read -p  " o Monitoring task ? (CaloDAQCalib, CaloDAQMon, ....)   [CaloDAQCalib] : " -e task 
    if [  "$task" = "" ]; then
	export task="CaloDAQCalib"
    fi
    
    read -p  " o Options file ?  [default] : " -e opt
    if [  "$opt" = "" ]; then
	export opt="def"
    fi
    
    read -p  " o Select savesets by ? (run , run.list, date , name, last)   [run] : " -e how 
    if [  "$how" = "" ]; then
	export how="run"
    fi
fi

count=0
tag=""
export files=""

#---- merging saveset by date
if [ "$how" == "date" ]; then
    read -p  "   - From when ? (format yyyy/mm/dd-H:M)   ["`date +%Y/%m/%d-%H:00`"] : " -e from 
    if [  "$from" = "" ]; then
	export from=`date +%Y/%m/%d-%H:00`
    fi
    
    read -p  "   - To when ?  (format yyyy/mm/dd-H:M)   ["`date +%Y/%m/%d-%H:%M`"] : " -e to
    if [  "$to" = "" ]; then
	export to=`date +%Y/%m/%d-%H:%M`
    fi

        
    export y1=`echo $from | awk -F / '{print $1}' | sed 's/^0//'`
    export m1=`echo $from | awk -F / '{print $2}' | sed 's/^0//'`
    export d1=`echo $from | awk -F / '{print $3}' | awk -F - '{print $1}' | sed 's/^0//'`
    export H1=`echo $from | awk -F - '{print $2}' | awk -F : '{print $1}' | sed 's/^0//'`
    export M1=`echo $from | awk -F - '{print $2}' | awk -F : '{print $2}' `
    
    export y2=`echo $to | awk -F / '{print $1}' | sed 's/^0//'`
    export m2=`echo $to | awk -F / '{print $2}' | sed 's/^0//'`
    export d2=`echo $to | awk -F / '{print $3}' | awk -F - '{print $1}' | sed 's/^0//'`
    export H2=`echo $to | awk -F - '{print $2}' | awk -F : '{print $1}' | sed 's/^0//'`
    export M2=`echo $to | awk -F - '{print $2}' | awk -F : '{print $2}' `

    fspl1=""
    fspl2=""
    read -p "   - Do you need a long trending (one/two samples a day) (y/n)  [n]? " -e long
    if [ $long == "y" ]; then

	read -p  "       At what time should be the 1st sample ? (format H:M)   ["`date +%H:%M`"] : " -e spltime1
	if [  "$spltime1" == "" ]; then
	    export spltime1=`date +%H:%M`
	fi
	
	export Hspl1=`echo $spltime1 | awk -F : '{print $1}' | sed 's/^0//'`
	export Mspl1=`echo $spltime1 | awk -F : '{print $2}' `

	fspl1="$Hspl1$Mspl1"01

	read -p  "       Want a second daily sample  (y/n)   [n] ? " -e long2
	if [ "$long2" == "y" ]; then
	    
	    read -p  "         At what time ? (format H:M)   ["`date +%H:%M`"] : " -e spltime2
	    if [  "$spltime2" == "" ]; then
		export spltime2=`date +%H:%M`
	    fi
	    
	    export Hspl2=`echo $spltime2 | awk -F : '{print $1}' | sed 's/^0//'`
	    export Mspl2=`echo $spltime2 | awk -F : '{print $2}' `
	    
	    fspl2="$Hspl2$Mspl2"01
	  
	else
	    fspl2=$fspl1
	fi
	
	
    fi
    
    if  [ -z "$d1" -o -z "$d2" -o -z "$H1" -o -z "$H2"  -o -z "$M1"  -o -z "$M2" ]; then
	echo invalid format
	return
    fi

    f1="$H1$M1"00 
    f2="$H2$M2"59
    
    echo  "... scanning  savesets ..."
    for (( y = $y1 ; y<= $y2 ; y++ ))
      do

      mm1=$m1
      dd1=$d1
      mm2=$m2
      dd2=$d2
      if [ $y -gt $y1 ];then
          mm1=1;
          dm1=1;
      fi
      if [ $y -lt $y2 ];then
          mm2=12;
          dd2=31;
      fi

      for (( m = $mm1 ; m<= $mm2 ; m++ ))
	do
        dd1=$d1
        dd2=$d2
        if [ $m -gt $mm1 ];then
            d1=1;
        fi
        if [ $m -lt $mm2 ];then
            d2=31;
        fi
	for (( d = $dd1 ; d<= $dd2 ; d++ ))
	  do
	  
	  mm=$m
	  dd=$d
	  if  [ $m  -lt 10 ] ; then
	      mm=0$m
	  fi
	  if  [ $d  -lt 10 ] ; then
	      dd=0$d
	  fi      


          ff1=$f1
          ff2=$f2
          if [ $d -gt $dd1 ];then
              ff1="000000"
          fi
          if [ $d -lt $dd2 ];then
              ff2="235959"
          fi
          if [ -d /hist/Savesets/$y/LHCb/$task/$mm/$dd ]; then
              for  f in `ls /hist/Savesets/$y/LHCb/$task/$mm/$dd/$task*-$y$mm$dd* ` 
                do
                if [ $count -gt 1000 ]; then
                    break
                fi
		
                flag=`echo $f | awk -F "$y$mm$dd"T '{print $2}' | awk -F .root '{print $1}' | awk -F -EOR '{print $1}' | sed 's/^0//'`
		
		if [ $flag -le $ff2 -a $flag -ge $ff1 ]; then
		    if [ $long == "y" ]; then
		    
			if [ $flag == $fspl1 -o $flag == $fspl2 ]; then
			    echo will analyse file  $f
			    files=$files" "$f
			    count=`expr $count + 1`
			fi

		    else
			echo current file  $f
			files=$files" "$f
			count=`expr $count + 1`
		    fi
		    
                fi
		
	       
              done
	  fi
	done
      done
    done
    flag="for the period [$from , $to]" 
fi


#---- merging saveset by run
if [ "$how" == "run.list" ]; then
    export files=""

    if [ "$1" = "fill" ]; then
	export input=run.list.fill-$2
    else
	read -p  "    - Which input file  ? [run.list] : " -e input
	if [  "$input" = "" ]; then
	    export input="run.list"
	fi
    fi

    for id in `more $input`; do
	f=`ls -r /hist/Savesets/ByRun/$task/*/*/*$id*`
	if [ -f $f ]; then
	    if [ "$f" != "" ]; then
		if [ "$count" = "0" ]; then
		    export from=$id
		fi
		count=` expr $count + 1 `
		files=$files" "$f
		export to=$id
	    fi
	fi
    done
    echo $from
    echo $to
    echo $files
fi

if [ "$how" == "run" ]; then

    temp=`ls -tr1 /hist/Savesets/ByRun/$task | tail -1`
    last=`ls -tr1 /hist/Savesets/ByRun/$task/$temp/* | tail -1 | awk -F run '{print $2}' |  awk -F .root '{print $1}'`


    read -p  "  - From run number ? [last run : $last] : " -e from
    if [  "$from" = "" ]; then
	export from=$last
    fi

    read -p  "  - To run number ? [last run : $last] : " -e to
    if [  "$to" = "" ]; then
	export to=$last
    fi
    
    echo  "... scanning  savesets ..."

    for d in `ls -r /hist/Savesets/ByRun/$task/`
    do
      for  f in `ls -r /hist/Savesets/ByRun/$task/$d/*/*`
        do
        if [ $count -gt 1000 ]; then
            break
        fi
        flag=`echo $f | awk -F run '{print $2}' |  awk -F .root '{print $1}'`
        if [ $flag -lt $from ]; then
            break
        fi
        if [ $flag -le $to -a $flag -ge $from ]; then
            files=$files" "$f
            count=`expr $count + 1`            
        fi
      done
    done
    flag="for the run interval [$from , $to]"   

# revert files order for trending to be in the right order
temp=`echo $files | awk '{n=split($0,A);S=A[n];{for(i=n-1;i>0;i--)S=S" "A[i]}}END{print S}'`
files=$temp
unset temp 
fi




#---- merging last savesets
if [ "$how" == "last" ]; then
    read -p  "  - How many recent saveset(s) ? [5] : " -e num
    if [  "$num" = "" ]; then
	export num=5
    fi
    if [ $count -gt 1000 ]; then
        echo "unreasonable number of saveset : set to 1000"
        num=1000
    fi

    export dir=`ls -trd1  /hist/Savesets/20*/LHCb/$task/*/* | tail -1` 
    files=`ls -tr "$dir"/* | tail -$num`
    count=$num
    flag="($num most recent)"   
fi

#---- merging saveset by timestamp name
if [ "$how" == "name" ]; then

    export dir=`ls -trd1  /hist/Savesets/20*/LHCb/$task/*/* | tail -1` 
    last=`ls -tr "$dir" | tail -1 | awk -F $task- '{print $2}' | awk -F .root '{print $1}' | awk -F -EOR '{print $1}' `

    read -p  "  - Saveset name contains ? [last saveset stamp : $last] : " -e name
    if [  "$name" = "" ]; then
	export name=$last
    fi

    files=`ls -tr1 "$dir"/* | grep -i "$name"`
    count=`ls -tr1 "$dir" | grep -i "$name" | wc -l`
    flag=" (stamp=$name)"    
fi

echo "  ... found $count saveset(s) with this criteria"

if [ $count == 0 ];then 
echo "  ... nothing to be processed - stop there "
return 
fi 

# Merging the saveset

export input=$task-merge-00000000T000000
if [ "$how" == "run" -o "$how" == "run.list" ]; then
    export input=$task-merge-$from-$to
fi
if [ "$how" == "date" ]; then
    export input=$task-merge-$y1$m1$d1$H1$M1-$y2$m2$d2$H2$M2
fi
if [ "$how" == "name" ]; then
    export input=$task-merge-$name
fi
if [ "$how" == "last" ]; then
                export input=$task-merge-$last$num
fi


if [ ! -d /tmp/$USER ]; then
    mkdir /tmp/$USER
fi

if [ $count == 1 ]; then
    rm -f /tmp/$USER/$input.root
    cp $files /tmp/$USER/$input.root
    merge="y"
else

    if [ "$1" = "fill" ]; then
	export merge="y"
    else

	read -p "   - Merge the $count savesets ? (y/n)  [y] : " -e merge
	if [  "$merge" = "" ]; then
	    export merge="y"
	fi
    fi
    if [ "$merge" == "y" ];then
        echo "  ... merging $count $task saveset(s) $flag   ... be patient  ..."
        rm -f /tmp/$USER/$input.root
        hadd /tmp/$USER/$input.root $files > /dev/null
    fi
fi

if [ "$task"="CaloDAQCalib" -o "$task"="CaloDAQMon" ]; then
    
    if [ "$1" = "fill" ]; then
	export ana="y"
    else
	read -p  " o Launch the automatic analysis ? (y/n)   [y] : " -e ana
    fi
    if [  "$ana" = "" ]; then
        export ana="y"
    fi
    
    if [ "$ana" == "y" ]; then       
	if [ "$opt" == "def" ]; then 
	    option="OMACaloLED"
	    out=CaloCalibAnalysis
	else
	    option=$opt
	    out=$opt
	fi
	data=""
        if [ "$task" == "CaloDAQMon" ]; then
	    if [ "$opt" == "def" ]; then
		option=OMACaloData
		out=CaloDataAnalysis
	    else
		option=$opt
		out=$opt
	    fi
	    data=Data
        fi

        if [ "$merge" == "y" ];then
	    if [ "$1" = "fill" ]; then
		export meth="both"
	    else
		read -p  "  - Analyze merged saveset (merge), each saveset (trend) or both  ? (merged/trend/both)   [merged] : " -e meth
	    fi
            if [  "$meth" = "" ]; then
                export meth="merged"
            fi
        else
            meth="trend"
        fi


	if [  "$meth" == "trend"  -o  "$meth" == "both" ]; then
            export go="y"
	    if [ $count -gt 1 ]; then 
                if [ $count -gt 15 ]; then 
                    echo "  ... warning : analyzing every saveset ("$count") can be time consuming"
		    if [ "$1" = "fill" ]; then
			export go="y"
		    else
			read -p  "  - perform trending analysis anyway ? (y/n)   [y] : " -e go
			if [  "$go" = "" ]; then
			    export go="y"
			fi
		    fi
		fi
	    else
		echo "   ... warning : merging and chaining is identical when only 1 saveset is selected - will run only merge analysis"
		export go="n"
	    fi
	fi
        ana1="n"
	if [ "$meth" == "merged"  -o  "$meth" == "both" ]; then
	    echo "  ... analyzing merged saveset"
            ana1="y"
	    rm -f file.opts
	    echo 'Ecal'$data'Analysis.InputFiles = {"/tmp/'$USER'/'$input'.root"};' > /tmp/file.opts
	    echo 'Hcal'$dat'Analysis.InputFiles = {"/tmp/'$USER'/'$input'.root"};' >> /tmp/file.opts
	    echo 'Prs'$data'Analysis.InputFiles = {"/tmp/'$USER'/'$input'.root"};'  >> /tmp/file.opts
	    echo 'Spd'$data'Analysis.InputFiles = {"/tmp/'$USER'/'$input'.root"};'  >> /tmp/file.opts    
	    export out1=$out-merge-00000000T000000.root
	    if [ "$how" == "run" -o "$how" == "run.list" ]; then
                export out1=$out-merge-$from-$to.root
            fi
            if [ "$how" == "date" ]; then
                export out1=$out-merge-$y1$m1$d1$H1$M1-$y2$m2$d2$H2$M2.root
            fi
            if [ "$how" == "name" ]; then
                export out1=$out-merge-$name.root
            fi
            if [ "$how" == "last" ]; then
                export out1=$out-merge-$last$num.root
            fi


	    echo 'HistogramPersistencySvc.OutputFile = "'$out1'";' >> /tmp/file.opts
	    source /group/calo/sw/scripts/setOrwell.sh
	    cat $OrwellPath/options/$option.opts /tmp/file.opts > /tmp/myOpts.opts    
	    $OrwellPath/$CMTCONFIG/Orwell.exe /tmp/myOpts.opts
        fi

	if [  "$meth" == "trend"  -o  "$meth" == "both" ]; then

	    c=0
	    if [ "$go" == "y" ]; then
		echo "  ... analyzing savesets chain"
		    chain=""
		    for f in $files 
		      do
		      c=`expr $c + 1`
		      chain=$chain'"'$f'"' 
		      
		      if [ $c -lt $count ]; then
			  chain=$chain','
		      fi
		    done

		    echo 'Ecal'$data'Analysis.InputFiles = {'$chain'};' > /tmp/files.opts 
		    echo 'Hcal'$data'Analysis.InputFiles = {'$chain'};' >> /tmp/files.opts
		    echo 'Prs'$data'Analysis.InputFiles = {'$chain'};'  >> /tmp/files.opts
		    echo 'Spd'$data'Analysis.InputFiles = {'$chain'};'  >> /tmp/files.opts    
		    export out2=$out-chain-00000000T000000.root
		    if [ "$how" == "run" -o "$how" == "run.list" ]; then
                        export out2=$out-chain-$from-$to.root
                    fi
                    if [ "$how" == "date" ]; then
                        export out2=$out-chain-$y1$m1$d1$H1$M1-$y2$m2$d2$H2$M2.root
                    fi
                    if [ "$how" == "name" ]; then
                        export out2=$out-chain-$name.root
                    fi
                    if [ "$how" == "last" ]; then
                        export out2=$out-chain-$last$num.root
                    fi



		    echo 'HistogramPersistencySvc.OutputFile = "'$out2'";' >> /tmp/files.opts
		    source /group/calo/sw/scripts/setOrwell.sh
		    cat $OrwellPath/options/$option.opts /tmp/files.opts > /tmp/myOpts.opts    
		    $OrwellPath/$CMTCONFIG/Orwell.exe /tmp/myOpts.opts
            else
                if [ "$ana1" == "n" ]; then
                    ana="n"
                fi
            fi

	fi
	
    fi
fi

echo ""
echo "----- processing completed ------ "
if [ "$merge" == "y" ]; then
    echo "  -- merged saveset is /tmp/$USER/$input.root"
fi
if [ $out1 ]; then
    echo "  -- merged analysis output is $PWD/$out1" 
fi
if [ $out2 ]; then
    echo "  -- chained analysis output is $PWD/$out2" 
fi
echo "--------------------------------- "
echo ""

if  [ "$ana" == "y" ]; then
    read -p  " o Open presenter in history mode ? (y/n)   [y] : " -e pres
    if [  "$pres" = "" ]; then
        export pres="y"
    fi
    
    
    if [ "$pres" == "y" ]; then
        
 
	if [ $out1 -a $out2 ]; then	   
	    read -p  "   - Open default saveset $out1 (merge) or $out2 (trend) ?   [merged] : " -e set
	    if [  "$set" = "" ]; then
		export set="merged"
	    fi

	    if [ "set" == "merged" ]; then
		export oset=$out1	    
	    elif [ "set" == "trend" ]; then
		export oset=$out2
	    else
		echo "   ... non valid choice : will open $out1"
		export oset=$out1 
	    fi


	else
	    if [ $out1 ]; then
		export oset=$out1
	    fi
	    if [ $out2 ]; then
		export oset=$out2
	    fi
	fi


  
        echo " "
#        echo "   -> In the presenter tool-bar choose 'History using : preset file' and reload the selected page"
        echo "   -> If you want to change the saveset: "
	echo "   -> in the presenter tool-bar choose 'History using : set file' to select the saveset to be displayed"
        echo "   -> Warning : the page comments are not updated"
        echo " "

	if [ "$task" == "CaloDAQCalib" ]; then
	    export opage="/Calorimeters/Analysis/CalibrationData/AnalysisSummary" 
	fi
	if [ "$task" == "CaloDAQMon" ]; then
	    export opage="/Calorimeters/Analysis/MonitoringData/AnalysisSummary"
	fi

        /group/calo/sw/scripts/presenter.sh -M history -S $PWD -l read-only --saveset-file=$oset --startup-page=$opage



        
    fi

fi

unset out1
unset out2
unset go
rm -f /tmp/myOpts.opts > /dev/null
rm -f /tmp/file.opts    > /dev/null
rm -f /tmp/files.opts    > /dev/null

