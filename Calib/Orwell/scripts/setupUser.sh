#export LHCBHOME=/sw/lib
export LHCBHOME=/cvmfs/lhcb.cern.ch/lib/
export LHCBRELEASES=${LHCBHOME}/lhcb
export User_release_area=${HOME}/cmtuser
export Calo_release_area=/group/calo/sw/cmtuser
export CMTPROJECTPATH=${User_release_area}:${Calo_release_area}:${LHCb_release_area}:${LCG_release_area}

echo "Calo_release_area is set to: " ${Calo_release_area}
echo "User_release_area is set to: " ${User_release_area}
echo "CMTPROJECTPATH is set to   : " ${CMTPROJECTPATH}


source /group/calo/sw/scripts/setup.sh

source /group/calo/sw/scripts/localOrwell.sh ${OrwellVsn} 

#unset OrwellVsn 

cd cmt

echo "setting up Orwell application ... wait"
if [ -f ./setupOrwell.vars ]; then
source setupOrwell.vars
else
source ./setup.sh
fi

cd ../options/ 

alias OrwellRun=$ORWELLROOT/$CMTCONFIG/Orwell.exe

echo "... ready to play !" 
