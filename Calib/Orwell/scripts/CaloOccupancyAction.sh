#!/bin/bash
#
export USER=online
export HOME=/home/${USER}
export MYSITEROOT=/sw/lib
source /sw/lib/LbLogin.sh
source /sw/oracle/set_oraenv.sh

export UTGID=LHCb_MONA0801_CaloOccupancyAction_00
export DIM_DNS_NODE="mona08"
export LOGFIFO=/tmp/logCaloOccupancyAction.fifo

. /group/calo/sw/scripts/setOrwell.sh

#exec -a ${UTGID} ${ORWELLROOT}/${CMTCONFIG}/Orwell.exe ${ORWELLOPTS}/AutoCaloOccupancyAction.opts -msgsvc=LHCb::FmcMessageSvc


# Start the task
exec -a ${UTGID} GaudiOnlineExe.exe libGaudiOnline.so OnlineTask -auto \
    -msgsvc=LHCb::FmcMessageSvc \
    -tasktype=LHCb::Class1Task \
    -main=/group/online/dataflow/templates/options/Main.opts \
    -opt="${ORWELLOPTS}/AutoCaloOccupancyAction.opts"
