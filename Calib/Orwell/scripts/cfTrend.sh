#unset det
#unset depth
depth=2
 if [ $1 ]; then

   if [ "$1" == "Ecal" -o "$1" == "Hcal" -o $1 == "Prs" -o "$1" == "Spd" ]; then

    export det=$1
    export depth=2 
    echo "Automated analysis log for $det"
    echo "log history depth = $depth" 

   else

     depth=$1
     echo "log history depth = $depth"
     read -p  "Which detector : Ecal,Hcal,Prs or Spd ? [Ecal] : " -e det
     if [  "$det" == "" ]; then
      export det="Ecal"
     fi
   fi

 else

     read -p  "Which detector : Ecal,Hcal,Prs or Spd ? [Ecal] : " -e det
     if [  "$det" == "" ]; then
      export det="Ecal"
     fi


 fi

 read -p  "Which analysis : CalibrationFarm (CF) or MonitoringFarm (MF) ? [CF] : " -e ana
 if [  "$ana" == "" ]; then
  export ana="CF"
 fi


echo "======="  $depth

depth=` expr $depth - 1 `
path=`ls -tr1 /clusterlogs/hosts/hist01/fmc*log*.gz | tail -$depth`


if [ $depth -gt 0 ]; then
    echo "----> "$path :
zcat $path | grep -i @@@ | grep -i Trend | grep -i "$det" | grep -i "${ana}Trend" | awk -F hist01 '{print $2  $3}' | awk -F "${ana}Trend" '{print $1  $2}'  | sed  's/INFO//'  | sed  's/WARNING//' | sed  's/FATAL//' | sed 's/\[\]//' | awk -F @@@ '{print $2}'
fi

fmc="/clusterlogs/hosts/hist01/fmclog"

echo "----> "$fmc :
more $fmc | grep -i @@@ | grep -i Trend | grep -i "$det" | grep -i "${ana}Trend" | awk -F hist01 '{print $2  $3}' | awk -F "${ana}Trend" '{print $1  $2}'  | sed  's/INFO//'  | sed  's/WARNING//' | sed  's/FATAL//' | sed 's/\[\]//' | awk -F @@@ '{print $2}'



