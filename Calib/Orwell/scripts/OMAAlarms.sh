. /group/calo/sw/scripts/setOrwell.sh


dumper="DumpOMAlarms.exe -d lhcbonr_hist -p r3aDerDB"
#dumper="DumpOMAlarms.exe -d lhcbonr_hist -p HISTEGGIA194"

if [ $1 ]; then
export det=$1
else
    read -p "OMA Alarms : subdetector (CALO/L0) ? [CALO] : " -e det
    if [ "$det" == "" ]; then
        export det="CALO"
    fi
fi

$dumper -s $det

export n=`$dumper -s $det | grep "There are 0 messages"  | wc -l`
if [ "$n" != "1" ]; then
    echo ""
    read -p "Clear alarm(s) ? [MSG ID] : " -e id
    if [ "$id" != "" ]; then
        $dumper -c -s $det   -m $id  -u HIST_WRITER -p HISTEGGIA194
    fi
fi

