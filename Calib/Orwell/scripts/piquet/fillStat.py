#!/usr/bin/python
from  RunDatabase import RunDbServer
from DbModel import createEngine_Oracle, createEngine_SQLite
from time import *
import sys

db=RunDbServer(engine=createEngine_Oracle())

d=float(sys.argv[1])*24.

start=strftime('%Y %m %d %H %M %S', gmtime(time()-d*60*60))
ok,fills=db.getFillEfficiencyStats(fromTime=start)
sortfills=sorted(fills, key=lambda field: field[0]) # sorted by fill

print ''
print '+++ Last %3dh fills :' % d
print "+--------------------------------------------------------------------------------------------------------+"
print " fillID      end-time             duration      Delivred (/nb)     Stored (/nb)      Inefficiency (%) "
print "+--------------------------------------------------------------------------------------------------------+"
s_d=0
s_lt=0
s_ls=0
for f in sortfills:
    if f[7] == 0. :
        continue
    end=f[1]
    fill=f[0]
    d= int(f[2] / 60/60)
    h = str(d)+':'
    duration=h+strftime('%M:%S', gmtime(f[2]))
    s_d = s_d + f[2]
    lumiTot=int(f[7]/10)/100.
    s_lt = s_lt+lumiTot
    lumiSto=int(f[11]/10)/100.
    s_ls = s_ls+lumiSto
    text=""
    if  abs(mktime(strptime(str(end), "%Y-%m-%d %H:%M:%S"))-mktime(localtime())) < 10 :
        text="         **in progress**"
    if lumiTot != 0 :
        ineff=round((1.-lumiSto/lumiTot)*10000)/100.
    else :
        ineff = 0.
    #print fill,duration,lumiTot,lumiSto,ineff
    print ' %5d    %10s   %10s       %10.2f        %10.2f        %10.2f   %15s' % ( fill, end,duration,lumiTot,lumiSto,ineff,text )
if  s_lt != 0 :
    s_i=round((1-s_ls/s_lt)*10000)/100.

d= int(s_d / 60/60)
h = str(d)+':'
s_dur=h+strftime('%M:%S', gmtime(s_d))
print "+--------------------------------------------------------------------------------------------------------+"
print ' %15s       %10s       %10.2f        %10.2f        %10.2f ' % ( 'Total                   ', s_dur,s_lt,s_ls,s_i )
print "+--------------------------------------------------------------------------------------------------------+"

 
