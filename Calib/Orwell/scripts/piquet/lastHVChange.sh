#!/bin/bash

echo ""


if [ "$1" != "both" -a "$1" == "Ecal" -a "$1" != "Hcal" ]; then
    read -p  "Which XCal (Ecal/Hcal/both) ? [both] : " -e calo
    if [  "$calo" == "" ]; then
	export calo="both"
    fi
else
    export calo=$1
fi

if [ "$calo" != "Ecal" -a "$calo" != "Hcal" -a "$calo" != "both" ]; then
    echo "Invalid calo name '"$calo"'"
    exit
fi
echo " "

lastDir=`ls -1dtr /hist/Savesets/2016/LHCb/CaloOccupancyAnalysis/*/* | tail -1`
lastSet=`ls -1tr /hist/Savesets/2016/LHCb/CaloOccupancyAnalysis/*/* | tail -1`
if [ "$2" != "last" ]; then
    echo "Last Savesets in $lastDir :"
    ls -1ltr $lastDir
    echo ""
fi
if [ "$2" != "last" ]; then
    read -p  "Which Saveset ? [$lastSet] : " -e saveset
    if [  "$saveset" == "" ]; then
	export saveset=$lastSet
    fi
else
    export saveset=$lastSet
    echo "  ... opening last saveset $lastDir/$saveset"
fi

file=$lastDir/$saveset
if [ "$calo" == "both" ]; then
    /group/calo/sw/scripts/presenter.sh -M history -l read-only --saveset-file=${file} --startup-page=/Calorimeters/Analysis/OccupancyData/Ecal/EcalAnalysisChangeSummary>/dev/null 2>/dev/null&
    /group/calo/sw/scripts/presenter.sh -M history -l read-only --saveset-file=${file} --startup-page=/Calorimeters/Analysis/OccupancyData/Hcal/HcalAnalysisChangeSummary>/dev/null 2>/dev/null&

else
    /group/calo/sw/scripts/presenter.sh -M history -l read-only --saveset-file=${file} --startup-page=/Calorimeters/Analysis/OccupancyData/${calo}/${calo}AnalysisChangeSummary>/dev/null 2>/dev/null&
fi

