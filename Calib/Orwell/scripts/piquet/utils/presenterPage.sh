#!/bin/bash
page="$1"
mode="$2"
export mydir=$PWD
export User_release_area=/group/online/dataflow/cmtuser
. ${User_release_area}/Presenter/setup.x86_64-slc6-gcc49-opt.vars;
cd $mydir
ulimit -s 1024
export TNS_ADMIN=/cvmfs/lhcb.cern.ch/lib/lcg/external/oracle/11.2.0.3.0/x86_64-slc6-gcc48-opt/admin
export LOCATION=/group/online/presenter
export LD_LIBRARY_PATH=/cvmfs/lhcb.cern.ch/lib/lcg/external/oracle/11.2.0.3.0/x86_64-slc5-gcc48-opt/lib:${LD_LIBRARY_PATH}
cd /hist
unset MOZ_NO_REMOTE
`which presenter.exe` -C $LOCATION/config.cfg -M "$mode" --window-width=1280 --window-height=500 -V silent -p "$page" >/dev/null 2>/dev/null&
# ==============================
