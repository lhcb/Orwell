#source  /sw/lib/scripts/ExtCMT.sh v1r20p20090520


#export LHCBHOME=/sw/lib
export LHCBHOME=/cvmfs/lhcb.cern.ch/lib/
export LHCBRELEASES=${LHCBHOME}/lhcb
export LHCBBIN=${LHCBHOME}/bin
#export LHCBPYTHON=${LHCBHOME}/scripts/python
#export LHCBSCRIPTS=${LHCBHOME}/scripts
#export LHCBPYTHON=/sw/lib/scripts/python
#export LHCBSCRIPTS=/sw/lib/scripts
#
export MYSITEROOT=/sw/lib
export SITEROOT=/sw/lib
#export PATH=${PATH}:$MYSITEROOT/scripts
#export CMT_DIR=$MYSITEROOT/contrib
#export CMTCONFIG=$CMTDEB


export User_release_area=/group/calo/sw/cmtuser
export CMTPROJECTPATH=${User_release_area}:${LHCb_release_area}:${LCG_release_area}


echo "User_release_area is set to: " ${User_release_area}
echo "CMTPROJECTPATH is set to   : " ${CMTPROJECTPATH}

source $LHCBHOME/LbLogin.sh

#----------------------
source /group/calo/sw/scripts/setOrwell.sh
# for new installation
#export OrwellVsn="v2r9"
#source $LHCBSCRIPTS/setenvProject.sh Orwell  ${OrwellVsn}
source setenvProject.sh Orwell  ${OrwellVsn}
if [ -d /group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn} ]; then
    export OrwellPath=/group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/${OrwellVsn}/
else
    export OrwellPath=/group/calo/sw/cmtuser/Orwell_${OrwellVsn}/Calib/Orwell/
fi
cd ${OrwellPath}/cmt
echo  $OrwellVsn
#----------------------
export MYSITEROOT=/sw/lib
#source /group/calo/sw/scripts/setup.sh

setenvOrwell  ${OrwellVsn}
echo "setting up the application ... wait"


echo "setting up Orwell application ... wait"
if [ -f ${OrwellPath}/cmt/setupCalo.vars ]; then
 echo "source setupCalo.vars"
 #source /group/calo/sw/scripts/setOrwell.sh
 source ${OrwellPath}/cmt/setupCalo.vars
else
 echo "source setup.sh"
 source ${OrwellPath}/cmt/setup.sh
fi




cd $ORWELLOPTS/

echo "... ready to play ! " 
