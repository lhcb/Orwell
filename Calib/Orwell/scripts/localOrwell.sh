echo "----------------o00o--------------------"
echo "setup a local copy of the Orwell project"
echo "----------------o00o--------------------"

# setenv local Orwell project
#source ${SITEROOT}/scripts/setenvProject.sh  Orwell_${HOST} $1
#source ${SITEROOT}/scripts/setenvProject.sh  myOrwell $1

if [ -d ~/cmtuser/myOrwell_$1 ]; then
    source setenvProject.sh  myOrwell $1
else
    export ref=v2r9
    echo "Install Orwell $ref and migrate to " $1
    source setenvProject.sh  Orwell $ref
    cd ~/cmtuser/
    mv Orwell_$ref myOrwell_$1
    cd myOrwell_$1
fi

# change project.cmt 
cd cmt/
#more -1  project.cmt | grep project > temp
echo "project myOrwell $1" > temp
echo "use Orwell_"$1 >>temp 
mv temp project.cmt

# re-setenv local Orwell project
#source ${SITEROOT}/scripts/setenvProject.sh  Orwell_${HOST} $1
source setenvProject.sh  myOrwell $1
echo "using Orwell_"$1 

# overwrite the CMTPROJECTPATH
export CMTPROJECTPATH=${User_release_area}:${Calo_release_area}:${LHCb_release_area}:${LCG_release_area}


# import package
if [ -d ./Calib ]; then
cd Calib
else
mkdir Calib
cd Calib
fi
if [ -d ./Orwell ]; then
cd Orwell
else
mkdir Orwell
cd Orwell
fi

if [ -d $Calo_release_area/Orwell_$1/Calib/Orwell/$1 ]; then
    if [ -d ./$1 ]; then
	cd $1
    else
	mkdir $1
	cd $1
    fi
    export OrwellPath=$Calo_release_area/Orwell_$1/Calib/Orwell/$1
else
    export OrwellPath=$Calo_release_area/Orwell_$1/Calib/Orwell
fi

if [ -d ./options ]; then
echo "'options' directory is already installed"
else
echo "import options directory"
cp -R $OrwellPath/options .
fi
if [ -d ./job ]; then
echo "'job' directory is  already installed"
else
echo "import job directory"
cp -R $OrwellPath/job .
fi

if [ -d ./work ]; then
echo "'work' directory is  already installed"
else
echo "import 'work' directory"
mkdir work 
fi


if [ -d ./${CMTCONFIG} ]; then
echo "'"$CMTCONFIG"' directory is already installed "
else
echo "link $CMTCONFIG directory"
#ln -sf $OrwellPath/${CMTCONFIG} .
cp -R $OrwellPath/${CMTCONFIG} .
fi

if [ -d ./cmt ]; then
echo "'cmt' directory is already installed"
else
echo "import cmt directory"
cp -R $OrwellPath/cmt .
cd cmt
echo "configuring the local package ... wait"
cmt config
cd ..
fi

cd options/

export quot='"'
# create destination IP options
#echo "Runable.rxIPAddr = "$quot > IPtemp
#/sbin/ifconfig | grep -m 1 'inet addr' | awk -F : '{print $2}' | awk -F Bcast '{print $1}' >> IPtemp
#echo $quot ";" >> IPtemp
#mv IPtemp destIP.opts
#create setFatal options
echo "MessageSvc.setFatal +={"$quot"Orwell@"${HOST}$quot"};"    > setFatal.opts
echo "MessageSvc.setFatal +={"$quot"TimeAlign@"${HOST}$quot"};" >> setFatal.opts
echo "MessageSvc.setFatal +={"$quot"CaloCalib@"${HOST}$quot"};" >> setFatal.opts
echo "MessageSvc.setFatal +={"$quot"L0CaloMoni@"${HOST}$quot"};" >> setFatal.opts

unset quot

cd ..
