if [ "${HOSTNAME:0:4}" = "plus" ]
then
	DATE=$( date +/%m/%d/ )
	YEAR=$( date +%y )
	TARGETDIR_MON=/hist/Savesets/20$YEAR/LHCb/CaloDAQMon
	TARGETDIR_CAL=/hist/Savesets/20$YEAR/LHCb/CaloDAQCalib

	FILENUM=$( ls -1 $TARGETDIR_MON$DATE | wc -l )

	if [ $FILENUM == 0 ]; then
 	echo Monitoring error! Check $TARGETDIR_MON
	else
	 echo [1/2] Monitoring OK on $TARGETDIR_MON "($FILENUM savesets in 20$YEAR$DATE)" 
	fi

	FILENUM=$( ls -1 $TARGETDIR_CAL$DATE | wc -l )

	if [ $FILENUM == 0 ]; then
	 echo Monitoring error! Check $TARGETDIR_CAL
	else
	 echo [2/2] Monitoring OK on $TARGETDIR_CAL  "($FILENUM savesets in 20$YEAR$DATE)"
	fi
else
	echo "Wrong Computer!! Must run under plus..."
fi

