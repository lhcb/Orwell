from Gaudi.Configuration import *


from GaudiPython.Bindings import gbl as cpp
LHCb       = cpp.LHCb
CellID = LHCb.CaloCellID 


import sys


first = sys.argv[1]
if first == 'Ecal' or first == 'Hcal' or first == 'Prs' or first == 'Spd'  :
    second = sys.argv[2]
    if second == 'Inner' or second == 'Middle' or second == 'Outer' or second == 'PinArea'  :
        k=0
        for arg in sys.argv:
            if k>0 and k%4 == 1:
                cell = CellID( sys.argv[k], sys.argv[k+1],int(sys.argv[k+2]),int(sys.argv[k+3]))
                print 'all = %7i - index = %7i - ID= [%s,%s,%5i,%5i]' % (cell.all(),cell.index(),cell.caloName(), cell.areaName(), cell.row(),  cell.col())
            k=k+1
    else :
        k=0
        for arg in sys.argv:
            if k>0 and k%2 == 1:
                cell = CellID(int(sys.argv[k+1]) )
                cell.setCalo(sys.argv[k])
                print 'all = %7i - index = %7i - ID= [%s,%s,%5i,%5i]' % (cell.all(),cell.index(),cell.caloName(), cell.areaName(), cell.row(),  cell.col())
            k=k+1
        

else :
    k=0
    for arg in sys.argv:
        if k>0 :
            cell = CellID( int(arg) )
            print 'all = %7i - index = %7i - ID= [%s,%s,%5i,%5i]' % (cell.all(),cell.index(),cell.caloName(), cell.areaName(), cell.row(),  cell.col())
        k=k+1



