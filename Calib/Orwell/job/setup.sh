source  /sw/lib/scripts/ExtCMT.sh v1r20p20090520
#source  /sw/lib/scripts/ExtCMT.sh v1r20p20070208


if `echo $HOST | grep "plus" 1>/dev/null 2>&1`; then
    echo ... increasing virtual memory ulimit on plus machine
    ulimit -v 2097152
fi
#----------------------
#source /group/calo/sw/scripts/setOrwell.sh
#----------------------
echo "Setting Calorimeter Online environment"
export MYSITEROOT=/sw/lib


export Calo_script_area=/group/calo/sw/scripts/
alias presenter='source /group/calo/sw/scripts/presenter.sh'
alias LEDanalysis='source /group/calo/sw/scripts/LEDanalysis.sh'

alias panoramix='source /group/calo/sw/scripts/launchPanoramix.sh'

alias setupUser='source /group/calo/sw/scripts/setupUser.sh'
alias setupEB='source /group/calo/sw/scripts/easySetupEB.sh' 
alias setEB='source /group/calo/sw/scripts/setupEB.sh' 
alias launchEB='source /group/calo/sw/scripts/launchEB.sh'

alias cleanall='source /group/calo/sw/scripts/cleanall.sh'
alias finalize='source /group/calo/sw/scripts/final.sh'
alias cleanup='source $ORWELLROOT/cmt/cleanup.sh'
alias cleanb='sudo /group/online/scripts/cleaneb'

alias getrun='source /group/calo/sw/scripts/getrun.sh'
alias caloTimer='source /group/calo/sw/scripts/caloTimer.sh'

alias cfTrend='source /group/calo/sw/scripts/cfTrend.sh'
alias caloAnalysis='source /group/calo/sw/scripts/caloMerger.sh'

alias OMACaloAlarms='/group/online/scripts/dumpOMAlarms -s CALO'
alias OMACaloAlarmClear="/group/online/scripts/dumpOMAlarms -c -s CALO -u HIST_WRITER -p histeggia194  -m"
alias OMAALarms='source /group/calo/sw/scripts/OMAAlarms.sh'
#alias CaloCellID='python /group/calo/sw/scripts/CaloCell.py'
alias CaloCellID='source /group/calo/sw/scripts/setEnv.sh ; python /group/calo/sw/scripts/CaloCell.py'
alias setup.sh='./setup.sh'

