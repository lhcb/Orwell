// Control channels (from Iouri 03/2010}
HcalAnalysis.ControlChannels = {54485 : "Unstable Hcal channel (added by Iouri 2010/03)",
                                55128 : "Unstable Hcal channel (added by Iouri 2010/03)",
                                50828 : "Unstable Hcal channel (added by Iouri 2010/03)",
                                54941 : "Unstable Hcal channel (added by Iouri 2010/03)",
                                49478 : "Unstable Hcal channel (added by Iouri 2010/03)"};
HcalAnalysis.HistDBPagePrefix = "/Calorimeters/Analysis/CalibrationData";


// Short Summary aka DM-plot
HcalAnalysis.ShortSummary +={
  "Pedestal"         : {"PedestalShift","LargePedestalShift","AveragePedestalNoise","PedestalNoise","LargePedestalNoise","PedestalShiftOverNoise","PedestalChi2"},
  "LEDNoise"         : {"LEDRMS","LEDNoise","LargeLEDNoise"},
  "NoGain"           : {"LowLEDSignal","OutRangeLED","LEDSaturated","NoGainMonitor"},
  "Pmt2Pin"          : {"Pmt2PinNoise","Pmt2PinMean","Pmt2PinRMS"},     
  "UnexpectedSignal" : {"UnexpectedSignal"},
  "GainVariation"    : {"GainVariation"}
};


HcalAnalysis.Monitors    += {  
  "PedestalShift"
    ,"LargePedestalShift"
    ,"AveragePedestalNoise"
    ,"PedestalNoise"
    ,"PedestalShiftOverNoise"
    ,"LargePedestalNoise"
    ,"PedestalChi2"
    ,"UnexpectedSignal" 
    ,"LowLEDSignal"
    ,"OutRangeLED"
    ,"LEDNoise"
    ,"LargeLEDNoise"
    ,"LEDSaturated"
    ,"NoGainMonitor"
    ,"Pmt2PinNoise"
    // for condDB update
    ,"Pmt2PinMean"
    ,"Pmt2PinRMS"
    ,"LEDRMS"
    // at the end
    ,"GainVariation"
};
// CaloMonitors needed for condDB update
HcalAnalysis.Pmt2PinMean.Data          = {"PMT/PIN", "Mean"};         
HcalAnalysis.Pmt2PinRMS.Data           = {"PMT/PIN", "RMS" };         
HcalAnalysis.LEDRMS.Data               = {"LED"    , "RMS" };         
HcalAnalysis.LEDRMS.PinMonitor = true;


// Alarm publishing
HcalAnalysis.PedestalShift.OnlineTrend   = true;
HcalAnalysis.PedestalShift.PublishAlarm = true;
HcalAnalysis.PedestalNoise.PublishAlarm = true;
HcalAnalysis.PedestalShiftOverNoise.PublishAlarm = true;
HcalAnalysis.LargePedestalShift.PublishAlarm = true;
HcalAnalysis.LargePedestalNoise.PublishAlarm = true;

//=============================== PEDESTAL SURVEY ===========================

//----------- PEDESTAL MEAN (1)
HcalAnalysis.PedestalShift.Data          = {"Pedestal" , "Mean" , "Profile" };
HcalAnalysis.PedestalShift.DBQualityMask = 4;     // Shifted pedestal
HcalAnalysis.PedestalShift.ExpectedRange  = {0.1 , 0.8} ;
HcalAnalysis.PedestalShift.AlarmLevels   = { 100   : "Warning",
                                             500   : "Alarm"   ,
                                             3000  : "Fatal"  }; 
HcalAnalysis.PedestalShift.PinMonitor = true;  // monitor also the pin-diodes
HcalAnalysis.PedestalShift.ExpectedPINRange  = { 0.1 , 3.} ;

//----------- PEDESTAL MEAN (2)
HcalAnalysis.LargePedestalShift.Data          = {"Pedestal" , "Mean"};         
HcalAnalysis.LargePedestalShift.DBQualityMask = 32;     // VeryShifted pedestal
HcalAnalysis.LargePedestalShift.ExpectedRange  = {-1.0 , 5.0} ;
HcalAnalysis.LargePedestalShift.AlarmLevels   = { 10   : "Alarm" ,
                                                  1000 : "Fatal"  }; 
HcalAnalysis.LargePedestalShift.PinMonitor = true;  // monitor also the pin-diodes
HcalAnalysis.LargePedestalShift.ExpectedPINRange  = { -10.0 , 10.0} ;

//----------- AVERAGE PEDESTAL NOISE
HcalAnalysis.AveragePedestalNoise.MonitorChannelsAverage = true;  
HcalAnalysis.AveragePedestalNoise.Data          = { "Pedestal", "RMS" };         
HcalAnalysis.AveragePedestalNoise.ExpectedRange = { 1.0 , 1.5          } ;
HcalAnalysis.AveragePedestalNoise.AlarmLevels   = { 0   : "Warning"  };

//----------- PEDESTAL MEAN/NOISE
HcalAnalysis.PedestalShiftOverNoise.Data          = {"Pedestal" , "Mean/RMS" , "Profile" };
HcalAnalysis.PedestalShiftOverNoise.ExpectedRange  = {0.1 , 0.8} ;
HcalAnalysis.PedestalShiftOverNoise.AlarmLevels   = { 100   : "Warning",
                                                      500   : "Alarm"   ,
                                                      1000  : "Fatal"  }; 
HcalAnalysis.PedestalShiftOverNoise.PinMonitor = true;  // monitor also the pin-diodes

//----------- PEDESTAL NOISE (1)
HcalAnalysis.PedestalNoise.OnlineTrend   = true;
HcalAnalysis.PedestalNoise.Data          = { "Pedestal", "RMS" };        
HcalAnalysis.PedestalNoise.DBQualityMask = 2;     // Noisy 
HcalAnalysis.PedestalNoise.ExpectedRange  = { 0.6 , 2.0} ;
HcalAnalysis.PedestalNoise.AlarmLevels   = { 100   : "Warning",
                                             500   : "Alarm"   ,
                                             1000  : "Fatal"  }; 
HcalAnalysis.PedestalNoise.PinMonitor = true;  // monitor also the pin-diodes
HcalAnalysis.PedestalNoise.ExpectedPINRange  = { 1.0 , 5.0} ;


//----------- PEDESTAL NOISE (2)
HcalAnalysis.LargePedestalNoise.Data          = { "Pedestal", "RMS" };  
HcalAnalysis.LargePedestalNoise.DBQualityMask = 16;     // VeryNoisy 
HcalAnalysis.LargePedestalNoise.ExpectedRange  = { 0.6 , 5.0} ;
HcalAnalysis.LargePedestalNoise.AlarmLevels   =  { 10   :  "Alarm" ,
                                                   100  :  "Fatal"  }; 
HcalAnalysis.LargePedestalNoise.PinMonitor = true;  // monitor also the pin-diodes
HcalAnalysis.LargePedestalNoise.ExpectedPINRange  = { 1.0 , 10.} ;

//----------- PEDESTAL Chi2
HcalAnalysis.PedestalChi2.Data           = {"Pedestal" , "Chi2", "Fit"}; 
HcalAnalysis.PedestalChi2.ExpectedRange  = {0. , 20.0} ;
HcalAnalysis.PedestalChi2.AlarmLevels    = { 100  :  "Warning",
                                             500  :  "Alarm"  ,
                                             1000 :  "Fatal"  }; 





//========================== UNEXPECTED SIGNAL SURVEY ===========================
HcalAnalysis.UnexpectedSignal.Data           = {"Desert" , "Entries", "Profile"}; 
HcalAnalysis.UnexpectedSignal.ExpectedRange  = {0. , 0.} ;
HcalAnalysis.UnexpectedSignal.AlarmLevels    = { 10   :  "Warning",
                                                 100  :  "Alarm"  ,
                                                 3000 :  "Fatal"  }; 


//=========================  LED SIGNAL SURVEY ==================================

//----------- LED MEAN SIGNAL (Low)
HcalAnalysis.LowLEDSignal.Data           = {"LED" , "Mean"};         
HcalAnalysis.LowLEDSignal.DBQualityMask = 1033;     // Dead channel or LED or optical bundle
HcalAnalysis.LowLEDSignal.ExpectedRange  = { 30. , 3840.} ;
HcalAnalysis.LowLEDSignal.AlarmLevels    = { 10   :  "Warning",
                                             100  :  "Alarm"  ,
                                             3000 :  "Fatal"  }; 
HcalAnalysis.LowLEDSignal.PinMonitor = true;  // monitor also the pin-diodes



//----------- LED MEAN SIGNAL (OutRange)
HcalAnalysis.OutRangeLED.Data           = {"LED" , "Mean"};         
HcalAnalysis.OutRangeLED.ExpectedRange  = { 100. , 3700.} ;
HcalAnalysis.OutRangeLED.AlarmLevels    = { 10   :  "Warning",
                                            100  :  "Alarm"  ,
                                            3000 :  "Fatal"  }; 
HcalAnalysis.OutRangeLED.PinMonitor = true;  // monitor also the pin-diodes


//----------- LED MEAN SIGNAL (Saturated LED)
HcalAnalysis.LEDSaturated.Data           = {"LED" , "Mean"};         
HcalAnalysis.LEDSaturated.DBQualityMask = 64;     // LED-saturated channel
HcalAnalysis.LEDSaturated.ExpectedRange  = { -254 , 3838.} ;
HcalAnalysis.LEDSaturated.AlarmLevels    = { 10   :  "Warning",
                                            100  :  "Alarm"  ,
                                            3000 :  "Fatal"  }; 
HcalAnalysis.LEDSaturated.PinMonitor = true;  // monitor also the pin-diodes


//----------- LED SIGNAL RMS/Mean
HcalAnalysis.LEDNoise.Data           = {"LED" , "RMS/Mean"}; 
HcalAnalysis.LEDNoise.ExpectedRange  = { 0.0, 0.05 } ;
HcalAnalysis.LEDNoise.AlarmLevels    = { 10   :  "Warning",
                                         100  :  "Alarm"  ,
                                         3000 :  "Fatal"  }; 
HcalAnalysis.LEDNoise.PinMonitor = true;  // monitor also the pin-diodes
HcalAnalysis.LEDNoise.ExpectedPINRange  = { 0.0 , 0.01} ;


//----------- LED SIGNAL RMS/Mean (2)
HcalAnalysis.LargeLEDNoise.Data          = {"LED" , "RMS/Mean"}; 
HcalAnalysis.LargeLEDNoise.ExpectedRange = { 0.0 , 0.10 } ;
HcalAnalysis.LargeLEDNoise.AlarmLevels   = { 10  :  "Alarm"  ,
                                             100 :  "Fatal"  }; 
HcalAnalysis.LargeLEDNoise.PinMonitor = true;  // monitor also the pin-diodes
HcalAnalysis.LargeLEDNoise.ExpectedPINRange  = { 0.0 , 0.05} ;


//======================== GAIN SURVEY ====================================

//----------- PMT/PIN ratio #Entries

HcalAnalysis.NoGainMonitor.Data          = {"PMT/PIN", "Entries","Profile"};         
HcalAnalysis.NoGainMonitor.ExpectedRange  = {0. , 0.} ;
HcalAnalysis.NoGainMonitor.InvertRange = true;
HcalAnalysis.NoGainMonitor.AlarmLevels   = { 10   :  "Warning",
                                             100  :  "Alarm"  ,
                                             1000 :  "Fatal"  }; 

//----------- PMT/PIN ratio RMS/MEAN

HcalAnalysis.Pmt2PinNoise.Data          = {"PMT/PIN", "RMS/Mean","Profile"};         
HcalAnalysis.Pmt2PinNoise.ExpectedRange  = {0.00 , 0.035} ;
HcalAnalysis.Pmt2PinNoise.AlarmLevels   = { 10   :  "Warning",
                                            100  :  "Alarm"  ,
                                            1000 :  "Fatal"  }; 


//----------- PMT/PIN ratio wrt DB

HcalAnalysis.GainVariation.OnlineTrend   = true;
HcalAnalysis.GainVariation.Data          = {"PMT/PIN", "GainVariation","Profile"};         
HcalAnalysis.GainVariation.ExpectedRange  = {0.90 , 1.10 } ;
HcalAnalysis.GainVariation.AlarmLevels   = { 50   :  "Warning",
                                             100   :  "Alarm"  ,
                                             1600 :  "Fatal"  }; 



