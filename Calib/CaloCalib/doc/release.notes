! Package: CaloCalib
! Package Coordinator : Olivier Deschamps
! Purpose : Algorithms for Online Calorimeter Calibration 
!----------------------------------------------------------------------------


!=================== CaloCalib v3r0 2019-01-09 ==========================

! 2019-01-09 : Drop Online dependency. Retired algs:
  	       	    -DumpCalibCoeff.cpp
	     	    -DumpGainShiftCoeff.cpp
	     	    -OMACaloAnalysis.cpp
	     	    -OMAOccupancyAnalysis.cpp
	     	    -OMAPi0Analysis.cpp
		    +CaloDb.cpp
		    +CaloEFlowAnalysis.cpp
		    +CaloMonitor.cpp
		    +L0CaloGainMonitor.cpp
		    +OccupancyAnalysis.cpp
		    +ParasiticDataMonitor.cpp


!=================== CaloCalib v2r13 2017-05-29 ==========================

! 2017-05-29 - CaloEflowAnalysis : fix constructor ERROR "Unknown detector name"


!=================== CaloCalib v2r11 2017-03-25 ==========================

! 2016-03-25 - OD : 
						 - update against changes in LHCb v42r1
						 - CMakeLists.txt : fix CMAKE compilation

!=================== CaloCalib v2r10 2016-04-01 ==========================

! 2016-04-01 - OD
  - Added :
    <= (A) =>  ECALmap.cpp 
    <= (A) =>  ECALmap.h 
    <= (A) =>  HCALmap.cpp 
    <= (A) =>  HCALmap.h 
    <= (A) =>  OccupancyAnalysis.cpp 

! 2015-10-14 - OD
  - Fix OMAPi0Analysis ...

!=================== CaloCalib v2r9 2015-04-16 ==========================

! 2015-10-01 - OD
  - Prepare for 2015 first data

! 2015-04-16 - OD
  - HcalMonitor.opts : enlarge Pmt/PIN noise window fro 2.5% to 3.5%


!=================== Orwell v2r8 2014-05-19 ==========================

! 2014-05-19 - OD
  - ParasiticMonitor : add threshold property


! 2014-03-18 - Marco Clemencic
 - Added CMake configuration file.
 - Fixed compilation with the latest Gaudi.

!=================== Orwell v2r7 2013-10-31 ==========================

! 2013-09-03 OD
  - CaloEflowAnalysis : fix finalize


! 2012-04-04 OD 
  - Add OMAPi0Analysis.cpp (from Benoit) : perform Pi0 mass trending

!=================== Orwell v2r6 2012-04-04 ==========================


! 2012-10-23 OD 
  - change SpdMonitor threshold + minStatistics increase

! 2012-04-04 OD 
  - Add L0CaloGainMonitor (from Patrick)

! 2011-10-09 OD 
  - fix CaloMonitorNtp

! 2011-09-15 OD for Aurelien Martens
	- update CaloEflowAnalysis + options
	- update ParasiticDataMonitor
	- OMACaloAnalysis : change histDB pagination + fix rms-longtrending 

! 2011-09-12 Benoit Viaud + Olivier Deschamps
	- OMACaloAnalysis : update histDB page creator for ShortSummary histo
	- update options/XCal(Data)Monitor.opts to configure shortSummary histo (Benoit settings)

! 2011-09-12 Olivier Deschamps
  - new monitoring algo : ParasiticDataMonitor (monitor any data directly from raw - useful for CrateTiming parasitic FEBs)
  - implement 'ShortSummary 2D histo' (aka DM-plots) in OMACaloAnalysis
  - new algorithm : CaloCalibDBCreator : produce xml condDB files from Kali output

! 2011-09-04 Olivier Deschamps
  - add dependency to Online/Trending
  - adapt to change in Online/Trending interface

! 2011-04-24 Olivier Deschamps
  - update automated analysis + XXMonitor options

! 2011-03-25 Olivier Deschamps
  - tune the parameters for the LowOccupancy monitor 
 
! 2011-03-18 Olivier Deschamps
  - remover TriggerTypeCounter (moved to DAQMonitors)

! 2011-03-17 Olivier Deschamps
  - HcalMonitor : add missing monitor ('LEDSaturated')

!=================== Orwell v2r4 2011-03-08 ==========================
  
! 2011-03-07 Olivier Deschamps
  - OMACalibAnalysis : fix PrsAutoUpdate
  - Numeric Gains now given separatly for odd and even BCID (via options) 

! 2011-01-12 Olivier Deschamps
  - OMACalibAnalysis : 
    - prepare usage of Prs NumericalGain from condDB
    - simplify trending module + add spread and min-max trending
    - condDB table output (from Benoît)
  - XcalMonitor.opts : complete condDB quality flag 
  - CaloCalib/CaloHisto : add possibility to get the histo range and window from condDB

  
! 2010-11-03 Olivier Deschamps
  - update DumpCalibCoeff : possibility to set input coefficients via an options table.

! 2010-10-20 Olivier Deschamps
  - OMACaloAnalysis : fix label for Spd monitor (ADC->rate)
  
! 2010-10-16 Olivier Deschamps
  - OMACaloAnalysis : fix refreshMessageList 
  
! 2010-10-16 Olivier Deschamps
  - OMACaloAnalysis : send the full path of the output saveset into OMA messaging.

! 2010-10-13 Olivier Deschamps
  - SpdMonitor : extend the expected fake/missing rate range up to 10^-3

  
! 2010-10-12 Olivier Deschamps
  - OMACaloAnalysis : fix raiseMessage histo link + message

!=================== CaloCalib v2r3 2010-10-12 ==========================

! 2010-10-12 Olivier Deschamps
  - prepare v2r3

! 2010-10-12 Olivier Deschamps
  - Update : CaloEfficiency, OMACaloAnalysis.

! 2010-01-29 Olivier Deschamps

  - OMACaloAnalysis/CaloMonitor : fix monitor status re-initialisation
  - CaloSpectrum : produce split + full 1D histo
  - prepare v2r2

! 2009-12-18 - Olivier Deschamps
  - EcalCalib.opts : set downscal rate for pedestal to 64%

!=================== CaloCalib v2r1 2009-12-03 ==========================
! 2009-11-03 - Olivier Deschamps 
  - OMACaloAnalysis/CaloMonitor : fix the online publishing of histograms
  - add (temporarly) a new algorithm : TriggerTypeCounter (should be moved in ODIN monitor component library)
 
! 2009-11-20 - Olivier Deschamps 
  - OMACaloAnalysis.cpp/CaloMonitor : delegate renaming to MonitorSvc when active
  - CaloEFlowAnalysis : HistoID( integer ) -> HistoID( toString( integer) ) to please HistoID::literalID in Calo2Dview

! 2009-11-20 - Olivier Deschamps 
  - OMACaloAnalysis.cpp/CaloMonitor : set histo Name in the code for local saving (no longer rely on MonitorSvc)

! 2009-11-17 - Olivier Deschamps 
  - CaloSpectrum.cpp : fix missing initalization

!=================== CaloCalib v2r0 2009-11-09 ==========================
! 2009-09-14 - Olivier Deschamps 
 - remove Calo(L0)DataProvider::getBanks everywhere (now handled by IncidentSvc in CaloDAQ). 
  Use CalODataProvider::ok() to protect against missing banks.

! 2009-09-11 - Olivier Deschamps 
 - CaloTiming : add kernel & asymmetry monitoring per crate

! 2009-09-11 - Olivier Deschamps 
 - New analysis alg. : DumCalibCoeff.{cpp,h}

! 2009-09-02 - Olivier Deschamps 
 - CaloDisplay : add TAE slot in default 2Dview title

! 2009-07-31 - Olivier Deschamps 
 - OMACaloAnalysis.cpp : - allow READ-ONLY access to root file (needed online)
                         - protect fit against empty histo



!========================== CaloCalib v1r8 2009-06 ==========================

! 2009-05-05 - Olivier Deschamps for Aurelien Martens
 - Automatic Eflow histo analysis : CaloEFlowAnalysis
 - CaloDisplay.cpp : pre-book histo at initialize() (when #Sequences = 1)


! 2009-05-05 - Olivier Deschamps
 - Automatic Histo Analysis : OMACaloAnalysis / CaloMonitor


! 2009-03-10 - Olivier Deschamps
 - new algorithm : CaloPMSignal
 - src/CaloCalib.{cpp,h} : default options for profile error evaluation set to "s" for spread (new propery 'ProfileError')
 - options/XXCalib.opts  : define new default
 - src/CaloNtpMonitor : new property 'inspectAll' (default = true) : allow to force filling the ntuple with all valid channels and 
                        not only the existing ADCs in the reference time slot (which make the additional time slots filling dependent on the
                        0-suppression in the reference time slot)
 
! 2009-02-26 - Olivier Deschamps
 - cmt/requirements : add dependency to LHCbAlgs (to access OdinTimeDecoder that has moved)

! 2008-10-30 - Olivier Deschamps
 - remove CaloDigitChannel (moved to CaloMoniDigi)



!========================== Orwell v1r7 2008-10-16 ==========================
! 2008-08-22 - Olivier Deschamps
 - new release v1r7 
! 2008-08-22 - Olivier Deschamps
 - CaloSpectrum.cpp : monitor detector asymetries (left/right, top/bottom)
 - CaloCalib.cpp    : add detector name + threshold in histo title
 - new algorithms   : CaloMatchSpdPrs & CaloTiming & SpdL0Timing (from Hugo)
 - add Analysis Alg : OMACaloProfileAnalysisTask (from Sergey)
 - cmt/requirements : add OMALib dependancy


!========================== Orwell v1r5 2008-06-13 ==========================

! 2008-06-08 - Olivier Deschamps
 - add filter functionalities in CaloChannelCounter


! 2008-06-06 - Olivier Deschamps
 - offset and threshold as double in CaloDisplay


!========================== Orwell v1r4 2008-05-30 ==========================
! 2008-05-31 - Olivier Deschamps
 - release v1r4


! 2008-04-16 Olivier Deschamps
  - new monitoring algorithm : CaloSpectrum.cpp
  - add ODIN calibrationStep and EventType in CaloNtpMonitor tupling

! 2008-04-01 Olivier Deschamps
  - Add summary profile1D in CaloCalib (boolean property 'Profile' in CaloHisto)

! 2008-03-26 Olivier Deschamps
  - New monitoring algorithm : CaloChannelCounter.{cpp,h}

! 2008-03-02 Olivier Deschamps
  - CaloCalib/CaloDisplay : move putStatusOnTES() call 

! 2008-03-07 Olivier Deschamps
  - CaloCalib/CaloNtpMonitor : fix several minor problems with rootInTES when reference sample != "T0"

! 2008-03-07 Olivier Deschamps
  - CaloCalib/CaloDisplay : improve default properties
  - CaloDisplay : add filter property


! 2008-02-04 Yasmine Amhis + Olivier Deschamps
  - Add Yasmine's TAE analysis code

!========================== Orwell v1r3 2008-01-29 ==========================
! 2008-01-29 Olivier Deschamps
	- new release v1r3
	- CaloCalib :
		- improve histo path (adding crate/feb reference for channel histos)
	  - book histo according to requested Bank list (if any)
		- improve treatment of TAE events 
			-> book additional time slots  when rawEvent is present
			-> can run transparently on non-TAE event with non-empty AdditionalTimeSlots vector
		- put RawBankReadoutStatus on TES for further monitoring (via DAQEvent/RawBankReadoutStatusMonitor)
	  - include pedestal shift (from condDB) in Ratio computation [ratio = (PMT-pedShift)/(PIN-pinPedShift) ]
	  - few small bug fixes
 	- CaloNtpMonitor :
		- add L0-ADC
	  - allow to link pin-diode from a different time slot than the reference one (PinTimeSlot property)
	  - improve treatment of TAE events 	
			-> tuple array declared when rawEvent is present
			-> can run transparently on non-TAE event with non-empty AdditionalTimeSlots vector
		- put RawBankReadoutStatus on TES for further monitoring (via DAQEvent/RawBankReadoutStatusMonitor)
	  - few small bug fixes
	- new algorithm : CalODisplay (2D display of calo events - inherits from CaloUtils/Calo2Dview)
	- Refresh options



! 2007-08-23 Olivier Deschamps
  - implement TAE also for pedestal histos

! 2007-08-06 Olivier Deschamps
  - fix unchecked StatusCodes in CaloNtpMonitor (ntp-> & setProperties)

! 2007-08-06 Olivier Deschamps
  - adapt to the change in RootInTes syntax (Gaudi v19r4)
  - implement TAE histos in CaloCalib (see XcalCalib.opts to get the syntax)
  - implement the possibility to define a specific histo range for each Calo cell in CaloHisto.h
  - fix crash in CaloNtpMonitor when a bank is not present in the first event

!=================== Calo/CaloCalib v1r1 2007-04-30 =========================
! 2007-04-25 Olivier Deschamps
  - Allow to disable/enable the pin readout in CaloNtpMonitor

! 2007-04-17 Olivier Deschamps
  - Remove obsolete options
  - add new monitoring algorithm : CaloNtpMonitor.{cpp,h}
  - add corresponding options : [Det]Ntp.opts

! 2007-04-11 Olivier Deschamps
 - Remove obsolete CaloMonitor algs
 - CaloCalib : produce only histo for channels connected to requested banks

!=================== Calo/CaloCalib v1r0 2007-03-07 =========================
! 2007-03-07 - Marco Cattaneo
 - Remove obsolete CaloCalib_load.cpp file

! 2007-03-05 - Olivier Deschamps
 - Allow to duplicate Signal histogram (SignalBis) with alternative range
 - few fixed for pedestal rate and HCAL

! 2007-03-02 - Olivier Deschamps
 - histo publication : declareInfo() added

! 2007-02-28 - Olivier Deschamps
 -  CaloCalib algorithm completed
 -  Prs/Spd calibration included
 -  add new algorithm : CaloHisto
 -  remove CaloMonitor (included in CaloCalib)

! 2007-02-23 - Olivier Deschamps
 - improve algorithms

! 2007-02-22 - Olivier Deschamps
 - first version 
 - contain 2 preliminary algorithms :
	- CaloCalib.{cpp,h}  : calibration with PIN-diode (Ecal/Hcal)
	- CaloMonitor.{cpp,h}: 'calibration' without PIN (Spd/Prs)
 	

