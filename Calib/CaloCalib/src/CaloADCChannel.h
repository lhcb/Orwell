
#ifndef CaloADCChannel_H
#define CaloADCChannel_H 1
//
//Include files

//from STL

#include <cmath>
#include <string>
#include <vector>
//From Gaudi
#include "GaudiKernel/IEventTimeDecoder.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "CaloDAQ/ICaloEnergyFromRaw.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloDAQ/ICaloDataProvider.h"
/** @class CaloADCChannel CaloADCChannel.h
 *
 *  Evaluate the ADC Hits in the Calo Channel by Channel
 *
 *  @author Yasmine AMHIS
 *  @date   2008-02-25
 */


class CaloADCChannel: public GaudiTupleAlg{
public:
  CaloADCChannel  ( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CaloADCChannel( ); ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:
  DeCalorimeter* m_calo;

  IEventTimeDecoder*  m_odin;
  std::string m_nameOfDetector; // Detector short name
  std::string m_inputData;         ///< Input container
  std::string m_TAEcontainer;
  ICaloEnergyFromRaw*  m_energyTool;
  bool m_normal;
  ICaloDataProvider* m_daq;
std::string m_readoutTool;
};
#endif //CaloADCChannel
