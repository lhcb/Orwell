#ifndef CALOTYP_C_ORACLE
#define CALOTYP_C_ORACLE

#ifndef OCI_ORACLE
#include <oci.h>
#endif

typedef OCIRef hcal_chan_typ_ref;
typedef OCIRef hcal_pmcalib_typ_ref;
typedef OCIRef hcal_cscalib_typ_ref;
typedef OCIRef tile_curr_typ_ref;
typedef OCIRef ecal_chan_typ_ref;
typedef OCIRef ecal_pmcalib_typ_ref;
typedef OCIRef pmgain_pnt_typ_ref;
typedef OCIRef hcal_ledpin_typ_ref;
typedef OCIRef ecal_led_typ_ref;
typedef OCIRef ecal_pin_typ_ref;
typedef OCIRef hcal_module_typ_ref;
typedef OCIRef hcal_ledpinref_typ_ref;
typedef OCIRef leddac_setting_typ_ref;
typedef OCIRef ledtsb_setting_typ_ref;
typedef OCIRef ecal_ledpin_calib_typ_ref;
typedef OCIRef ledpin_calib_typ_ref;
typedef OCIRef ledpin_pnt_typ_ref;
typedef OCITable hcal_cscalib_list;
typedef OCITable hcal_pmcalib_list;
typedef OCITable dac_setting_list;
typedef OCITable adc_setting_list;
typedef OCITable pmgain_pnt_list;
typedef OCITable tile_curr_list;
typedef OCITable ecal_pmcalib_list;
typedef OCIRef dac_setting_typ_ref;
typedef OCIRef adc_setting_typ_ref;
typedef OCITable ledpin_calib_list;
typedef OCITable leddac_setting_list;
typedef OCITable ledtsb_setting_list;
typedef OCITable ecal_ledpin_calib_list;
typedef OCITable hcal_ledpinref_list;
typedef OCITable ledpin_pnt_list;

struct dac_setting_typ
{
   OCIDate d_set;
   OCIDate d_applied;
   OCINumber hv_phys;
   OCINumber hv_stdb;
};
typedef struct dac_setting_typ dac_setting_typ;

struct dac_setting_typ_ind
{
   OCIInd _atomic;
   OCIInd d_set;
   OCIInd d_applied;
   OCIInd hv_phys;
   OCIInd hv_stdb;
};
typedef struct dac_setting_typ_ind dac_setting_typ_ind;

struct adc_setting_typ
{
   OCIDate d_set;
   OCIDate d_applied;
   OCINumber delay_adc;
};
typedef struct adc_setting_typ adc_setting_typ;

struct adc_setting_typ_ind
{
   OCIInd _atomic;
   OCIInd d_set;
   OCIInd d_applied;
   OCIInd delay_adc;
};
typedef struct adc_setting_typ_ind adc_setting_typ_ind;

struct hcal_chan_typ
{
   OCIString * chan_type;
   OCINumber chan_x;
   OCINumber chan_y;
   OCIString * mod_id;
   OCINumber mod_chan;
   hcal_cscalib_list * cs_calibs;
   hcal_pmcalib_list * pm_calibs;
   OCINumber dac_chan;
   OCINumber adc_chan;
   dac_setting_list * dac_settings;
   adc_setting_list * adc_settings;
};
typedef struct hcal_chan_typ hcal_chan_typ;

struct hcal_chan_typ_ind
{
   OCIInd _atomic;
   OCIInd chan_type;
   OCIInd chan_x;
   OCIInd chan_y;
   OCIInd mod_id;
   OCIInd mod_chan;
   OCIInd cs_calibs;
   OCIInd pm_calibs;
   OCIInd dac_chan;
   OCIInd adc_chan;
   OCIInd dac_settings;
   OCIInd adc_settings;
};
typedef struct hcal_chan_typ_ind hcal_chan_typ_ind;

struct pmgain_pnt_typ
{
   OCINumber hv;
   OCINumber gain;
};
typedef struct pmgain_pnt_typ pmgain_pnt_typ;

struct pmgain_pnt_typ_ind
{
   OCIInd _atomic;
   OCIInd hv;
   OCIInd gain;
};
typedef struct pmgain_pnt_typ_ind pmgain_pnt_typ_ind;

struct hcal_pmcalib_typ
{
   OCIDate start_v;
   OCIString * pmt_id;
   OCINumber g0;
   OCINumber alpha;
   OCINumber chrg1;
   OCINumber chrg2;
   pmgain_pnt_list * pmgain_pnts_1;
   pmgain_pnt_list * pmgain_pnts_2;
};
typedef struct hcal_pmcalib_typ hcal_pmcalib_typ;

struct hcal_pmcalib_typ_ind
{
   OCIInd _atomic;
   OCIInd start_v;
   OCIInd pmt_id;
   OCIInd g0;
   OCIInd alpha;
   OCIInd chrg1;
   OCIInd chrg2;
   OCIInd pmgain_pnts_1;
   OCIInd pmgain_pnts_2;
};
typedef struct hcal_pmcalib_typ_ind hcal_pmcalib_typ_ind;

struct hcal_cscalib_typ
{
   OCIDate start_v;
   OCINumber cs_hv;
   OCINumber av_curr;
   tile_curr_list * tiles_current;
};
typedef struct hcal_cscalib_typ hcal_cscalib_typ;

struct hcal_cscalib_typ_ind
{
   OCIInd _atomic;
   OCIInd start_v;
   OCIInd cs_hv;
   OCIInd av_curr;
   OCIInd tiles_current;
};
typedef struct hcal_cscalib_typ_ind hcal_cscalib_typ_ind;

struct tile_curr_typ
{
   OCINumber irow;
   OCINumber itile;
   OCINumber curr;
};
typedef struct tile_curr_typ tile_curr_typ;

struct tile_curr_typ_ind
{
   OCIInd _atomic;
   OCIInd irow;
   OCIInd itile;
   OCIInd curr;
};
typedef struct tile_curr_typ_ind tile_curr_typ_ind;

struct ecal_chan_typ
{
   OCIString * chan_type;
   OCINumber chan_x;
   OCINumber chan_y;
   OCIString * mod_id;
   OCINumber mod_chan;
   OCINumber dac_chan;
   OCINumber adc_chan;
   OCINumber cw_pow_conn;
   OCIString * cable_bcode;
   ecal_pmcalib_list * pm_calibs;
   dac_setting_list * dac_settings;
   adc_setting_list * adc_settings;
   OCINumber ledfib_len;
};
typedef struct ecal_chan_typ ecal_chan_typ;

struct ecal_chan_typ_ind
{
   OCIInd _atomic;
   OCIInd chan_type;
   OCIInd chan_x;
   OCIInd chan_y;
   OCIInd mod_id;
   OCIInd mod_chan;
   OCIInd dac_chan;
   OCIInd adc_chan;
   OCIInd cw_pow_conn;
   OCIInd cable_bcode;
   OCIInd pm_calibs;
   OCIInd dac_settings;
   OCIInd adc_settings;
   OCIInd ledfib_len;
};
typedef struct ecal_chan_typ_ind ecal_chan_typ_ind;

struct ecal_pmcalib_typ
{
   OCIDate start_v;
   OCIString * pmt_id;
   OCINumber g0;
   OCINumber alpha;
   OCINumber chrg;
   pmgain_pnt_list * pmgain_pnts;
};
typedef struct ecal_pmcalib_typ ecal_pmcalib_typ;

struct ecal_pmcalib_typ_ind
{
   OCIInd _atomic;
   OCIInd start_v;
   OCIInd pmt_id;
   OCIInd g0;
   OCIInd alpha;
   OCIInd chrg;
   OCIInd pmgain_pnts;
};
typedef struct ecal_pmcalib_typ_ind ecal_pmcalib_typ_ind;

struct hcal_ledpin_typ
{
   OCIString * ledpin_id;
   OCIString * bcode;
   ledpin_calib_list * ledpin_calibs;
};
typedef struct hcal_ledpin_typ hcal_ledpin_typ;

struct hcal_ledpin_typ_ind
{
   OCIInd _atomic;
   OCIInd ledpin_id;
   OCIInd bcode;
   OCIInd ledpin_calibs;
};
typedef struct hcal_ledpin_typ_ind hcal_ledpin_typ_ind;

struct ecal_led_typ
{
   OCINumber led_num;
   OCIString * pin_id;
   OCIString * fire_conn;
   OCIString * dac_conn;
   OCINumber led_dac_chan;
   OCINumber led_ledtsb_chan;
   leddac_setting_list * leddac_settings;
   ledtsb_setting_list * ledtsb_settings;
   ecal_ledpin_calib_list * ledpin_calibs;
   OCINumber pinfib_len;
   OCINumber ledfib_len;
};
typedef struct ecal_led_typ ecal_led_typ;

struct ecal_led_typ_ind
{
   OCIInd _atomic;
   OCIInd led_num;
   OCIInd pin_id;
   OCIInd fire_conn;
   OCIInd dac_conn;
   OCIInd led_dac_chan;
   OCIInd led_ledtsb_chan;
   OCIInd leddac_settings;
   OCIInd ledtsb_settings;
   OCIInd ledpin_calibs;
   OCIInd pinfib_len;
   OCIInd ledfib_len;
};
typedef struct ecal_led_typ_ind ecal_led_typ_ind;

struct ecal_pin_typ
{
   OCIString * pin_id;
   OCINumber ldb_box;
   OCINumber ldb_conn;
   OCINumber adc_chan;
   adc_setting_list * pinadc_settings;
};
typedef struct ecal_pin_typ ecal_pin_typ;

struct ecal_pin_typ_ind
{
   OCIInd _atomic;
   OCIInd pin_id;
   OCIInd ldb_box;
   OCIInd ldb_conn;
   OCIInd adc_chan;
   OCIInd pinadc_settings;
};
typedef struct ecal_pin_typ_ind ecal_pin_typ_ind;

struct leddac_setting_typ
{
   OCIDate d_set;
   OCIDate d_applied;
   OCINumber uled;
};
typedef struct leddac_setting_typ leddac_setting_typ;

struct leddac_setting_typ_ind
{
   OCIInd _atomic;
   OCIInd d_set;
   OCIInd d_applied;
   OCIInd uled;
};
typedef struct leddac_setting_typ_ind leddac_setting_typ_ind;

struct ledtsb_setting_typ
{
   OCIDate d_set;
   OCIDate d_applied;
   OCINumber delay;
   OCIString * tune;
};
typedef struct ledtsb_setting_typ ledtsb_setting_typ;

struct ledtsb_setting_typ_ind
{
   OCIInd _atomic;
   OCIInd d_set;
   OCIInd d_applied;
   OCIInd delay;
   OCIInd tune;
};
typedef struct ledtsb_setting_typ_ind ledtsb_setting_typ_ind;

struct hcal_ledpinref_typ
{
   OCIDate start_v;
   OCIString * ledpin_id;
   OCINumber apmtref;
};
typedef struct hcal_ledpinref_typ hcal_ledpinref_typ;

struct hcal_ledpinref_typ_ind
{
   OCIInd _atomic;
   OCIInd start_v;
   OCIInd ledpin_id;
   OCIInd apmtref;
};
typedef struct hcal_ledpinref_typ_ind hcal_ledpinref_typ_ind;

struct hcal_module_typ
{
   OCINumber mod_x;
   OCINumber mod_y;
   OCIString * mod_id;
   OCIString * bcode;
   hcal_ledpinref_list * ledpin1_ref;
   OCINumber pin1_adc_chan;
   adc_setting_list * pin1_adc_settings;
   OCINumber led1_dac_chan;
   leddac_setting_list * led1_dac_settings;
   OCINumber led1_ledtsb_chan;
   ledtsb_setting_list * led1_ledtsb_settings;
   hcal_ledpinref_list * ledpin2_ref;
   OCINumber pin2_adc_chan;
   adc_setting_list * pin2_adc_settings;
   OCINumber led2_dac_chan;
   leddac_setting_list * led2_dac_settings;
   OCINumber led2_ledtsb_chan;
   ledtsb_setting_list * led2_ledtsb_settings;
   OCINumber integ_conn;
   OCINumber cw_pow_conn;
};
typedef struct hcal_module_typ hcal_module_typ;

struct hcal_module_typ_ind
{
   OCIInd _atomic;
   OCIInd mod_x;
   OCIInd mod_y;
   OCIInd mod_id;
   OCIInd bcode;
   OCIInd ledpin1_ref;
   OCIInd pin1_adc_chan;
   OCIInd pin1_adc_settings;
   OCIInd led1_dac_chan;
   OCIInd led1_dac_settings;
   OCIInd led1_ledtsb_chan;
   OCIInd led1_ledtsb_settings;
   OCIInd ledpin2_ref;
   OCIInd pin2_adc_chan;
   OCIInd pin2_adc_settings;
   OCIInd led2_dac_chan;
   OCIInd led2_dac_settings;
   OCIInd led2_ledtsb_chan;
   OCIInd led2_ledtsb_settings;
   OCIInd integ_conn;
   OCIInd cw_pow_conn;
};
typedef struct hcal_module_typ_ind hcal_module_typ_ind;

struct ecal_ledpin_calib_typ
{
   OCIDate start_v;
   OCINumber uledref;
   OCINumber apmtref;
   OCINumber apinref;
   ledpin_pnt_list * ledpin_pnts;
};
typedef struct ecal_ledpin_calib_typ ecal_ledpin_calib_typ;

struct ecal_ledpin_calib_typ_ind
{
   OCIInd _atomic;
   OCIInd start_v;
   OCIInd uledref;
   OCIInd apmtref;
   OCIInd apinref;
   OCIInd ledpin_pnts;
};
typedef struct ecal_ledpin_calib_typ_ind ecal_ledpin_calib_typ_ind;

struct ledpin_pnt_typ
{
   OCINumber uled;
   OCINumber apinrel;
   OCINumber apmtrel;
};
typedef struct ledpin_pnt_typ ledpin_pnt_typ;

struct ledpin_pnt_typ_ind
{
   OCIInd _atomic;
   OCIInd uled;
   OCIInd apinrel;
   OCIInd apmtrel;
};
typedef struct ledpin_pnt_typ_ind ledpin_pnt_typ_ind;

struct ledpin_calib_typ
{
   OCIDate start_v;
   OCINumber uledref;
   OCINumber apmtref;
   OCINumber apinref;
   ledpin_pnt_list * ledpin_pnts;
};
typedef struct ledpin_calib_typ ledpin_calib_typ;

struct ledpin_calib_typ_ind
{
   OCIInd _atomic;
   OCIInd start_v;
   OCIInd uledref;
   OCIInd apmtref;
   OCIInd apinref;
   OCIInd ledpin_pnts;
};
typedef struct ledpin_calib_typ_ind ledpin_calib_typ_ind;

#endif
