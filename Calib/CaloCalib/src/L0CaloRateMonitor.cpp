// $Id: L0CaloRateMonitor.cpp,v 1.31 2009-06-04 07:53:00 robbep Exp $

// local
#include "L0CaloRateMonitor.h"


// AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IAxis.h"

// Event
#include "Event/L0CaloCandidate.h"
#include "Event/RawEvent.h"
#include "Event/L0DUBase.h"
#include "Event/ODIN.h"

// Declare Algorithm
DECLARE_COMPONENT( L0CaloRateMonitor )

//-----------------------------------------------------------------------------
// Implementation file for class : L0CaloRateMonitor
//
// 31/05/2001 : Olivier Callot
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor
//=============================================================================
L0CaloRateMonitor::L0CaloRateMonitor( const std::string& name, 
				ISvcLocator* pSvcLocator ) : 
  Calo2Dview ( name , pSvcLocator ),
  m_nEvents(0){ 
  declareProperty( "InputDataSuffix"     , m_inputDataSuffix = ""    ) ;  
  declareProperty( "AlarmThresholdRatio" , m_alarmThresholdRatio = 5 ) ;
  declareProperty( "L0HadThreshold" , m_L0HadThreshold = 181 ) ;
  declareProperty( "L0EleThreshold" , m_L0EleThreshold = 136 ) ;
  declareProperty( "L0PhoThreshold" , m_L0PhoThreshold = 136 ) ;
  declareProperty( "UpdateFrequency"     , m_updateFrequency = 1000    ) ;   
}

//=============================================================================
// Standard destructor
//=============================================================================
L0CaloRateMonitor::~L0CaloRateMonitor() { }

//=============================================================================
// Initialisation. 
//=============================================================================
StatusCode L0CaloRateMonitor::initialize() {
	
  m_selHad = 0 ; 
  m_selEle = 0 ; 
  m_selPho = 0 ; 
	
  m_selHadInner = 0 ; 
  m_selEleInner = 0 ; 
  m_selPhoInner = 0 ; 

  m_selEleMiddle = 0 ; 
  m_selPhoMiddle = 0 ; 
	
  m_selHadOuter = 0 ; 
  m_selEleOuter = 0 ; 
  m_selPhoOuter = 0 ; 



  debug() << "==> Initialize" << endmsg;

  // Initialize event counters
  m_nEvents       = 0 ;

  // Retrieve the ECAL detector element, build cards
  m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  // Retrieve the HCAL detector element, build cards
  m_hcal = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );  

  // Book all histograms created by monitoring algorithm
  debug() << "==> Default Monitoring histograms booking " << endmsg;  

  m_bcidHist = GaudiHistoAlg::book( "BCId" , "BCId" , 0 , 3564 , 3564 ) ;
  m_L0HadSelRateHist = GaudiHistoAlg::book( "L0HadSelRate" , "L0HadSelRate" , 0 , 4 , 4 ) ;
  m_L0EleSelRateHist = GaudiHistoAlg::book( "L0EleSelRate" , "L0EleSelRate" , 0 , 4 , 4 ) ;
  m_L0PhoSelRateHist = GaudiHistoAlg::book( "L0PhoSelRate" , "L0PhoSelRate" , 0 , 4 , 4 ) ;
   
   
  return StatusCode::SUCCESS; 
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode L0CaloRateMonitor::execute() {

  // Read input data in default L0Calo container, ie what is sent to 
  // L0DU.
  // Input data suffix is used to select if we read the L0Calo data 
  // (from the L0Calo TELL1) 
  // or the result of the simulation/emulation
  debug() << "Execute will read " 
	  << LHCb::L0CaloCandidateLocation::Default + m_inputDataSuffix 
	  << endmsg ;

  if ( ! exist< LHCb::L0CaloCandidates >
       ( LHCb::L0CaloCandidateLocation::Default + m_inputDataSuffix ) ) {
    Warning( "No data at " + LHCb::L0CaloCandidateLocation::Default + 
	     m_inputDataSuffix ).ignore() ;
    return StatusCode::SUCCESS ;
  }

  LHCb::L0CaloCandidates * candidates = get<LHCb::L0CaloCandidates>
    ( LHCb::L0CaloCandidateLocation::Default + m_inputDataSuffix );
  LHCb::L0CaloCandidates::iterator cand ;

  if ( 0 == candidates ) return StatusCode::SUCCESS ;

  m_nEvents++ ; 

  // Read ODIN bank to obtain BCId and event number
  ulonglong event( 0 ) ; 
  unsigned int BCId( 4000 ) ; 
 
  
  if ( exist< LHCb::ODIN >( LHCb::ODINLocation::Default ) ) {
    LHCb::ODIN * odin = get< LHCb::ODIN >( LHCb::ODINLocation::Default ) ;
    if ( 0 != odin ) {
      event = odin->eventNumber() ; 
      BCId = odin->bunchId() ; 
 
    }
  }

  // Fill BCId histogram
  m_bcidHist -> fill( BCId , 1. ) ;

  // Loop over default candidates
  for ( cand = candidates -> begin() ; candidates -> end() != cand ; 
	++cand ) { 
    LHCb::L0CaloCandidate * theCand = (*cand) ;

    debug() << " Event " << event << " Type  = " << theCand -> type() 
	    << " Et  = " << theCand -> etCode() << endmsg ;

    defaultMonitoring( theCand ) ;
  }

 // Fill regularly the histograms  
  if ( ( m_updateFrequency > 0 ) ) { 
    int goForCheck = (int)m_nEvents % m_updateFrequency ; 
    if ( 0 == goForCheck ) { 
      debug() << "m_nEvents = " << m_nEvents << " go for check ..." << endmsg ; 
 
      double fractionL0Had =  (m_selHad/m_nEvents) ; 
      double fractionL0HadInner =  (m_selHadInner/m_nEvents) ; 	
      double fractionL0HadOuter =  (m_selHadOuter/m_nEvents)  ; 	
 
      double fractionL0Ele =  (m_selEle/m_nEvents)  ; 
      double fractionL0EleInner =  (m_selEleInner/m_nEvents) ; 	
      double fractionL0EleMiddle =  (m_selEleMiddle/m_nEvents)  ; 	
      double fractionL0EleOuter =  (m_selEleOuter/m_nEvents)  ; 	
 
      double fractionL0Pho =  (m_selPho/m_nEvents); 
      double fractionL0PhoInner =  (m_selPhoInner/m_nEvents) ; 	
      double fractionL0PhoMiddle =  (m_selPhoMiddle/m_nEvents) ; 	
      double fractionL0PhoOuter =  (m_selPhoOuter/m_nEvents) ; 	
        
       m_L0HadSelRateHist -> reset() ; 
       m_L0EleSelRateHist -> reset() ; 
       m_L0PhoSelRateHist -> reset() ; 

    
       m_L0HadSelRateHist -> fill(0,fractionL0Had) ;
       m_L0HadSelRateHist -> fill(1,fractionL0HadInner) ; 
       m_L0HadSelRateHist -> fill(3,fractionL0HadOuter) ; 
 
       m_L0EleSelRateHist -> fill(0,fractionL0Ele) ;
       m_L0EleSelRateHist -> fill(1,fractionL0EleInner) ; 
       m_L0EleSelRateHist -> fill(2,fractionL0EleMiddle) ; 
       m_L0EleSelRateHist -> fill(3,fractionL0EleOuter) ; 
       
       m_L0PhoSelRateHist -> fill(0,fractionL0Pho) ;
       m_L0PhoSelRateHist -> fill(1,fractionL0PhoInner) ; 
       m_L0PhoSelRateHist -> fill(2,fractionL0PhoMiddle) ; 
       m_L0PhoSelRateHist -> fill(3,fractionL0PhoOuter) ; 
 
      
      
    }
  }
  
  return StatusCode::SUCCESS; 
}

//=============================================================================
// Finalize.
//=============================================================================
StatusCode L0CaloRateMonitor::finalize() {
 
  info() << "*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_" << endmsg ;
  info() << "Number of events processed: " << m_nEvents << endmsg ;
  info() << "Number of events L0Had selected : " << m_selHad << endmsg ;
  info() << "Number of events L0Ele selected : " << m_selEle << endmsg ;
  info() << "Number of events L0Pho selected : " << m_selPho << endmsg ;
 /*
  float fractionL0Had =  (float)m_selHad/(float)m_nEvents  ; 
  float fractionL0HadInner =  (float)m_selHadInner/(float)m_nEvents  ; 	
  float fractionL0HadOuter =  (float)m_selHadOuter/(float)m_nEvents  ; 	
 
  float fractionL0Ele =  (float)m_selEle/(float)m_nEvents  ; 
  float fractionL0EleInner =  (float)m_selEleInner/(float)m_nEvents  ; 	
  float fractionL0EleMiddle =  (float)m_selEleMiddle/(float)m_nEvents  ; 	
  float fractionL0EleOuter =  (float)m_selEleOuter/(float)m_nEvents  ; 	
 
  float fractionL0Pho =  (float)m_selPho/(float)m_nEvents  ; 
  float fractionL0PhoInner =  (float)m_selPhoInner/(float)m_nEvents  ; 	
  float fractionL0PhoMiddle =  (float)m_selPhoMiddle/(float)m_nEvents  ; 	
  float fractionL0PhoOuter =  (float)m_selPhoOuter/(float)m_nEvents  ; 	
 */
 
  double fractionL0Had =  (m_selHad/m_nEvents) ; 
  double fractionL0HadInner =  (m_selHadInner/m_nEvents) ; 	
  double fractionL0HadOuter =  (m_selHadOuter/m_nEvents)  ; 	
 
  double fractionL0Ele =  (m_selEle/m_nEvents)  ; 
  double fractionL0EleInner =  (m_selEleInner/m_nEvents) ; 	
  double fractionL0EleMiddle =  (m_selEleMiddle/m_nEvents)  ; 	
  double fractionL0EleOuter =  (m_selEleOuter/m_nEvents)  ; 	
 
  double fractionL0Pho =  (m_selPho/m_nEvents); 
  double fractionL0PhoInner =  (m_selPhoInner/m_nEvents) ; 	
  double fractionL0PhoMiddle =  (m_selPhoMiddle/m_nEvents) ; 	
  double fractionL0PhoOuter =  (m_selPhoOuter/m_nEvents) ; 	
 

 
  info() << "Fraction of events L0Had selected : " << fractionL0Had << endmsg ;
  info() << "Fraction of events L0Ele selected : " << fractionL0Ele << endmsg ;
  info() << "Fraction of events L0Pho selected : " << fractionL0Pho << endmsg ;

  info() << "Fraction of events L0Had selected Inner Region : " << fractionL0HadInner << endmsg ;
  info() << "Fraction of events L0Ele selected Inner Region: " << fractionL0EleInner << endmsg ;
  info() << "Fraction of events L0Pho selected Inner Region: " << fractionL0PhoInner << endmsg ;
 
  info() << "Fraction of events L0Ele selected Middle Region: " << fractionL0EleMiddle << endmsg ;
  info() << "Fraction of events L0Pho selected Middle Region: " << fractionL0PhoMiddle << endmsg ;
 
  info() << "Fraction of events L0Had selected Outer Region : " << fractionL0HadOuter  << endmsg ;
  info() << "Fraction of events L0Ele selected Outer Region: " << fractionL0EleOuter<< endmsg ;
  info() << "Fraction of events L0Pho selected Outer Region: " << fractionL0PhoOuter << endmsg ;

  m_L0HadSelRateHist -> reset() ; 
  m_L0EleSelRateHist -> reset() ; 
  m_L0PhoSelRateHist -> reset() ; 
   
  m_L0HadSelRateHist -> fill(0,fractionL0Had) ;
  m_L0HadSelRateHist -> fill(1,fractionL0HadInner) ; 
  m_L0HadSelRateHist -> fill(3,fractionL0HadOuter) ; 
  
  m_L0EleSelRateHist -> fill(0,fractionL0Ele) ;
  m_L0EleSelRateHist -> fill(1,fractionL0EleInner) ; 
  m_L0EleSelRateHist -> fill(2,fractionL0EleMiddle) ; 
  m_L0EleSelRateHist -> fill(3,fractionL0EleOuter) ; 
         
  m_L0PhoSelRateHist -> fill(0,fractionL0Pho) ;
  m_L0PhoSelRateHist -> fill(1,fractionL0PhoInner) ; 
  m_L0PhoSelRateHist -> fill(2,fractionL0PhoMiddle) ; 
  m_L0PhoSelRateHist -> fill(3,fractionL0PhoOuter) ; 
 


   info() << "*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_" << endmsg ;

  
  return Calo2Dview::finalize() ;
}

//=============================================================================
// Fill the counters for the default monitoringfloat
//=============================================================================
void L0CaloRateMonitor::defaultMonitoring( const LHCb::L0CaloCandidate * cand ) { 
  // Cell ID of the candidate
  //LHCb::CaloCellID caloCell = cand -> id() ; 
  
  // Type of the candidate (L0DUBase::CaloType) 
  int type = cand -> type() ;


  // Inner = 2 , Middle = 1 , Outer = 0
  // for HCAL (so for L0Had ): only Inner and Outer 	

    if (type==L0DUBase::CaloType::Hadron && cand -> etCode() >  m_L0HadThreshold) { 
    	m_selHad++ ; 
	if ( cand -> id().area()  == 1) m_selHadInner++ ;
	if ( cand -> id().area()  == 0) m_selHadOuter++ ;	
    }	
    if (type==L0DUBase::CaloType::Electron && cand -> etCode() > m_L0EleThreshold ) { 
    	m_selEle++ ;  
    	if ( cand -> id().area()  == 2) m_selEleInner++ ;
	if ( cand -> id().area()  == 1) m_selEleMiddle++ ;
	if ( cand -> id().area()  == 0) m_selEleOuter++ ;	
    }	
        
    if (type==L0DUBase::CaloType::Photon && cand -> etCode() > m_L0PhoThreshold ) { 
    	m_selPho++ ;  
   	if ( cand -> id().area()  == 2) m_selPhoInner++ ;
	if ( cand -> id().area()  == 1) m_selPhoMiddle++ ;
	if ( cand -> id().area()  == 0) m_selPhoOuter++ ;	
    }	

}

