// $Id: CaloNtpMonitor.h,v 1.6 2009-04-06 16:06:07 odescham Exp $
#ifndef CALONTPMONITOR_H
#define CALONTPMONITOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
// from LHCb
#include "CaloDet/DeCalorimeter.h"
#include "GaudiKernel/IEventTimeDecoder.h"

#include "CaloDAQ/ICaloDataProvider.h"
#undef CALODAQ_ICALODATAPROVIDER_H  // temporary patch
#include "CaloDAQ/ICaloL0DataProvider.h"
#undef CALODAQ_ICALODATAPROVIDER_H  // temporary patch

/** @class CaloNtpMonitor CaloNtpMonitor.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-04-13
 */
class CaloNtpMonitor : public GaudiTupleAlg {
public:
  /// Standard constructor
  CaloNtpMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloNtpMonitor( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  std::string m_detectorName;
  std::string m_readoutTool;
  std::string m_l0ReadoutTool;
  std::vector<int> m_banks;
  int m_max;
  int m_thresh;
  bool m_pin;
  DeCalorimeter* m_calo;
  std::string m_ref;
  std::vector<std::string> m_slots;
  std::string m_pinSlot;
  bool  m_l0;
  bool  m_statusOnTES;

  ICaloDataProvider*  m_daq;
  std::map<std::string,ICaloDataProvider*>  m_daqs;

  ICaloL0DataProvider*  m_l0daq;
  std::map<std::string,ICaloL0DataProvider*>  m_l0daqs;



  IEventTimeDecoder* m_odin;
  std::map<std::string,IEventTimeDecoder*> m_odins;


  bool m_first;
  bool m_force;
  std::map<std::string,bool> m_ok;
  std::map<std::string,bool> m_adj;
  std::map<std::string,bool> m_l0ok;
  std::map<std::string,bool> m_l0adj;
  bool m_inspectAll;
};
#endif // CALONTPMONITOR_H
