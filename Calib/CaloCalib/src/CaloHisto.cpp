// $Id: CaloHisto.cpp,v 1.8 2009-11-08 18:46:20 odescham Exp $
// Include files 
#include "GaudiKernel/RndmGenerators.h"

// local
#include "CaloHisto.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloHisto
//
// setup the histograms parameters
//
// 2007-02-28 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloHisto )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloHisto::CaloHisto( const std::string& type,
                      const std::string& name,
                      const IInterface* parent )
  : GaudiTool ( type, name , parent ){
  declareInterface<CaloHisto>(this);
  declareProperty("Histo"     , m_histo = false);
  declareProperty("Name"      , m_name = "" );   // Expert use
  declareProperty("SplitLeds" , m_split = false);
  declareProperty("Scale"     , m_scale);
  declareProperty("Ranges"     , m_ranges);
  declareProperty("Windows"    , m_windows);
  // for summary histos only
  declareProperty("Order"     , m_order);
  declareProperty("Profile"   , m_profile=false);
  declareProperty("PercentRate", m_rate = 100.);
  declareProperty("InvertWindow",  m_invert = false );
  declareProperty("Detector"    ,  m_det );
  declareProperty("useDBRanges"   ,  m_useDBR=false);
  declareProperty("useDBWindows"  ,  m_useDBW=false);
  declareProperty("NumberOfSigmas", m_nsig=25);
  declareProperty("DefaultRatioBins", m_defBin=100);

  // detector name from parent name
  int ind = 0 ; 
  m_det = name.substr( ind , 4 );
  if ( name.substr(ind,3) == "Prs" ) m_det = "Prs";  
  if ( name.substr(ind,3) == "Spd" ) m_det = "Spd";


  // Default histo name from this->name() set as defined in CaloCalib.cpp
  int index = name.find_first_of(".") +1 ; 
  m_hName = name.substr( index , std::string::npos );
  m_typ = name.substr( index , 3 );
  if( "Sum" == m_typ){ 
    // Summary & Profile name
    m_pName  = "Profile/" + m_hName.substr(3,std::string::npos);
    m_hName  = "Summary/" + m_hName.substr(3,std::string::npos);
    m_ranges[LHCb::CaloCellID()].push_back( 0. );
    m_ranges[LHCb::CaloCellID()].push_back( 13800. );
    m_ranges[LHCb::CaloCellID()].push_back( 13800  );  // 14 bit (CaloCellID::INDEX) (limited to 13800)
  } 
  else if("Rat" == m_typ){
    m_ranges[LHCb::CaloCellID()].push_back( -0.75 );
    m_ranges[LHCb::CaloCellID()].push_back(  1.25 );
    m_ranges[LHCb::CaloCellID()].push_back(  50   );
  }
  else{
    // default : full 12 bits dynamics
    m_ranges[LHCb::CaloCellID()].push_back( -256);
    m_ranges[LHCb::CaloCellID()].push_back( 3840);
    m_ranges[LHCb::CaloCellID()].push_back( 4096);
  }
  // common default
  m_windows[LHCb::CaloCellID()].push_back(-256);
  m_windows[LHCb::CaloCellID()].push_back(3840);
  m_scale=1.;
  m_order=0;
  m_count=0.;
}
//=============================================================================
// Destructor
//=============================================================================
CaloHisto::~CaloHisto() {} 

//=============================================================================
StatusCode CaloHisto::initialize() {
  StatusCode sc = GaudiTool::initialize();
  debug() << "Check Histo setup consistency (default values)" << endmsg;

  // get detector element
  if(m_det == "Ecal")m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  else if(m_det == "Hcal")m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  else if(m_det == "Prs")m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  else if(m_det == "Spd")m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
  else return Error("Unknnow Calorimeter '" + m_det +"'",StatusCode::FAILURE);
  

  
  if(m_profile && !m_histo){
    m_histo = true;
    m_order = -1;
  }
  if( !m_histo && !m_profile){
    m_rate=-1;
  }
  m_count = 0.;  


  if( m_useDBR  || m_useDBW ){
    int nsig = abs(m_nsig); 
    const CaloVector<CellParam>& cells = m_calo->cellParams();
    int index = name().find_first_of(".") +1 ; 
    std::string hName = name().substr( index , std::string::npos );
    for( CaloVector<CellParam>::const_iterator icel = cells.begin() ; cells.end() != icel ; ++icel){
      LHCb::CaloCellID id = icel->cellID();
      double c=0;
      double r=0;
      int nbin = m_defBin;
      bool ratio  = false;
      if( hName == "Ratio" || hName == "SumRatio"){
        c =  icel->ledMoni();
        r =  icel->ledMoniRMS();
        nbin= bins();
        ratio = true;
      }
      else if (hName == "Signal" || hName == "SumSignal" || m_name == "Desert"){
        c = icel->ledData();
        r = icel->ledDataRMS();
      }else{
        warning() << "Undefined DB for data type '" << hName << "' -> use default range" << endmsg;
        break;
      }
      if( r > 0){
        double a   = c - nsig*r;
        double b   = c + nsig*r;
        if(!ratio) {
          a=(double) floor(a);
          b=(double) floor(b);
          nbin = int(b-a);
        }
        if( m_useDBR){
          m_ranges[ id ].push_back( a );
          m_ranges[ id ].push_back( b );
          m_ranges[ id ].push_back( (double) nbin );
          if ( msgLevel( MSG::DEBUG) )debug() << name() << " ranges : " << id << " " << m_ranges[id] << endmsg;
        }
        if( m_useDBW){
          if( m_name == "Desert" && !m_windows[LHCb::CaloCellID()].empty() ){
            m_windows[id].push_back( *(m_windows[LHCb::CaloCellID()].begin())   ); // pedestal min range
            m_windows[id].push_back( *(m_windows[LHCb::CaloCellID()].begin()+1) ); // pedestal max range
            m_windows[ id ].push_back( a ); // Led min range from condDB
            m_windows[ id ].push_back( b ); // Led max range from condDB
          }else if (m_name != "Desert"){
            m_windows[ id ].push_back( a );
            m_windows[ id ].push_back( b );
          }else 
            warning() << "'Desert' does not support condDB window when no default pedestal range is given" << endmsg;
          if ( msgLevel( MSG::DEBUG) )debug() << name() << " windows : " << id <<" "<< m_windows[id] << endmsg;
        }        
      }
    }
    if(m_useDBR)info() << "Added " << m_ranges.size() << " ranges for " << hName << endmsg;
    if(m_useDBW)info() << "Added " << m_windows.size() << " windows for " << hName << endmsg;
  }  


  for(std::map<LHCb::CaloCellID , std::vector<double>  >::iterator it = m_ranges.begin() ; it != m_ranges.end() ; ++it) {
    std::vector<double>   ranges = (*it).second;
    if( ranges.size() !=3){
      error() << "Expect 3 values for the range (min, max, bin) for CaloCellID index " << (*it).first << endmsg;
      return StatusCode::FAILURE;
    }
  }




  for(std::map<LHCb::CaloCellID , std::vector<double>  >::iterator it = m_windows.begin() ; it != m_windows.end() ; ++it) {
    std::vector<double>   windows = (*it).second;
    if(windows.size()%2 != 0){
      error() << "Expect even number of  values for the window [vector(min, max)] for CaloCellID index " <<(*it).first << endmsg;
      return StatusCode::FAILURE;
    }
  }
  

  if(min() >= max() ){
    error() << "Range is not correct (min >= max) " << m_ranges[LHCb::CaloCellID()] << endmsg;
    return StatusCode::FAILURE;
  }
  if(bins() <=0){
    error() << "Bining is not correct  " << bins() << endmsg;
    return StatusCode::FAILURE;
  }
  if(m_order<-1){
    error() << "Order is not correct  " << m_order << endmsg;
    return StatusCode::FAILURE;
  }
  if(m_scale<=0){
    error() << "Scale is not correct  " << m_scale << endmsg;
    return StatusCode::FAILURE;
  }  
  debug() << " Histo setup is consistent" << endmsg;

  // external renaming 
  if( m_name != "" ){
    unsigned int index = m_hName.find_last_of("/") +1 ;  
    m_hName = ( 0 == index) ? m_name : "Summary/" + m_name;
    m_pName = "Profile/" + m_name;
  }

  return sc;
}
