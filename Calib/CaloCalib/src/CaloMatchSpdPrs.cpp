// $Id: CaloMatchSpdPrs.cpp,v 1.4 2009-10-27 16:49:40 odescham Exp $
// Include files 


// local
#include "CaloMatchSpdPrs.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloMatchSpdPrs
//
// 2008-08-18 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloMatchSpdPrs )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloMatchSpdPrs::CaloMatchSpdPrs( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
{
  declareProperty( "ReadoutTool"           ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "HistoMax"              ,  m_max = 100 );
  declareProperty( "HistoBin"              ,  m_bin = 100 );
  declareProperty( "PrsThreshold"          ,  m_prsThreshold = 20 );
}
//=============================================================================
// Destructor
//=============================================================================
CaloMatchSpdPrs::~CaloMatchSpdPrs() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloMatchSpdPrs::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  m_spd = tool<ICaloDataProvider>( m_readoutTool , "SpdReadoutTool" , this );
  m_prs = tool<ICaloDataProvider>( m_readoutTool , "PrsReadoutTool" , this );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloMatchSpdPrs::execute() {

  debug() << "==> Execute" << endmsg;

  if(!m_prs->ok()){
    debug() << "The Readout tool cannot load the rawEvent for Prs  ==> STOP processing" << endmsg;
    return StatusCode::SUCCESS;
  }
  if(!m_spd->ok()){
    debug() << "The Readout tool cannot load the rawEvent for Spd  ==> STOP processing" << endmsg;
    return StatusCode::SUCCESS;
  }


  long prs = 0;
  long spd = 0;
  long spdprs = 0;
  
  const CaloVector<LHCb::CaloAdc>& spds = (m_spd->adcs());
  const CaloVector<LHCb::CaloAdc>& prss = (m_prs->adcs());
  debug() << "SPD hits : " << spds.size() << " Prs hits : "  << prss.size() << endmsg;
  for(CaloVector<LHCb::CaloAdc>::const_iterator ispd = spds.begin();ispd!= spds.end();++ispd){
     if( (*ispd).adc() == 0)continue;
     if( prss[ (*ispd).cellID()].adc() > m_prsThreshold ){
       spdprs++;
     }else{
       spd++;
     }
   }
   for(CaloVector<LHCb::CaloAdc>::const_iterator iprs = prss.begin();iprs!= prss.end();++iprs){
     if( (*iprs).adc() < m_prsThreshold )continue;
     if( spds[ (*iprs).cellID() ].adc() == 0)prs++;
   }

   std::string thresh = Gaudi::Utils::toString(m_prsThreshold) ;
   plot1D( prs , "Prs/1","Prs and NO matching Spd multiplicity (Prs threshold = " + thresh + " ADCs)" , 0., m_max, m_bin);
   plot1D( spd , "Spd/1","Spd and NO matching Prs Multiplicity "  , 0., m_max, m_bin); 
   plot1D( spdprs , "SpdPrs/1","Spd AND matching Prs  multiplicity (prs threshold = " + thresh +" ADCs)", 0., m_max, m_bin);
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloMatchSpdPrs::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
