// $Id: CaloDisplay.h,v 1.7 2009-10-27 16:49:40 odescham Exp $
#ifndef CALODISPLAY_H
#define CALODISPLAY_H 1

// Include files
// from Gaudi
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDAQ/ICaloL0DataProvider.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "CaloUtils/Calo2Dview.h"

/** @class CaloDisplay CaloDisplay.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-01-18
 */
class CaloDisplay : public Calo2Dview {
public:
  /// Standard constructor
  CaloDisplay( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloDisplay( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  long m_sequenceCounter;
  long m_refreshCounter;
  double m_scaleCounter;
  long m_sequence;
  long m_refresh;
  double m_scale;
  std::string m_readoutTool;
  std::string m_l0readoutTool;
  std::string m_detectorName;
  double m_l0Threshold;
  double  m_l0Offset;
  bool m_statusOnTES;
  long m_filter;
  bool m_dynTitle;

  ICaloDataProvider* m_daq;
  ICaloL0DataProvider* m_l0daq;
  IEventTimeDecoder* m_odin;

  std::string m_tit;
};
#endif // CALODISPLAY_H
