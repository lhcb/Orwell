// $Id: CaloOccupancy.cpp,v 0.0 2014-11-28 09:42:03 jmarchan Exp $
// Include files 
#include <fstream>
#include "GaudiUtils/Aida2ROOT.h"
// from LHCb
#include "Event/ODIN.h"
#include "CaloUtils/CaloAlgUtils.h"
// local
#include "CaloOccupancy.h"

using namespace LHCb;
using namespace Gaudi::Units;
using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : CaloOccupancy
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloOccupancy )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloOccupancy::CaloOccupancy( const std::string& name,
			ISvcLocator* pSvcLocator)
  : Calo2Dview ( name , pSvcLocator ),
    m_currPat(-1),
    m_ok(true),
    m_nPattern(0)
{
  declareProperty( "ReadoutTool"    , m_readoutTool  = "CaloDataProvider" );
  declareProperty( "Detector"       , m_detectorName = "Spd" );
  declareProperty( "StatusOnTES"    , m_statusOnTES = true);
  declareProperty( "CheckNCell"     , m_cellCheck = 50);
  declareProperty( "Sequence"       , m_sequence      );
  declareProperty( "SplitBXParity"  , m_splitParity = false );
  declareProperty( "Save2DProfile"  , m_profile2D = false );
  declareProperty( "ActiveCounters" , m_activeCounter = false ); // active debugging counter stat in finalize (offline)
  declareProperty( "TimeRange"      , m_timeRange = 600 );
  declareProperty( "Thresholds"     , m_threshold = { 10, 12, 14, 16, 18, 20 } );

  setOneDimension(true); // produce 1D views by default
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name );
}


//=============================================================================
// Destructor
//=============================================================================
CaloOccupancy::~CaloOccupancy() {} 


//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloOccupancy::initialize() {
  StatusCode sc = Calo2Dview::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  //--- Get Detector Element
  m_det = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( m_detectorName ) );
  if( !m_det )return StatusCode::FAILURE;

  //--- Get Calo readout tool
  m_daq = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool", this );
  //--- Get Odin decoder
  m_odin = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder",this);

  m_nChannels = m_det->numberOfCells();
  // default sequence if not defined
  if( 0 == m_sequence.size() ){
    info() << "NO sequence defined : assume a single sequence with all channels" << endmsg;
    const CaloVector<CellParam>& cells = m_det->cellParams();
    for(CaloVector<CellParam>::const_iterator  ic = cells.begin() ; ic != cells.end() ; ++ic){
      const LHCb::CaloCellID id = (*ic).cellID();
      if( id.isPin() )continue;
      m_sequence["0"].push_back( id.index() );
    }
  } 
  
  //--- Read Pattern file -----------------------
  m_cellPattern.clear();
  //
  m_nPattern = m_sequence.size();

  unsigned int nCells = 0;
  for( std::map<std::string,std::vector<int> >::iterator it = m_sequence.begin() ; it != m_sequence.end(); ++it){
    std::istringstream is( ((*it).first).c_str() );
    int pattern ;
    is >> pattern;
    const std::vector<int>& cells = (*it).second;
    nCells += cells.size();
    for( std::vector<int>::const_iterator ic = cells.begin() ; ic != cells.end() ; ++ic){
      const LHCb::CaloCellID cellID = LHCb::CaloCellID(*ic);
      m_cellPattern.addEntry(pattern , cellID ); //gives what pattern a cell belongs to
    }
  }
  if( nCells != m_nChannels){
    m_ok = false;
    info() << "Sequence size : " << m_sequence.size() << " / Ncells :" << m_nChannels << endmsg;
    return Warning("Incomplete LED sequence -> No processing",StatusCode::SUCCESS);
  }  
  
  //----------------------------------------------
  // time slot
  m_timeSlot = rootInTES();
  if( "" == m_timeSlot )m_timeSlot = "T0";

  hTimeInit = NULL;
  hTimeEnd  = NULL;
  hCell     = NULL;
  if (m_profile2D) 
    hOccADC = NULL;
  for (int i=0 ; i<6 ; i++)
    hOcc[i] = NULL;
  
  return StatusCode::SUCCESS;
}
  



//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloOccupancy::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  if( !m_ok) return StatusCode::SUCCESS;

  StatusCode sc;
  // get adcs
  if(!m_daq->ok())return StatusCode::SUCCESS;  // OD getBanks no longer needed  
  const CaloVector<LHCb::CaloAdc>& adcs = m_daq->adcs();
  if(m_statusOnTES)  m_daq->putStatusOnTES(); 

  //--- Get ODIN
  m_odin->getTime();
  long bunch=0;
  longlong eventTime=0;//m_odin->getTime();//0;
  std::string parity = "";
  bool isBeamXing = false;
  if( exist<ODIN>(ODINLocation::Default) ){
    ODIN* odin = get<ODIN> (ODINLocation::Default);
    bunch = odin->bunchId();
    eventTime = (longlong) odin->eventTime().ns() * 1/1e9;
    int bxParity = bunch%2;
    if(m_splitParity)parity =  (bxParity == 0) ? "EvenBX/" : "OddBX/";
    if(m_activeCounter)counter("Parity : " + parity)+=1;
    if (odin->bunchCrossingType()==LHCb::ODIN::BeamCrossing) isBeamXing = true;
  }
  else if(m_splitParity)
    return Warning("ODIN bank not found - cannot analyze data per BX parity",StatusCode::SUCCESS);
  
  if (!isBeamXing) return StatusCode::SUCCESS;


  //------- DETERMINE CURRENT PATTERN -------------------------------------
  //-----------------------------------------------------------------------    
  m_currPat = findPattern(adcs);
  if( m_currPat < 0 ){
    if( adcs.size() > m_cellCheck){
      if(m_activeCounter)counter("Pattern not found") += 1;
      return Warning( "Pattern not found in non empty flash event",StatusCode::SUCCESS);
    } else
      if(m_activeCounter)counter("Empty flash event") += 1;
  }
  
  //-----------------------------------------------------------------------
  //------- FILL MONITORING HISTOGRAMS ------------------------------------
  //----------------------------------------------------------------------- 

  const HistoID uTimeInit = HistoID(parity + "timeInit");
  const HistoID uTimeEnd  = HistoID(parity + "timeEnd");

  HistoID uOcc[6];
  for (int i=0 ; i<6 ; i++) {
    char title[50];
    sprintf(title,"occ%d",i);
    uOcc[i] = HistoID(parity + string(title));
    hOcc[i] = GaudiHistoAlg::bookProfile1D( uOcc[i], title,  0, 12000, 12000);
  }
  const HistoID uCell     = HistoID(parity + "cell");

  hTimeInit = GaudiHistoAlg::book1D (uTimeInit,   "TimeInit",   0, 2, 2); 
  hTimeEnd  = GaudiHistoAlg::book1D (uTimeEnd,    "TimeEnd",    0, 2, 2); 

  if (m_profile2D) {
    const HistoID uOccADC   = HistoID(parity + "occADC");
    hOccADC   = GaudiHistoAlg::bookProfile2D( uOccADC,  "OccupancyADC",  0, 12000, 12000,0,200,100);
  }
  hCell     = GaudiHistoAlg::book1D (uCell,    "Cell",    0, 12000, 12000); 

  if (NULL!=hCell && hCell->allEntries()==0) {
    hTimeInit->setTitle("TimeInit");
    hTimeEnd->setTitle("TimeEnd"); 
  }

  // loop over all channels (as defined in the sequence)
  for( std::map<std::string,std::vector<int> >::const_iterator it = m_sequence.begin() ; it != m_sequence.end(); ++it){
    const std::vector<int>& cells = (*it).second;
    for( std::vector<int>::const_iterator ic = cells.begin() ; ic != cells.end() ; ++ic){
      const LHCb::CaloCellID& cellID = LHCb::CaloCellID( *ic );
      double adc = (double) m_daq->adc(cellID);
            
      if(m_cellPattern[cellID] == m_currPat) {

	if (NULL!=hCell)  hCell->fill( cellID.index() );

	if (m_profile2D) {
	  for (int i=0 ; i<100 ; i++) {
	    int k = 2*i;
	    double value = (adc > double(k)) ? 1. : 0.;
	    if (NULL!=hOccADC)  hOccADC->fill( cellID.index(), (double)k, value );
	  }
	}
	
	for (unsigned int i=0 ; i<m_threshold.size() ; i++) {
	  double value = (adc > m_threshold.at(i)) ? 1. : 0.;
	  if (NULL!=hOcc[i])  hOcc[i]->fill( cellID.index(), value );
	}

      }  

    }
  }
  
  std::string time = std::to_string(eventTime);
  if (NULL != hTimeInit ) {
    std::string strInit("TimeInit");
    std::string timeInitHistStr = hTimeInit->title();
    if (timeInitHistStr.compare("TimeInit")==0) hTimeInit->setTitle(time);
    else {
      longlong timeInitHist = atoi(timeInitHistStr.c_str());
      if (timeInitHist>eventTime) hTimeInit->setTitle(time);
    }
  }
  if (NULL != hTimeEnd ) {
    std::string strEnd("TimeEnd");
    std::string timeEndHistStr = hTimeEnd->title();
    if (timeEndHistStr.compare(strEnd)==0) hTimeEnd->setTitle(time);
    else {
      longlong timeEndHist = atoi(timeEndHistStr.c_str());
      if (timeEndHist<eventTime) hTimeEnd->setTitle(time);
    }
  }  

  return StatusCode::SUCCESS;

}



//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloOccupancy::finalize() {
  
  StatusCode sc;
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return Calo2Dview::finalize();  // must be called after all other actions
  
}

//=============================================================================
//  findPattern
//=============================================================================

int CaloOccupancy::findPattern(const CaloVector<LHCb::CaloAdc>& adcs) {
  if(m_activeCounter)counter("Pattern finder") += 1;
  if( m_sequence.size() == 0)return -1;
  if( m_sequence.size() == 1)return 0;  

  //Arrays for counting how many cells have a 1 in each pattern
  std::vector<unsigned int> countXPattern;
  for( int i = 0 ; i < m_nPattern ; ++i){ 
    countXPattern.push_back( 0 );  
  } 
  int initPattern = -1;
  for( CaloVector<LHCb::CaloAdc>::const_iterator iAdc = adcs.begin(); iAdc != adcs.end(); ++iAdc ) {
    //if( (double) (*iAdc).adc() < m_thresh )continue;
    const LHCb::CaloCellID& cellID = (*iAdc).cellID();
    if(++countXPattern[m_cellPattern[cellID]]> m_cellCheck) {
      initPattern = m_cellPattern[cellID];
      break;
    }
  }
  //----------------------------------------------------------------------------
  //--------------- Debugging printout -----------------------------------------
  //----------------------------------------------------------------------------
  for(int i=0; i<m_nPattern; i++) {
    debug() << "Count in pattern " << i  << " : " << countXPattern[i] << endmsg;  
    if(m_activeCounter)counter("#Flash in pattern " + Gaudi::Utils::toString(i)) += (countXPattern[i] > m_cellCheck) ? 1 :0 ;
;
  }
  return initPattern;
}
