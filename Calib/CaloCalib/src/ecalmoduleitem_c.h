#ifndef ECALMODULEITEM_C_ORACLE
#define ECALMODULEITEM_C_ORACLE

#ifndef OCI_ORACLE
#include <oci.h>
#endif

typedef OCIRef box_module_item_ref;
typedef OCIRef box_chan_item_ref;
typedef OCIRef pmt_item_ref;
typedef OCIRef pmt_res_item_ref;
typedef OCIRef pmt_val_ref;
typedef OCIRef cw_item_ref;
typedef OCITable box_chan_lst;
typedef OCITable pmt_lst_res;
typedef OCITable pmt_val_tab;

struct box_module_item
{
   OCIString * mod_ls_name;
   OCIString * mod_type;
   OCINumber mod_nch;
   box_chan_lst * mod_chans;
};
typedef struct box_module_item box_module_item;

struct box_module_item_ind
{
   OCIInd _atomic;
   OCIInd mod_ls_name;
   OCIInd mod_type;
   OCIInd mod_nch;
   OCIInd mod_chans;
};
typedef struct box_module_item_ind box_module_item_ind;

struct box_chan_item
{
   OCINumber chan_n;
   OCINumber mip;
   OCINumber gain;
   OCINumber res1;
   OCINumber res2;
};
typedef struct box_chan_item box_chan_item;

struct box_chan_item_ind
{
   OCIInd _atomic;
   OCIInd chan_n;
   OCIInd mip;
   OCIInd gain;
   OCIInd res1;
   OCIInd res2;
};
typedef struct box_chan_item_ind box_chan_item_ind;

struct pmt_item
{
   OCIString * pmt_id;
   OCINumber batch;
   OCINumber box;
   OCINumber place;
   pmt_lst_res * results;
   OCIString * l1;
   OCIString * l2;
   OCIString * l3;
   OCIString * l4;
   OCIString * l5;
   OCIString * location;
   OCINumber cwname;
};
typedef struct pmt_item pmt_item;

struct pmt_item_ind
{
   OCIInd _atomic;
   OCIInd pmt_id;
   OCIInd batch;
   OCIInd box;
   OCIInd place;
   OCIInd results;
   OCIInd l1;
   OCIInd l2;
   OCIInd l3;
   OCIInd l4;
   OCIInd l5;
   OCIInd location;
   OCIInd cwname;
};
typedef struct pmt_item_ind pmt_item_ind;

struct pmt_res_item
{
   OCIString * src;
   OCIDate c_date;
   OCIString * slot;
   OCINumber report_uid;
   OCIString * report;
   OCINumber cls;
   OCINumber als;
   OCINumber dcr1m;
   OCINumber g0;
   OCINumber alpha;
   OCINumber qeff;
   OCINumber chisq;
   OCINumber numf;
   pmt_val_tab * g_data;
};
typedef struct pmt_res_item pmt_res_item;

struct pmt_res_item_ind
{
   OCIInd _atomic;
   OCIInd src;
   OCIInd c_date;
   OCIInd slot;
   OCIInd report_uid;
   OCIInd report;
   OCIInd cls;
   OCIInd als;
   OCIInd dcr1m;
   OCIInd g0;
   OCIInd alpha;
   OCIInd qeff;
   OCIInd chisq;
   OCIInd numf;
   OCIInd g_data;
};
typedef struct pmt_res_item_ind pmt_res_item_ind;

struct pmt_val
{
   OCIString * descr;
   OCINumber value;
};
typedef struct pmt_val pmt_val;

struct pmt_val_ind
{
   OCIInd _atomic;
   OCIInd descr;
   OCIInd value;
};
typedef struct pmt_val_ind pmt_val_ind;

struct cw_item
{
   OCINumber cwname;
   OCINumber type;
   OCINumber run;
   OCIDate run_date;
   OCINumber ratio_rel;
   OCINumber pmt_amp;
   OCINumber pmt_wid;
   OCIString * comments;
   OCIString * pmt_id;
};
typedef struct cw_item cw_item;

struct cw_item_ind
{
   OCIInd _atomic;
   OCIInd cwname;
   OCIInd type;
   OCIInd run;
   OCIInd run_date;
   OCIInd ratio_rel;
   OCIInd pmt_amp;
   OCIInd pmt_wid;
   OCIInd comments;
   OCIInd pmt_id;
};
typedef struct cw_item_ind cw_item_ind;

#endif
