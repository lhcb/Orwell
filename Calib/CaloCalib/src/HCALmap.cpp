#include "TROOT.h"
#include "TFile.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include "TMinuit.h"
#include "TMatrixD.h"

#include <math.h>
#include <map>

#include "HCALmap.h"

double stepX_hcal=262.6; //mm
double stepY_hcal=262.6; //mm

int PINcellIDs[64]={
  0,0,0,12288,12352,12416,12480,12289,12353,12417,12481,12290,12354,12418,12482,12291,
  12355,12419,12483,12292,12356,12420,12484,12293,12357,12421,12485,12294,12358,0,0,0,
  0,0,0,12592,12656,12720,12784,12593,12657,12721,12785,12594,12658,12722,12786,12595,
  12659,12723,12787,12596,12660,12724,12788,12597,12661,12725,12789,12598,12662,0,0,0
};
std::map<int,int> cellIDPINs;

int HCALcellIDs[NHCELLS]={0};
std::map<int,int> cellIDHCAL;

bool isHCALcell(int z, int x, int y){
  if(z!=1 && z!=2 && z!=4)return false;
  if(1==z){
    if(x<0 || x>31 || y<3 || y>28)return false;
    if(x>=8 && x<=23 && y>=9 && y<=22)return false;
  }else if(2==z){
    if(x<0 || x>31 || y<2 || y>29)return false;
    if(x>=14 && x<=17 && y>=14 && y<=17)return false;
  }else if(4==z){
    if( (x!=0 && x!=31) || y<3 || y>28)return false;
  }

  return true;
}

bool HCALcell2xy(int z, int x, int y, double& xc, double& yc){ // coordinates in mm; inner==(z==2), outer==(z==1)
  if(!isHCALcell(z,x,y)){
    return false;
  }else{
    xc=((double)x-15.5)*(stepX_hcal/z);  // at z == 2 (inner) size is 1/2 of stepX_hcal
    yc=((double)y-15.5)*(stepY_hcal/z);  // at z == 2 (inner) size is 1/2 of stepY_hcal
    return true;
  }
}

bool HCALxy2cell(int& z, int& x, int& y, double xc, double yc){ // coordinates in mm; inner==(z==2), outer==(z==1)
  for(z=1; z<=2; ++z){
    x=(int)floor(xc/ (stepX_hcal/z) )+16;  // at z == 2 (inner) size is 1/2 of stepX_hcal
    y=(int)floor(yc/ (stepY_hcal/z) )+16;  // at z == 2 (inner) size is 1/2 of stepY_hcal
    if(isHCALcell(z,x,y))return true;
  }
  return false;
}

int iHCAL2iPIN(int iHCAL){
  return cellID2iPIN(iHCAL2cellID(iHCAL));
}

int cellID2iPIN(int cellID){
  int z,x,y;
  zxy(cellID,z,x,y);
  return zxy2iPIN(z,x,y);
}

int zxy2iPIN(int &z, int &x, int &y){
  if(isHCALcell(z,x,y)){
    int iPIN=y;
    if(2==z)iPIN=y/2+8;
    if(x>15)iPIN+=32;
    return iPIN;
  }else return 0;
}

bool zxy(int cellid, int& z, int& x, int& y){
  int idet=(cellid&0xC000)>>14;
  z=(cellid&0x3000)>>12;
  z+=1;
  y=(cellid&0x0FC0)>>6;
  x= cellid&0x003F  ;
  return (idet==3);
}

bool zxy_H(int cellid, int& z, int& x, int& y){
  return zxy(cellid, z, x, y);
}

int cellid(int z, int x, int y){
  int c=0;
  c += (x & 0x003F);
  c += (y & 0x003F) <<6;
  c += ((z-1) & 0x0003) <<12;
  return c;
}

int cellid_H(int z, int x, int y){
  return cellid(z,x,y);
}

int iPINofcellID(int cellID){
  if(0==cellIDPINs.size()){
    for(int i=0; i<64; ++i){
      if(PINcellIDs[i]>0)cellIDPINs.insert(std::pair<int,int>(PINcellIDs[i],i));
    }
  }
  return cellIDPINs[cellID];
}

int cellIDofPIN(int iPIN){
  return PINcellIDs[iPIN];
}

bool iPIN2zxy(int iPIN, int& z, int& x, int& y){
  if(iPIN<0 || iPIN>63)return false;
  int cid=cellIDofPIN(iPIN);
  if(cid<10000)return false;
  zxy(cid,z,x,y);
  return isHCALcell(z,x,y);
}

int iHCAL2cellID(int iHCAL){
  if(0==HCALcellIDs[0]){
    int nhcal=0;
    for(int iz=1; iz<=2; ++iz){
      for(int ix=0; ix<32; ++ix){
        for(int iy=0; iy<32; ++iy){
          if(isHCALcell(iz,ix,iy)){
            HCALcellIDs[nhcal]=cellid(iz,ix,iy);
            ++nhcal;
          }
        }
      }
    }
    if(nhcal!=NHCELLS){
      printf("impossible: nhcal!=NHCELLS!\n");
      return 0;
    }
  }

  if(iHCAL>=0 && iHCAL<NHCELLS)return HCALcellIDs[iHCAL];
  else return 0;
}

int cellID2iHCAL(int cellID){
  if(0==cellIDHCAL.size()){
    for(int i=0; i<NHCELLS; ++i){
      cellIDHCAL.insert(std::pair<int,int>(iHCAL2cellID(i),i));
    }
  }
  return cellIDHCAL[cellID];
}

int zxy2iHCAL(int iz, int ix, int iy){
  if(iz<1 || iz>2)return -1;
  if(!isHCALcell(iz,ix,iy)) return -1;
  int cid=cellid_H(iz,ix,iy);
  int iHCAL=cellID2iHCAL(cid);
  return iHCAL;
}

TH2D* creHCALmap(const char* name, const char* title){
  //char cur[256]; cur[254]=cur[255]='\0';
  //strncpy(cur, gDirectory->GetPath(), 240);
  TH2D* hmap=(TH2D*)gROOT->FindObject(name);
  if(hmap){
    printf("creHCALmap: %s already exists, deleting and re-creating...\n",name);
    hmap->Delete();
  }
  
  hmap=new TH2D(name,title,64,-4201.6,4201.6, 52,-3413.8,3413.8);
  hmap->SetOption("colz");
  return hmap;
}

TH2F* creHCALmapF(const char* name, const char* title){
  char cur[256]; cur[254]=cur[255]='\0';
  strncpy(cur, gDirectory->GetPath(), 240);

  TH2F* map=new TH2F(name,title,64,-4201.6,4201.6, 52,-3413.8,3413.8);
  return map;
}

bool isHCALmapH(TH2* hmap){
  if(!hmap)return false;

  int nx=0, ny=0;
  double x1=0,x2=0,y1=0,y2=0;
  nx=hmap->GetXaxis()->GetNbins();
  x1=hmap->GetXaxis()->GetXmin();
  x2=hmap->GetXaxis()->GetXmax();
  ny=hmap->GetYaxis()->GetNbins();
  y1=hmap->GetYaxis()->GetXmin();
  y2=hmap->GetYaxis()->GetXmax();
  if(64==nx && -4201.6==x1 && 4201.6==x2 
     && 52==ny && -3413.8==y1 && 3413.8==y2)return true;
  return false;
}

bool isHCALmapO(TObject* ob){
  if(!ob)return false;

  const char *classnam;//, *objnam;
  classnam=ob->IsA()->GetName();
  //objnam=ob->GetName();

  int cmp=strncmp("TH2",classnam,3);
  //printf("classnam=%s, objnam=%s, cmp=%d\n", classnam, objnam, cmp);
  if(0!=cmp)return false;

  TH2* hmap=(TH2*)ob;

  int nx=0, ny=0;
  double x1=0,x2=0,y1=0,y2=0;
  nx=hmap->GetXaxis()->GetNbins();
  x1=hmap->GetXaxis()->GetXmin();
  x2=hmap->GetXaxis()->GetXmax();
  ny=hmap->GetYaxis()->GetNbins();
  y1=hmap->GetYaxis()->GetXmin();
  y2=hmap->GetYaxis()->GetXmax();
  //if(64==nx && -32==x1 && 32==x2 && 52==ny && -26==y1 && 26==y2)return true;
  //if(64==nx && 52==ny )return true;
  if(64==nx && -4201.6==x1 && 4201.6==x2 
     && 52==ny && -3413.8==y1 && 3413.8==y2)return true;
  return false;
}

void saveHCALallH(const char* fnam, ...){
  TH1D* h1[100];
  TH2D* h2[100];
  for(int i=0; i<100;++i){h1[i]=NULL; h2[i]=NULL;}

  va_list vl;
  va_start(vl, fnam);

  int n=0;
  const char* p=0;
  while(true){
    try{
      h2[n]=va_arg(vl,TH2D*);
      if(NULL==h2[n])break;
      p=h2[n]->GetName();
      if(!p) break;
    }
    catch (...){
      break;
    }
    h2[n]->SetOption("colz");
    if(isHCALmapH(h2[n])){
      h1[n]=makeHCALmap_1H(h2[n]);
    }
    n++;
  }

  va_end(vl);

  TFile* wfil=new TFile(fnam,"recreate");
  for(int i=0; i<n;++i){
    if(h2[i])h2[i]->Write();
    if(h1[i])h1[i]->Write();
  }
  wfil->Close();
}

void makeavrmsH(TH2D* h0, TH2D* h1, TH2D* h2){
  for(int ibinx=1; ibinx<=64; ++ibinx){
    for(int ibiny=1; ibiny<=52; ++ibiny){
      Double_t s0=h0->GetBinContent(ibinx, ibiny);
      Double_t s1=h1->GetBinContent(ibinx, ibiny);
      Double_t s2=h2->GetBinContent(ibinx, ibiny);
      if(s0>0){
        Double_t av=s1/s0;
        Double_t rms=sqrt(fabs(s2/s0-av*av));
        h1->SetBinContent(ibinx, ibiny, av);
        h2->SetBinContent(ibinx, ibiny, rms);
      }
    }
  }
}

void makevarH(TH2D* h, TProfile* t[32][32][3]){
  //char nm[256];
  for(int iz=1; iz<3; ++iz){
    for(int iy=0; iy<32; ++iy){
      for(int ix=0; ix<32; ++ix){
        if(isHCALcell(iz,ix,iy)){
          int nbin=t[ix][iy][iz]->GetXaxis()->GetNbins();
          Double_t mxA=-100000;
          Double_t mnA= 100000;
          Double_t avA=0;
          Int_t nA=0;
          for(int ibin=1; ibin<=nbin; ++ibin){
            Double_t A=t[ix][iy][iz]->GetBinContent(ibin);
            Double_t N=t[ix][iy][iz]->GetBinEntries(ibin);
            if(N>0){
              avA+=A;
              if(mxA<A)mxA=A;
              if(mnA>A)mnA=A;
              nA++;
            }
          }
          if(nA>0){
            avA/=nA;
            if(avA>0){
              Double_t var=(mxA-mnA)/avA;
              fillHCALhitmapH(h,iz,ix,iy,var);
            }
          }
        }
      }
    }
  }
}

int FEBwd2in(int wd){
  static int pre[32]={ 0, 4, 8,12,16,20,24,28, 1, 5, 9,13,17,21,25,29, 2, 6,10,14,18,22,26,30, 3, 7,11,15,19,23,27,31};
  if(wd>=0 && wd<32)return pre[wd];
  else return -1;
}

int FEBin2wd(int in){
  static int pre[32]={ 0, 8,16,24, 1, 9,17,25, 2,10,18,26, 3,11,19,27, 4,12,20,28, 5,13,21,29, 6,14,22,30, 7,15,23,31};
  if(in>=0 && in<32)return pre[in];
  else return -1;
}

static int mod_prodnum[2][26]={
  {2, 7,25,26, 5, 3,16, 8,15,11,10,17,13,14, 6,18,20,19, 9,12,21,22,24,23, 1, 4},
  {52,49,50,51,47,48,37,38,42,41,44,43,39,40,35,36,45,46,33,34,29,30,31,28,27,32}
};

static int mod_ypos[53]={
  0, /* 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26*/ 
  27, 3, 8,28, 7,17, 4,10,21,13,12,22,15,16,11, 9,14,18,20,19,23,24,26,25, 5, 6,
  /*  27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52*/
  27,26,23,24,25,28,21,22,17,18, 9,10,15,16,12,11,14,13,19,20, 7, 8, 4, 5, 6, 3
};

int xpos(int modnum){
  if(modnum>26 && modnum<=52)return 31;
  else if(modnum>0  && modnum<=26)return 0;
  else return -1;
}

int ypos(int modnum){
  if(modnum>0 && modnum<=52)return mod_ypos[modnum];
  else return -1;
}

int prodnum(int side, int ypos){
  int iside=0;
  if(side!=0)iside=1;
  if(ypos<3 || ypos>28)return(-1);
  return mod_prodnum[iside][ypos-3];
}

