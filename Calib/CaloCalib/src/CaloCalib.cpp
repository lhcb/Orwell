// $Id: CaloCalib.cpp,v 1.31 2010-10-11 10:54:51 odescham Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/ODIN.h"
// local
#include "CaloCalib.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloCalib
//
// 2007-02-08 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloCalib )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloCalib::CaloCalib( const std::string& name,
                    ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
  ,m_pCount(0)
  ,m_lCount(0)
  ,m_cCount()
  ,m_sCount()
  ,m_skip(0)
  ,m_cHistos()
  ,m_sHisto()
  ,m_hTimeInit(NULL)
  ,m_hTimeEnd(NULL)
  ,m_timeInit(-1)
  ,m_timeEnd(-1)
{
  
  declareProperty( "Detector"             ,  m_detectorName); 
  declareProperty( "PinMonitoring"        ,  m_pinDriven     = false   );
  declareProperty( "UseEmptyEvent"        ,  m_useEmpty      = true    );
  declareProperty( "ReadoutTool"          ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "Threshold"            ,  m_threshold    = 50  );
  declareProperty( "CheckNCell"           ,  m_cellCheck    = 7   );
  declareProperty( "Banks"                ,  m_banks              ); // Active when not pinDriven only
  declareProperty( "PedestalRate"         ,  m_pedRate      = 100 ); // percent
  declareProperty( "TAERate"              ,  m_taeRate      = 0   ); // percent
  declareProperty( "ReferenceTimeSlot"    ,  m_ref="T0");
  declareProperty( "TAESlots"             ,  m_slots);
  declareProperty( "StatusOnTES"          ,  m_statusOnTES = true);
  declareProperty( "SetLocationInPath"    ,  m_loc  = true);
  declareProperty( "ProfileError"         ,  m_prof = "s");
  declareProperty( "DefaultLED"           ,  m_led= 0    ); // look only to LED m_led when several (<0 -> check all LEDs) 
  declareProperty( "KillXTalk"            ,  m_xt = true ); // Use X-talk free channel for pedestal survey
  declareProperty( "CheckRate"            ,  m_checkRate=0.5 ); // Full check rate
  declareProperty( "SkipPinFEBs"          ,  m_febs ); // Expert usage (Hcal : MUST REMOVE THE UNFIRED PIN)
  declareProperty( "StandBySel"           ,  m_sbSel = 20);

  // set default detectorName
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name );

  if ( "Ecal" == m_detectorName || "Hcal" == m_detectorName  )m_pinDriven = true; 
  if ( "Hcal" == m_detectorName ){
    m_febs.push_back( 51 );  // MANDATORY TO REMOVE UNFIRED PIN (2ND LED/PMT) : no protection 
    m_febs.push_back( 53 );
  }
  
  setHistoDir( name );
 }
//=============================================================================
// Destructor
//=============================================================================
CaloCalib::~CaloCalib() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloCalib::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize " << name() << " Det = "<< m_detectorName << endmsg;

  // Get Detector Element
  //=====================
  m_calo = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( m_detectorName ) );

  // get DAQ tools
  //===============
  // for the reference slot
  m_daq = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" , this );
  if(m_ref != "T0")m_daq->_setProperty("RootInTES", m_ref +"/").ignore();
  info() << " -> DAQ tool for reference slot " << m_ref << " instantiated as "<<m_detectorName + "ReadoutTool" <<endmsg;
  info()<< "   ---> RawEvent will be loaded from  root in TES: '"<< m_ref << "'" << endmsg;

  // for additional slot
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if( *islot == m_ref ){
      warning() << "The requested additional slot " << *islot 
                << " being already active as reference slot is removed from the list ... " <<endmsg;
      continue;    
    }    
    m_daqs[*islot] = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" + *islot , this );
    if(*islot != "T0")m_daqs[*islot]->_setProperty("RootInTES", *islot +"/").ignore();
    info() << " -> DAQ tool for additional slot " << *islot <<  " instantiated as " 
           << m_detectorName + "ReadoutTool" + *islot << endmsg;
    info()<< "   ---> RawEvent will be loaded from  root in TES: '"<< *islot << "'" << endmsg;
    // check roots in TES are uniquely defined
    for(std::vector<std::string>::iterator jslot = m_slots.begin(); jslot != islot ;  ++jslot){
      if( *jslot == *islot ){
        error() << "the  RootInTES " << *islot << " is  defined twice" << endmsg;
        return StatusCode::FAILURE;
      }
    }
  }


  // get the CaloHisto tools
  //========================
  m_min = m_calo->numberOfTell1s();
  debug() << " Load CaloHisto tool for reference time slot" << endmsg;
  m_max = 0 ;
  m_booked[m_ref]=false;
  m_Sig[m_ref]   = tool<CaloHisto>( "CaloHisto" , "Signal"       , this );
  m_Ped[m_ref]   = tool<CaloHisto>( "CaloHisto" , "Pedestal"     , this );
  m_Rat[m_ref]   = tool<CaloHisto>( "CaloHisto" , "Ratio"        , this );
  m_All[m_ref ]  = tool<CaloHisto>( "CaloHisto" , "All"          , this );
  m_SumSig[m_ref]= tool<CaloHisto>( "CaloHisto" , "SumSignal"    , this );
  m_SumPed[m_ref]= tool<CaloHisto>( "CaloHisto" , "SumPedestal"  , this );
  m_SumRat[m_ref]= tool<CaloHisto>( "CaloHisto" , "SumRatio"     , this );
  m_SumAll[m_ref]= tool<CaloHisto>( "CaloHisto" , "SumAll"       , this );

  // highest rate for detailed histos
  m_detRate      = m_Sig[m_ref]->rate();
  if(m_detRate < m_Ped[m_ref]->rate())m_detRate      = m_Ped[m_ref]->rate();
  if(m_detRate < m_Rat[m_ref]->rate())m_detRate      = m_Rat[m_ref]->rate();
  if(m_detRate < m_All[m_ref]->rate())m_detRate      = m_All[m_ref]->rate();


  if( !m_pinDriven && (m_SumRat[m_ref]->histo() || m_Rat[m_ref]->histo()) ){
    warning() << "PMT/PIN ratio is only implemented for PIN-driven monitoring" << endmsg;
    m_SumRat[m_ref]->setHisto(false);
    m_Rat[m_ref]->setHisto(false);
  }
  

  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if( *islot == m_ref )continue;    
    debug() << " Load CaloHisto tool for additional time slot" << *islot << endmsg;
    m_ok[*islot]=false;
    m_histo[*islot]=true;
    m_booked[*islot]=false;
    m_Sig[*islot]    = tool<CaloHisto>("CaloHisto", "Signal"   + *islot , this);
    m_Ped[*islot]    = tool<CaloHisto>("CaloHisto", "Pedestal" + *islot , this);
    m_All[*islot]    = tool<CaloHisto>("CaloHisto", "All"      + *islot , this);
    m_Rat[*islot]    = tool<CaloHisto>("CaloHisto", "Ratio"    + *islot , this);
    m_SumSig[*islot] = tool<CaloHisto>("CaloHisto", "SumSignal"+ *islot , this);
    m_SumPed[*islot] = tool<CaloHisto>("CaloHisto", "SumPedestal"+ *islot , this);
    m_SumRat[*islot] = tool<CaloHisto>("CaloHisto", "SumRatio"+ *islot , this);
    m_SumAll[*islot] = tool<CaloHisto>("CaloHisto", "SumAll"+ *islot , this);
    if( !m_Sig[*islot]->histo() 
        && !m_All[*islot]->histo() 
        && !m_SumSig[*islot]->histo()
        && !m_Ped[*islot]->histo() 
        && !m_SumPed[*islot]->histo() 
        && !m_SumAll[*islot]->histo()){
      warning() << " The adjacent time slot " << *islot 
                << " is defined in the options but no distribution histos and no summary histos is activated" 
                << endmsg;
      m_histo[*islot]=false;
    }
    if(m_taeRate <=0){
      m_histo[*islot]=false;
    }
    

    if( m_Rat[*islot]->histo() || m_SumRat[*islot]->histo()){
      warning() << " PMT/PIN ratio is only implemented for the reference time slot - removed from the configuration" 
                << endmsg;
      m_Rat[*islot]->setHisto(false);
      m_SumRat[*islot]->setHisto(false);
    }
  }
  

  // Checks property consistency
  //=============================
  debug() << " Check Consistency " << endmsg;
  if(!m_Ped[m_ref]->histo() && !m_SumPed[m_ref]->histo() && 0 != m_pedRate){
    warning() << " No pedestal histograming is requested - Force  pedestal rate to be 0 " << endmsg;
    m_pedRate = 0;
  }


  // Init tables
  // ===========
  m_skip = 0;
  debug() << " Init table " << endmsg;
  const std::vector<CardParam>& cards = m_calo->cardParams();
  for(std::vector<CardParam>::const_iterator  iCard = cards.begin() ; iCard != cards.end() ; ++iCard){
    if(!(*iCard).isPinCard())continue;
    bool keep = true;
    for( std::vector<int>::iterator ifeb = m_febs.begin() ; m_febs.end() != ifeb ; ++ifeb){
      if( *ifeb == (*iCard).number() ){
        keep = false;
        break;
      }
    }  
    if( !keep ){
      m_skip++;
      continue;
    }
    info() << "Channels of FE-board number "<<(*iCard).number() << " is added to PIN readout channels" << endmsg;
    CardParam card = *iCard;
    const std::vector<LHCb::CaloCellID>& chans = card.ids();
    for(std::vector<LHCb::CaloCellID>::const_iterator  iChan = chans.begin() ; iChan != chans.end() ; ++iChan){
      if(m_calo->valid(*iChan) && isInBankList( *iChan) ){
        m_pinChannels.push_back(*iChan);
        debug() << "add pin channels " << *iChan << endmsg;
      } 
    }
  }
  
    
  debug() << "added " << m_pinChannels.size() << " pin channels" << endmsg;
  

  m_nCells  = m_calo->numberOfCells()+ m_calo->numberOfPins();
  m_pCount = 0;
  m_lCount = 0;
  m_sCount.clear();
  m_cCount.clear();
  //
  m_minFire = m_calo->numberOfPins();
  m_maxFire = 0;
  
  m_pShift    =  m_calo->pedestalShift();  
  m_ppShift   =  m_calo->pinPedestalShift();

  // Get random Generator
  //=====================
  m_rndSvc = svc< IRndmGenSvc>( "RndmGenSvc" , true );
  m_rnd = new Rndm::Numbers( m_rndSvc , Rndm::Flat( 0.0 , 100.0) );


  // ===  Get OdinTimeDecoder
  m_odin = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder",this);

  // Book Histo for reference time slot at the initialisation step
  // Alternative time slot are booked on-Demand during execution
  // ===========================================================
  return bookHistos(m_ref); 
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloCalib::execute() {

  // === TIMING HISTO TO SYNCHRONIZE WITH CaloOccupancy ===
  // get odin time
  m_odin->getTime();
  LHCb::ODIN* odin = getIfExists<LHCb::ODIN> (LHCb::ODINLocation::Default);
  longlong eventTime = (odin != NULL) ? (longlong) odin->eventTime().ns() * 1/1e9 : 0;

  if(m_timeInit < 0  && m_timeEnd < 0 ){
    // Book at first event 
    std::string time = Gaudi::Utils::toString( eventTime );
    m_hTimeInit = GaudiHistoAlg::book1D (HistoID("timeInit"), time  ,    0, 2, 2);  
    m_hTimeEnd  = GaudiHistoAlg::book1D (HistoID("timeEnd") , time  ,    0, 2, 2);  
    m_timeInit = eventTime;
    m_timeEnd  = eventTime;
  }else  if(  eventTime < m_timeInit){
    m_timeInit = eventTime;
    m_hTimeInit->setTitle(Gaudi::Utils::toString( eventTime ) );
  }else if(   eventTime  > m_timeEnd ){
    m_timeEnd = eventTime;
    m_hTimeEnd->setTitle(Gaudi::Utils::toString( eventTime ) );
  }
  // =========== =========== =========== =========== =========== =========== ===========

  m_lCount   = 0;
  m_pCount   = 0;
  m_sCount.clear();
  m_cCount.clear();
  
  // Get Raw + Calo Bank for the reference time slot
  if(!m_daq->ok())return setStatus();
  // ... and for adjacent slots if needed
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if( *islot == m_ref )continue;    
    if(m_histo[*islot])m_ok[*islot]= m_daqs[*islot]->ok(); // getBank only if histo is required
    if(m_ok[*islot] && !m_booked[*islot] )bookHistos( *islot ).ignore(); // book histos on-Demand
  }

  // Init 
  unsigned int fire = 0;        // for info only
  // prepare downscalings 
  m_ped = false;
  m_tae = false;
  m_det = false;
  m_check = false;
  m_shoot  = m_rnd->shoot();
  if( m_pedRate > 0 && m_rnd->shoot()  < m_pedRate  )m_ped=true;
  if( m_taeRate > 0 && m_rnd->shoot()  < m_taeRate  )m_tae=true;
  if( m_detRate > 0 && m_shoot         < m_detRate  )m_det=true;
  if( m_checkRate > 0 && m_shoot       < m_checkRate)m_check=true;

  // Fill event counters
  fill( m_countHistos[m_ref] , 0  , 1.);
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if( *islot == m_ref )continue;    
    if( m_ok[*islot] )fill( m_countHistos[*islot] , 0  , 1.);
  }


  // ----- PROCESSING ------//
  if( !m_pinDriven)return fullDecoding();

  // Decode PIN channels
  int nStandBy = 0;
  for ( std::vector<LHCb::CaloCellID>::iterator iChan = m_pinChannels.begin() ; iChan !=  m_pinChannels.end() ; ++iChan ){
    LHCb::CaloCellID pinId =  *iChan;
    double pin = (double) m_daq->adc(pinId);    
    bool pinIsfired = ( pin >= m_threshold);

    if ( msgLevel( MSG::DEBUG) ) debug() << " pin " << pinId << " adc = " << pin << " Is fired ? " << pinIsfired << endmsg; 

    // connected leds
    const std::vector<int>& leds = m_calo->pinLeds( pinId );
    // test
    int firedLed = -1;
    // check whether the pin is dead at low rate (full decoding)
    if( pinIsfired || m_check )firedLed = whichLedIsFired( leds  );    
    if( pinIsfired && firedLed < 0 ){
      pinIsfired = false ; // noisy PIN-diode or X-talk or PMT in StandBy !!
      nStandBy++;
    }


    if ( msgLevel( MSG::DEBUG) ) debug() << " fired pin ? " << pinIsfired << "  fired LED ? " << firedLed << endmsg;

    if(!pinIsfired && firedLed <0 ){
      if( m_useEmpty )continue;    // use ONLY empty event for pedestal survey
      if( !m_useEmpty && !m_ped)continue;          // Downscale pedestal (and dead PIN)
    }
    else
      fire++;
    
    fillHisto(pinId , firedLed , pin , 1. ,  pinIsfired , false); // fill histo for PIN-diode

    // loop over connected LEDs
    for(std::vector<int>::const_iterator led = leds.begin();led!=leds.end();++led){
      const std::vector<LHCb::CaloCellID>& cells = m_calo->ledChannels( *led ); // !! reference
      bool ledIsfired = (*led == firedLed );

      if( m_useEmpty &&  !ledIsfired )continue; // keep only fired LED and PINs
      if( !ledIsfired && !m_ped )continue; // downscale pedestal histogramming      


      // loop over connected PMTs
      for(CellIds::const_iterator cell=cells.begin();cell!=cells.end();++cell){
        const LHCb::CaloCellID& id = *cell;
        double pmt = (double) m_daq->adc(*cell);
        //        double pmt = (double) m_daq->adc(*cell,-256);
        //        if(pmt == -256)continue;
        if( !isInBankList( id ) ) continue;
        if( m_xt && !ledIsfired  && firedNeighbor( id , true))continue; // reject if any FEB channel is flashed (X-talk cleaning)
        if( pinIsfired && pmt < m_threshold )continue; // DEAD PMT or connection
        fillHisto(id , *led , pmt , pin , ledIsfired , pinIsfired );
        // Event dump          
        if(msgLevel(MSG::VERBOSE)){
          std::string txt = "Pedestal";
          if ( ledIsfired )txt = "Signal";
          verbose() << txt 
                    << " => PMT : [" << id << "," << pmt 
                    << "] PIN : [" << pinId << "," << pin << "]" << endmsg;
        }
      } 
    }
  } 



 // Empty event -> PEDESTAL
  bool standBy  = (nStandBy > m_sbSel );
  if(m_useEmpty && fire == 0 && m_ped && !standBy ){
    counter("empty led flash event") += 1;
    const CaloVector<LHCb::CaloAdc>& adcs = m_daq->adcs();
    for( CaloVector<LHCb::CaloAdc>::const_iterator it = adcs.begin();adcs.end() != it;++it){
      int adc = (*it).adc();
      LHCb::CaloCellID id = (*it).cellID();
      fillHisto(id , -1 , (double) adc , 1.  , false , false ); // FILL PEDESTAL ...
    }
  }
  
  
  int nChannels = m_nCells - m_skip;
  double aLed = ( nChannels == 0 ) ? 0. : (double) m_lCount  / (double) nChannels;
  double aPed = ( nChannels == 0 ) ? 0. : (double) m_pCount  / (double) nChannels;
  double sPed = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::Pedestal]/(double) nChannels;
  double sSig = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::Signal]/(double) nChannels;
  double sRat = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::Ratio]/(double) nChannels;
  double sAll = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::All]/(double) nChannels;
  double dPed = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::Pedestal]/(double) nChannels;
  double dSig = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::Signal]/(double) nChannels;
  double dRat = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::Ratio]/(double) nChannels;
  double dAll = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::All]/(double) nChannels;
  

  fill( m_countHistos[m_ref] , 1 , aLed); // average LED flash event/channel
  fill( m_countHistos[m_ref] , 2 , aPed); // average PED event/channel
  fill( m_countHistos[m_ref] , 3 , sPed); // average PED entries in summary histo/channel
  fill( m_countHistos[m_ref] , 4 , sSig); // average Signal entries in summary histo/channel
  fill( m_countHistos[m_ref] , 5 , sRat); // average Ratio entries in summary histo/channel
  fill( m_countHistos[m_ref] , 6 , sAll); // average All entries in summary histo/channel
  fill( m_countHistos[m_ref] , 7 , dPed); // average PED entries in detail histo/channel
  fill( m_countHistos[m_ref] , 8 , dSig); // average Signal entries in detail histo/channel
  fill( m_countHistos[m_ref] , 9 , dRat); // average Ratio entries in detail histo/channel
  fill( m_countHistos[m_ref] , 10 , dAll); // average All entries in detail histo/channel

  counter("LED flash/channel") += aLed;
  counter("PEDESTAL entries/channel")  += aPed;
  
  // Decoding rates (for info) 
  unsigned int nTell1s = m_daq->nTell1s();
  if(nTell1s <= m_min )m_min = nTell1s;
  if(nTell1s >= m_max )m_max = nTell1s;
  if(fire <= m_minFire )m_minFire = fire;
  if(fire >= m_maxFire )m_maxFire = fire;
  //
  return setStatus();
}

StatusCode CaloCalib::setStatus(StatusCode sc){  
  // put Readout Status on TES
  if(m_statusOnTES){
    m_daq->putStatusOnTES();
    for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){
      if( *islot == m_ref )continue;    
      if(NULL != m_daqs[*islot])m_daqs[*islot]->putStatusOnTES();
    } 
  }
  return sc;
}



//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloCalib::finalize() {
  
  delete(m_rnd);

  debug() << "==> Finalize" << endmsg;

  info() << "Number of fired PIN-diodes varies from "<< m_minFire <<" to "<< m_maxFire 
         << " (among " << m_calo->numberOfPins()<< ")"<< endmsg;
  info() << "Number of decoded TELL1s (for reference time slot) varies from "<< m_min <<" to "<< m_max 
         << " among " << m_calo->numberOfTell1s()<< endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
StatusCode CaloCalib::fullDecoding(){
  if( 0 == m_banks.size() )m_banks.push_back(-1);

  // decode adcs from raw
  const CaloVector<LHCb::CaloAdc>& adcs = m_daq->adcs( m_banks );

  unsigned int nTell1s = m_daq->nTell1s();
  // Decoding rate (for info) 
  if(nTell1s <= m_min )m_min = nTell1s;
  if(nTell1s >= m_max )m_max = nTell1s;
  m_minFire = 0;
  m_maxFire = 0;
  // put Readout Status on TES
  setStatus().ignore();


  // PROCESS

  // Empty LED flash event
  int maxADC = m_daq->adcRange().second.adc();  
  bool isEmpty = false;
  if( maxADC < m_threshold ){
    isEmpty = true;
    counter("empty led flash event") += 1;
  }
  // confirm empty event (check for noisy channels above thresholds)
  if( m_useEmpty && !isEmpty ){
    int mult = 0;
    for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
      double val  = (double) (*iadc).adc();
      if (m_threshold <= val ) mult++;
      if( mult > m_cellCheck )break;
    }
    if( mult <= m_cellCheck){
      isEmpty = true;
      counter("almost empty led flash event") += 1;
    }
  }
  if( isEmpty && m_useEmpty && !m_ped )return StatusCode::SUCCESS; 


  int ped  = 0;
  int fire = 0;
  for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
    const LHCb::CaloCellID& id = (*iadc).cellID();
    double val  = (double) (*iadc).adc();
    bool fired  (m_threshold <= val ) ;
    if(fired)fire++;
    // Event Dump
    if(msgLevel(MSG::VERBOSE)) verbose() << " => PMT : " << id 
                                         << " ADC = " << val << " Fired ? " 
                                         <<fired << " Accept ? "<< m_ped << endmsg;      
    if(!fired &&  !isEmpty && m_useEmpty)continue; //  use ONLY empty LED flash for pedestal survey    
    if(!fired && !m_useEmpty && !m_ped    )continue;      
    if(!fired)ped++;
    fillHisto( id , -1 ,val , 1. , fired , false);
  }
  
  int nChannels = adcs.size();
  double aLed = ( nChannels == 0 ) ? 0. : (double) fire / (double) nChannels;
  double aPed = ( nChannels == 0 ) ? 0. : (double) ped  / (double) nChannels;
  double sPed = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::Pedestal]/(double) nChannels;
  double sSig = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::Signal]/(double) nChannels;
  double sRat = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::Ratio]/(double) nChannels;
  double sAll = ( nChannels == 0 ) ? 0. : (double) m_sCount[CaloSummaryHisto::All]/(double) nChannels;
  double dPed = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::Pedestal]/(double) nChannels;
  double dSig = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::Signal]/(double) nChannels;
  double dRat = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::Ratio]/(double) nChannels;
  double dAll = ( nChannels == 0 ) ? 0. : (double) m_cCount[CaloChannelHisto::All]/(double) nChannels;
  

  fill( m_countHistos[m_ref] , 1 , aLed); // average LED flash event/channel
  fill( m_countHistos[m_ref] , 2 , aPed); // average PED event/channel
  fill( m_countHistos[m_ref] , 3 , sPed); // average PED entries in summary histo/channel
  fill( m_countHistos[m_ref] , 4 , sSig); // average Signal entries in summary histo/channel
  fill( m_countHistos[m_ref] , 5 , sRat); // average Ratio entries in summary histo/channel
  fill( m_countHistos[m_ref] , 6 , sAll); // average All entries in summary histo/channel
  fill( m_countHistos[m_ref] , 7 , dPed); // average PED entries in detail histo/channel
  fill( m_countHistos[m_ref] , 8 , dSig); // average Signal entries in detail histo/channel
  fill( m_countHistos[m_ref] , 9 , dRat); // average Ratio entries in detail histo/channel
  fill( m_countHistos[m_ref] , 10 , dAll); // average All entries in detail histo/channel

  counter("LED flash/channel") += aLed;
  counter("PEDESTAL entries/channel")  += aPed;
  
  return StatusCode::SUCCESS;
}
//=============================================================================
void CaloCalib::fillHisto(const LHCb::CaloCellID& id  , int led,
                          double val  , double ref , 
                          bool fired , bool addRatio){
  

  int index = 0;
  if( led >= 0)index = ledIndex(id,led);
  if( index < 0)return;

  if( m_led >=0 && m_led != index )return;


  

  // -------------------
  // Fill Summary histos
  // -------------------
  if(fired){
    m_lCount += 1;
    fillSummaryHisto(id, val, index , CaloSummaryHisto::Signal , m_SumSig, m_ref);
    if(addRatio)fillSummaryHisto( id    , val-m_pShift , index , CaloSummaryHisto::Ratio , m_SumRat, m_ref, ref-m_ppShift);
  }else{    
    m_pCount += 1;
    fillSummaryHisto( id  , val, 0    , CaloSummaryHisto::Pedestal , m_SumPed, m_ref);
  }
  fillSummaryHisto( id  , val   , index    , CaloSummaryHisto::All , m_SumAll, m_ref);


  // TAE summary histos
  if(m_tae){
    for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
      if( *islot == m_ref )continue;    
      if( !m_ok[*islot] )continue;
      double value = (double) m_daqs[*islot]->adc(id);
      fillSummaryHisto( id    , value , index    , CaloSummaryHisto::All , m_SumAll, *islot);
      fired ? 
        fillSummaryHisto(id    , value , index , CaloSummaryHisto::Signal , m_SumSig, *islot) :
        fillSummaryHisto( id  , value, 0    , CaloSummaryHisto::Pedestal , m_SumPed, *islot)  ;
    }
  }
  
  // --------------------
  // Fill detailed histos
  // --------------------
  
  if( !m_det )return;
  // get CaloChannelHisto
  CaloChannelHisto* cHisto = m_cHistos[id];

  if(fired){
    fillChannelHisto( val, index , cHisto, CaloChannelHisto::Signal , m_Sig, m_ref);
    if(addRatio)fillChannelHisto( val-m_pShift , index , cHisto, CaloChannelHisto::Ratio, m_Rat, m_ref, ref-m_ppShift);
  }
  else{
    fillChannelHisto( val , 0 , cHisto, CaloChannelHisto::Pedestal , m_Ped   , m_ref);
  }
  fillChannelHisto( val , index , cHisto   , CaloChannelHisto::All , m_All   , m_ref);
  

  if( !m_tae)return;
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if( *islot == m_ref )continue;    
    if( !m_ok[*islot] )continue;
    double value = (double) m_daqs[*islot]->adc(id);
    fired ?
      fillChannelHisto( value, index , cHisto, CaloChannelHisto::Signal , m_Sig   , *islot) :
      fillChannelHisto( value , 0 , cHisto, CaloChannelHisto::Pedestal , m_Ped   , *islot);
    fillChannelHisto( value , index , cHisto   , CaloChannelHisto::All , m_All   , *islot);
    
  }
  return;
}




// Filling ------------------ //
void CaloCalib::fillSummaryHisto(const LHCb::CaloCellID& id,  double val, int led ,
                                 CaloSummaryHisto::Type type, toolMap& tMap , const std::string& slot, double norm){

  if( !tMap[slot]->histo() )return;
  if( tMap[slot]->rate() < m_shoot)return;
  if( !tMap[slot]->isInWindow(val , id ))return;
  m_sCount[type] +=1;
  double index = (double) id.index();
  double scale = tMap[slot]->scale();
  double v = scale;
  double value = (norm != 0) ? val/norm : val;
  int iled = led;
  if( !tMap[slot]->splitLeds() || !m_pinDriven )iled=0;

  for(int iord = 0 ; iord <= tMap[slot]->order() ; ++iord){
    AIDA::IHistogram1D* h1D = m_sHisto->histo( type , slot, iord , iled );
    fill( h1D , index , v);
    v *= value;
  }
  if( tMap[slot]->profile() ){
    AIDA::IProfile1D* prof1D = m_sHisto->profile( type , slot, iled );
    fill( prof1D , index , value , scale);
  }
}

void CaloCalib::fillChannelHisto( double val, int led,CaloChannelHisto* cHisto ,
                                  CaloChannelHisto::Type type, toolMap& tMap , const std::string& slot,double norm){
  if( NULL == cHisto )return;
  // LED signal 
  if( !tMap[slot]->histo() )return;
  if( tMap[slot]->rate() < m_shoot)return;
  const LHCb::CaloCellID& id = cHisto->id();
  if( !tMap[slot]->isInWindow(val , id ))return;
  m_cCount[type] +=1;
  int iled = led;
  if( !tMap[slot]->splitLeds() || !m_pinDriven  )iled=0;
  AIDA::IHistogram1D* h1D = cHisto->histo( type , slot, iled );
  double value = (norm != 0) ? val/norm : val;
  fill( h1D , value  , tMap[slot]->scale());      
}


// Booking
void CaloCalib::bookSummaryHisto( CaloSummaryHisto::Type type, toolMap& tMap , const std::string& slot){
  int maxLedAll = 0;
  int maxLed = 0;
  int nCells  = m_nCells;
  for(int iCell = 0 ; iCell < nCells ; ++iCell){
    LHCb::CaloCellID id = m_calo->cellIdByIndex(iCell);
    std::vector<int> leds = m_calo->cellParam(id).leds();
    if( (unsigned)maxLedAll < leds.size() )maxLedAll = leds.size();
    if( (unsigned)maxLed < leds.size() && !id.isPin() )maxLed = leds.size();
  }

  // tit'ling
  std::string hName = tMap[slot]->hName();
  std::string pName = tMap[slot]->pName();
  std::string sTitle = m_detectorName + " " +  hName + " data 'moment' for time slot" + slot  ;
  std::string pTitle = m_detectorName + " " +  pName + " data 'profile' for time slot" + slot  ;
  bool split = tMap[slot]->splitLeds() && m_pinDriven;

  // Signal moments
  if( tMap[slot]->histo() ){      
    int nLeds = maxLedAll;
    if( !split ) nLeds=1;
    for(int iLed = 0 ; iLed < nLeds ; ++iLed){
      if( m_led >=0 && m_led != iLed )continue;
      if(  tMap[slot]->profile() ){
        const std::string pUnit = getSummaryUnit(pName , slot, iLed , 0 , split);       
        AIDA::IProfile1D* prof = bookProfile1D(pUnit,pTitle ,tMap[slot]->min(),tMap[slot]->max(), tMap[slot]->bins(),m_prof);
        m_sHisto->addProfile( prof , type,  slot  ); 
        counter("Bookeed " +  pName + " 'profile' histo(s) for " + slot) += 1;
      }
      // 'moment' summary histo
      for(int iord = 0 ; iord <= tMap[slot]->order() ; ++iord){
        const std::string sUnit = getSummaryUnit(hName , slot, iLed , iord, split);       
        std::string  ord = " (order " + Gaudi::Utils::toString( iord ) + ")";
        AIDA::IHistogram1D* h1D  = book1D( sUnit, sTitle + ord , tMap[slot]->min(), tMap[slot]->max(),tMap[slot]->bins());        
        counter("Booked " +  hName + " 'moment' histo(s) for " + slot) += 1;
        m_sHisto->addHisto( h1D , type, iord, slot  ); 
      }
    }
  }
}


void CaloCalib::bookChannelHisto( CaloChannelHisto* cHisto ,CaloChannelHisto::Type type,
                                  toolMap& tMap , const std::string& slot){

  const LHCb::CaloCellID& id = cHisto->id();
  // Book channelHisto
  if( !tMap[slot]->histo() )return;


  // tit'ling
  std::string name = tMap[slot]->hName();
  std::ostringstream sid;
  int feb = m_calo->cardNumber( id );
  sid << "[crate=" << format("%02i", m_calo->cardCrate( feb ) ) <<  ","
      << "feb="   << format( "%02i" , m_calo->cardSlot( feb  ) )<< ","
      << "readout=" <<Gaudi::Utils::toString( m_calo->cardColumn( id ) + nColCaloCard * m_calo->cardRow( id ) )
      << "] => " << id << " (index="<< Gaudi::Utils::toString( id.index() ) << ")" ;
  
  std::string title = m_detectorName + " " +  name + " data  for channel  " + sid.str() + " in " + slot ;
  bool split = tMap[slot]->splitLeds() && m_pinDriven;

  // LED spliting
  const std::vector<int> leds = m_calo->cellParam(id).leds();
  int nLeds =  leds.size();
  if(m_detectorName == "Spd" || m_detectorName == "Prs" || !m_pinDriven){
    nLeds =1 ;
  }
  int N = ( split ) ? nLeds : 1;

  // actual booking
  for(int iLed = 0 ; iLed < N; ++iLed){
    if( m_led >=0 && m_led != iLed )continue;
    std::string unit = getChannelUnit(name , slot, iLed , id, split);       
    AIDA::IHistogram1D* h1D = book1D( unit , title , tMap[slot]->min(id) , tMap[slot]->max(id) , tMap[slot]->bins(id) );  
    Gaudi::Utils::Aida2ROOT::aida2root( h1D )->SetCanExtend(TH1::kXaxis);
    cHisto->addHisto( h1D , type, slot  ); 
    counter("Booked " +  name + " histo(s) for " + slot) += 1;
  }
}



//=============================================================================
StatusCode  CaloCalib::bookHistos(std::string slot){

  debug()<<  "Booking histograms for time slot " << slot << endmsg;
  
  // remove MSG
  int level = msgLevel();  
  setProperty("OutputLevel", MSG::FATAL ).ignore();
  //setOutputLevel(MSG::FATAL);


  //---------------------
  // Counter 1D histo  !!
  //---------------------
  std::string slo="";
  if(slot != m_ref)slo += "/" +  slot ;
  AIDA::IHistogram1D* count = book1D( "Counters" + slo +"/1" , m_detectorName 
                                        + ": Event rate per channel for time slot " + slot , 0 , 11 , 11 );
  m_countHistos[slot] = count; 
  TH1D* th = Gaudi::Utils::Aida2ROOT::aida2root( count );
  th->GetXaxis()->SetBinLabel(1, std::string("Events").c_str() );
  th->GetXaxis()->SetBinLabel(2, std::string("<LED event>").c_str() );
  th->GetXaxis()->SetBinLabel(3, std::string("<PED event>").c_str() );
  th->GetXaxis()->SetBinLabel(4, std::string("<sum PED>").c_str() );
  th->GetXaxis()->SetBinLabel(5, std::string("<sum SIG>").c_str() );
  th->GetXaxis()->SetBinLabel(6, std::string("<sum RAT>").c_str() );
  th->GetXaxis()->SetBinLabel(7, std::string("<sum ALL>").c_str() );
  th->GetXaxis()->SetBinLabel(8, std::string("<det PED>").c_str() );
  th->GetXaxis()->SetBinLabel(9, std::string("<det SIG>").c_str() );
  th->GetXaxis()->SetBinLabel(10, std::string("<det RAT>").c_str() );
  th->GetXaxis()->SetBinLabel(11, std::string("<det ALL>").c_str() );

  //--------------
  // DISTRIBUTIONS per channel
  //--------------  
  m_cHistos.clear();
  // Book channel histos 
  int nCells  = m_nCells;
  for(int iCell = 0 ; iCell < nCells ; ++iCell){
    LHCb::CaloCellID id = m_calo->cellIdByIndex(iCell);    
    if( !m_calo->isReadout( id ) )continue;
    if (!isInBankList( id ))continue;    
    id.setCalo( CaloCellCode::CaloNumFromName( m_detectorName ));
    CaloChannelHisto* cHisto = new CaloChannelHisto(id, true);
    m_cHistos.addEntry( cHisto, id);
    bookChannelHisto(cHisto , CaloChannelHisto::Signal   , m_Sig , slot );
    bookChannelHisto(cHisto , CaloChannelHisto::All      , m_All , slot );
    bookChannelHisto(cHisto , CaloChannelHisto::Ratio    , m_Rat , slot );
    bookChannelHisto(cHisto , CaloChannelHisto::Pedestal , m_Ped , slot );    
  }// end of loop over chanels

  //--------------------
  // SUMMARY HISTOGRAMS
  //--------------------
  m_sHisto = new CaloSummaryHisto( true );
  bookSummaryHisto( CaloSummaryHisto::Signal   , m_SumSig, slot);
  bookSummaryHisto( CaloSummaryHisto::All      , m_SumAll, slot);
  bookSummaryHisto( CaloSummaryHisto::Pedestal , m_SumPed, slot);
  bookSummaryHisto( CaloSummaryHisto::Ratio    , m_SumRat, slot);  

// reset outputlevel
  //setOutputLevel( level ); 
  setProperty("OutputLevel", level ).ignore();
  m_booked[slot]=true;
  return StatusCode::SUCCESS;
}


const std::string CaloCalib::getSummaryUnit(const std::string& type ,const std::string& slot, 
                                      int led, int ord, bool split){  
  std::string unit = type;    
  // Split led 
  std::string sled ="";
  if( split && led >= 0 )sled = "/LED" + Gaudi::Utils::toString( led );
  unit += sled;
  // TAE slot
  if(m_ref != slot)unit += "/" + slot ;  
  // moment Order
  unit += "/" +  Gaudi::Utils::toString( ord + 1 );

  return unit;
}

const std::string CaloCalib::getChannelUnit(const std::string& type , 
                                            const std::string& slot, 
                                            int led,
                                            const LHCb::CaloCellID& id, 
                                            bool split){
  std::string unit = type;

  // localisation  
  std::ostringstream loc;
  if(m_loc){ 
    int feb = m_calo->cardNumber( id );
    loc << "/crate" << format("%02i", m_calo->cardCrate( feb ) ) << "/" 
        << "feb"  << format( "%02i" , m_calo->cardSlot( feb  ) )  ;
  }
  unit += loc.str();
  
  // Split led
  std::string sled ="";
  if( split && led >= 0 )sled = "/LED" + Gaudi::Utils::toString( led );
  unit += sled;

  // TAE slot
  if(m_ref != slot)unit += "/" + slot ;

  // channel
  std::string index = "/" + Gaudi::Utils::toString( id ) ;
  std::string local = "/" +  Gaudi::Utils::toString( m_calo->cardColumn( id) + nColCaloCard * m_calo->cardRow( id ) );
  unit += (m_loc) ? local : index;
  //
  return unit;
} 
        
bool CaloCalib::firedNeighbor(const LHCb::CaloCellID& id, bool skipFEB){
  int feb = m_calo->cardNumber(id);

  if(skipFEB){
    const std::vector<LHCb::CaloCellID>& cells = m_calo->cardChannels( feb ) ;
    for( std::vector<LHCb::CaloCellID>::const_iterator it = cells.begin() ; cells.end()!=it ; ++it){
      if( m_daq->adc(*it) > m_threshold ) return true;
    }
  } else {
   const std::vector<LHCb::CaloCellID>&   cells = m_calo->neighborCells( id );
   for( std::vector<LHCb::CaloCellID>::const_iterator it = cells.begin() ; cells.end()!=it ; ++it){
     if( feb != m_calo->cardNumber( *it ) )continue;
     if( m_daq->adc(*it) > m_threshold ) return true;
   }
  }
  return false;
}
