
// $Id: $
// Include files 

//From LHCb
#include "Event/ODIN.h" 

// local
#include "InspectTAE.h"

//-----------------------------------------------------------------------------
// Implementation file for class : InspectTAE
//
// 2011-03-17 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( InspectTAE )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
InspectTAE::InspectTAE( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  declareProperty( "TAESlots"  ,  m_slots);
  m_slots.push_back("Prev2");
  m_slots.push_back("Prev1");
  m_slots.push_back("T0");
  m_slots.push_back("Next1");
  m_slots.push_back("Next2");
}
//=============================================================================
// Destructor
//=============================================================================
InspectTAE::~InspectTAE() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode InspectTAE::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;


 for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
   this->setProperty("RootInTES","").ignore();//reset
   if(*islot != "T0")this->setProperty("RootInTES",*islot).ignore();   
   m_odins[*islot] = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder" + *islot ,this);
 }
  this->setProperty("RootInTES","").ignore();//reset
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode InspectTAE::execute() {

  debug() << "==> Execute" << endmsg;


  int run;
  int evt;
  std::vector<int> order;
  std::vector<int> evts;
  for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){
    
    m_odins[*islot]->getTime();    
    std::string pref;
    if(*islot != "T0")pref = *islot + "/";
    if( exist<LHCb::ODIN>(pref +LHCb::ODINLocation::Default) ){
      LHCb::ODIN* odin=get<LHCb::ODIN> (pref + LHCb::ODINLocation::Default);
      run = odin->runNumber();
      evt = odin->eventNumber();
      debug() << " EVENT " << run << " : " << evt << endmsg;
      order.push_back(odin->bunchId());
      evts.push_back(evt);
    }
  }
  
  int valm=0;
  unsigned int sum=0;
  for(std::vector<int>::iterator i = order.begin() ; order.end() != i ; ++ i){
    if(i==order.begin())valm=*i;
    else{
      sum += *i-valm;
      valm = *i;
    }
  }
  debug() << "Order : " << order << endmsg;
  if( sum +1 != order.size() ){
    info() << " EVENT " << run << " : " << evt << endmsg;
    warning() << "WRONG TAE ORDERING : BCIDs=" << order << " EVTs=" << evts <<endmsg;
    counter("WRONG TAE ORDERING")+=1;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode InspectTAE::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
