// $Id: L0CaloMonit.h,v 1.6 2009-06-03 18:04:18 robbep Exp $
#ifndef L0CALORATEMONITOR_H
#define L0CALORATEMONITOR_H

// Include files
// from Gaudi
#include "CaloUtils/Calo2Dview.h"
// from Event
#include "Event/L0DUBase.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
// Forward declarations
class DeCalorimeter ;
namespace LHCb {
  class L0CaloCandidate ;
}

/** @class L0CaloRateMonitor L0CaloRateMonitor.h
 *  Monitoring algorithm for the L0 Calorimeter trigger rates ; fractions of selected candidates L0Had L0Ele and L0Pho above given thresholds
 *  @author Olivier Callot
 *  @author Marie-Helene Schune
 *  @date   26/07/2012
 */

class L0CaloRateMonitor: public Calo2Dview {

 public:
  /// Standard constructor
  L0CaloRateMonitor( const std::string& name , ISvcLocator* pSvcLocator ) ;

  /// Standard destructor
  virtual ~L0CaloRateMonitor( ) ;

  /// Initialization: book histograms
  StatusCode initialize() override;

  /// Main execution routine: fill histograms and find hot cells
  StatusCode execute   () override;

  /// Finalization: print hot cells
  StatusCode finalize  () override;

 protected:

 private:


  DeCalorimeter * m_ecal ; ///< Pointer to Ecal detector element
  DeCalorimeter * m_hcal ; ///< Pointer to Hcal detector element

  float m_nEvents ; ///< Counter of events with existing L0Calo data

  /** Suffix to add to the default name of L0Calo data containers. Allows
   *  to select between different sources (data or emulation)
   *  Set by option.
   */
  std::string m_inputDataSuffix ;


 /** Frequency with which the histograms are filled
*/
  int  m_updateFrequency ;

  int m_alarmThresholdRatio ;

  // trigger thresholds
  int m_L0HadThreshold ;
  int m_L0EleThreshold ;
  int m_L0PhoThreshold ;

// counter for the selected candidates
  float m_selHad ;
  float m_selEle ;
  float m_selPho ;

  float m_selHadInner ;
  float m_selEleInner ;
  float m_selPhoInner ;

  float m_selEleMiddle ;
  float m_selPhoMiddle ;

  float m_selHadOuter ;
  float m_selEleOuter  ;
  float m_selPhoOuter  ;

  // Information stored in histogram
  IHistogram1D * m_L0HadSelRateHist ;
  IHistogram1D * m_L0EleSelRateHist ;
  IHistogram1D * m_L0PhoSelRateHist ;

  /// BCId histogram
  IHistogram1D * m_bcidHist ;

  /** Auxiliary function to fill counters for the default monitoring
   *  @param[in] cand  L0CaloCandidate to use to fill histograms
   */
  void defaultMonitoring( const LHCb::L0CaloCandidate * cand ) ;

  /** Returns the detector element for a given type: Ecal for Electron,
   *  Photon, Local pi0 and Global pi0, and Hcal for hadron
   */
  inline DeCalorimeter * detector( const int type ) const {
    if ( L0DUBase::CaloType::Hadron == type ) return m_hcal ;
    return m_ecal ;
  }


  /// Name corresponding to the area
  inline std::string area( const int type ) const {
    switch ( type ) {
    case 0: return "Outer"  ; break ;
    case 1: return "Middle" ; break ;
    case 2: return "Inner"  ; break ;
    default: break ;
    }
    return "default" ;
  }
};
#endif // L0CALORATEMONITOR_H
