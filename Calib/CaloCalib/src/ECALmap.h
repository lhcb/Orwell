#pragma once

#include "TH1D.h"
#include "TH2D.h"
#include "TH2F.h"
#include "TDirectory.h"
#include "TFile.h"
#include <math.h>

#define NECELLS 6016
#define NEZ  4
#define NEX 64
#define NEY 64

bool isECALcell(int z, int x, int y);
bool zxy_E(int cellid, int& z, int& x, int& y);
int cellid_E(int z, int x, int y);

int iECAL2iPIN(int iECAL);
int iECAL2iLED(int iECAL);

char* PINname(int iPIN);

int iECAL2cellID(int iECAL);
int cellID2iECAL(int cellID);

int zxy2iECAL(int iz, int ix, int iy);

void saveECALallH(char* fnam, ...);
void saveECALallHd(const char* dnam, const char* fnam, ...);

TH2D* creECALmap(const char* name, const char* title);
TH2F* creECALmapF(const char* name, const char* title);

bool isECALmapH(TH2* emap);
bool isECALmapO(TObject* ob);

int FEBwd2in_E(int wd);
int FEBin2wd_E(int in);

bool ECALcell2xy(int z, int x, int y, double& xc, double& yc);
bool ECALxy2cell(int& z, int& x, int& y, double xc, double yc);

template<class HIST> void zlim_ECALmapH(HIST* emap){
  double zmax=-1e23, zmin=1e23;
  for(int iz=1; iz<NEZ; ++iz){
    for(int iy=0; iy<NEY; ++iy){
      for(int ix=0; ix<NEX; ++ix){
	if(isECALcell(iz,ix,iy)){
	  double v=getval_ECALmapH(emap, iz, ix, iy);
	  if(v!=0){
	    if(zmax<v)zmax=v;
	    if(zmin>v)zmin=v;
	  }
	}
      }
    }
  }
  emap->GetZaxis()->SetRangeUser(zmin, zmax);
}

template<class HIST> double getval_ECALmapH(HIST* emap, int iz, int ix, int iy){
  if(!isECALcell(iz,ix,iy)) return 0;
  int ibinx=0, ibiny=0;
  if(1==iz){
    ibinx=  1+6*ix; 
    ibiny=-35+6*iy;
  }else if(2==iz){
    ibinx= 97+3*ix;
    ibiny= 61+3*iy;
  }else if(3==iz){
    ibinx=129+2*ix;
    ibiny= 93+2*iy;
  }
  double bval=emap->GetBinContent(ibinx,ibiny);
  return bval;
}

template<class HIST> double geterr_ECALmapH(HIST* emap, int iz, int ix, int iy){
  if(!isECALcell(iz,ix,iy)) return 0;
  int ibinx=0, ibiny=0;
  if(1==iz){
    ibinx=  1+6*ix; 
    ibiny=-35+6*iy;
  }else if(2==iz){
    ibinx= 97+3*ix;
    ibiny= 61+3*iy;
  }else if(3==iz){
    ibinx=129+2*ix;
    ibiny= 93+2*iy;
  }
  double bval=emap->GetBinError(ibinx,ibiny);
  return bval;
}

template<class HIST> double getsum_ECALmapH(HIST* emap){
  double sumh=0;
  for(int iz=1; iz<NEZ; ++iz){
    for(int iy=0; iy<NEY; ++iy){
      for(int ix=0; ix<NEX; ++ix){
	if(isECALcell(iz,ix,iy)){
	  double bval = getval_ECALmapH(emap,iz,ix,iy);
	  sumh+=bval;
	}
      }
    }
  }
  return sumh;
}

template<class HIST> double getsumcentre_ECALmapH(HIST* emap){
  double sumh=0;
  int iz=3;
  for(int iy=0; iy<NEY; ++iy){
    for(int ix=0; ix<NEX; ++ix){
      if(isECALcell(iz,ix,iy)){
	double bval = getval_ECALmapH(emap,iz,ix,iy);
	sumh+=bval;
      }
    }
  }
  return sumh;
}

template<class HIST> void fillECALmapH(HIST* emap, int iz, int ix, int iy, double val/*, double err=0 */){
  if(!emap)return;
  if(!isECALcell(iz,ix,iy))return;
  if(1==iz){
    for(int ibinx=1+6*ix; ibinx<=6+6*ix; ++ibinx){
      for(int ibiny=-35+6*iy; ibiny<=-30+6*iy; ++ibiny){
	emap->SetBinContent(ibinx,ibiny,val);
      }
    }
  }else if(2==iz){
    for(int ibinx=97+3*ix; ibinx<=99+3*ix; ++ibinx){
      for(int ibiny=61+3*iy; ibiny<=63+3*iy; ++ibiny){
	emap->SetBinContent(ibinx,ibiny,val);
      }
    }
  }else if(3==iz){
    for(int ibinx=129+2*ix; ibinx<=130+2*ix; ++ibinx){
      for(int ibiny=93+2*iy; ibiny<=94+2*iy; ++ibiny){
	emap->SetBinContent(ibinx,ibiny,val);
      }
    }
  }
  emap->SetEntries(emap->GetEntries()+1);
}

template<class HIST2D> void fillECALhitmapH(HIST2D* emap, int iz, int ix, int iy, double wt){
  if(!isECALmapH(emap))return;
  if(!isECALcell(iz,ix,iy))return;
  
  if(1==iz){
    for(int ibinx=1+6*ix; ibinx<=6+6*ix; ++ibinx){
      for(int ibiny=-35+6*iy; ibiny<=-30+6*iy; ++ibiny){
	emap->Fill(emap->GetXaxis()->GetBinCenter(ibinx),emap->GetYaxis()->GetBinCenter(ibiny),wt);
      }
    }
  }else if(2==iz){
    for(int ibinx=97+3*ix; ibinx<=99+3*ix; ++ibinx){
      for(int ibiny=61+3*iy; ibiny<=63+3*iy; ++ibiny){
	emap->Fill(emap->GetXaxis()->GetBinCenter(ibinx),emap->GetYaxis()->GetBinCenter(ibiny),wt);
      }
    }
  }else if(3==iz){
    for(int ibinx=129+2*ix; ibinx<=130+2*ix; ++ibinx){
      for(int ibiny=93+2*iy; ibiny<=94+2*iy; ++ibiny){
	emap->Fill(emap->GetXaxis()->GetBinCenter(ibinx),emap->GetYaxis()->GetBinCenter(ibiny),wt);
      }
    }
  }
}

template<class HIST> TH1D* makeECALmap_1H(HIST* emap, double zmin=0, double zmax=0){
  int ix,iy;
  
  if(!emap)return 0;
  
  double mmax=-1000000, mmin=1000000;
  if(zmin>=zmax){
    //int ixmax=0, iymax=0, ixmin=0, iymin=0;
    for(ix=1; ix<=384; ++ix){
      for(iy=1; iy<=312; ++iy){
	if(ix>=177 && ix<=208 && iy>=145 && iy<=168)continue;
	double g=emap->GetBinContent(ix,iy);
	if(0!=g){
	  if(mmax<g){
	    mmax=g;
	    //ixmax=ix;
	    //iymax=iy;
	  }
	  if(mmin>g){
	    mmin=g;
	    //ixmin=ix;
	    //iymin=iy;
	  }
	}
      }
    }
  }else{
    mmax=zmax;
    mmin=zmin;
  }
  mmax+=0.001*(mmax-mmin);
  
  double lmscale=floor(log10(mmax-mmin));
  double mscale=pow((double)10,lmscale);
  if( ((mmax-mmin)/mscale)<3 )mscale/=10;
  double mb1=floor(mmin/mscale);
  double mb2=ceil(mmax/mscale);
  int mbins=(mb2-mb1)*10;
  double mx1=mb1*mscale;
  double mx2=mb2*mscale;
  
  char name_1[256]; 
  strncpy(name_1, emap->GetName(), 240);
  strcat(name_1, "_1");
  
  char title_1[256]; 
  strncpy(title_1, emap->GetTitle(), 240);
  strcat(title_1, "_1");
  
  TH1D* emap_1=new TH1D(name_1, title_1, mbins,mx1,mx2);
  
  for(ix=1; ix<=384; ++ix){
    for(iy=1; iy<=312; ++iy){
      bool take=false;
      if(ix>=177 && ix<=208 && iy>=145 && iy<=168)take=false;
      else if(ix>=145 && ix<=240 && iy>=121 && iy<=192){if(1==ix%2 && 1==iy%2)take=true;}
      else if(ix>= 97 && ix<=288 && iy>= 97 && iy<=216){if(1==ix%3 && 1==iy%3)take=true;}
      else if(ix>=  1 && ix<=384 && iy>=  1 && iy<=312){if(1==ix%6 && 1==iy%6)take=true;}
      else take=false;
      if(take){
	emap_1->Fill(emap->GetBinContent(ix,iy));
      }
    }
  }
  return emap_1;
}

template<class HIST> TH2D* makecorr_ECALmapsH(HIST* h1, HIST* h2, char* namc, char* titc){
  if( (!h1) || (!h2) ) return 0; 
  if(!isECALmapH(h1) ) return 0;
  if(!isECALmapH(h2) ) return 0;
  
  int ix, iy;
  double h1min=999999, h1max=-999999, h2min=999999, h2max=-999999;
  for(ix=1; ix<=384; ++ix){
    for(iy=1; iy<=312; ++iy){
      double g1=h1->GetBinContent(ix,iy);
      double g2=h2->GetBinContent(ix,iy);
      if( (0!=g1) && (0!=g2) ){
	if(h1max<g1)h1max=g1;
	if(h1min>g1)h1min=g1;
	if(h2max<g2)h2max=g2;
	if(h2min>g2)h2min=g2;
      }
    }
  }
  
  double lh1scale=floor(log10(h1max-h1min));
  double h1scale=pow((double)10,lh1scale);
  if( ((h1max-h1min)/h1scale)<3 )h1scale/=10;
  double h1b1=floor(h1min/h1scale);
  double h1b2=ceil(h1max/h1scale);
  int nb1=(h1b2-h1b1)*10;
  double h1x1=h1b1*h1scale;
  double h1x2=h1b2*h1scale;
  
  double lh2scale=floor(log10(h2max-h2min));
  double h2scale=pow((double)10,lh2scale);
  if( ((h2max-h2min)/h2scale)<3 )h2scale/=10;
  double h2b1=floor(h2min/h2scale);
  double h2b2=ceil(h2max/h2scale);
  int nb2=(h2b2-h2b1)*10;
  double h2x1=h2b1*h2scale;
  double h2x2=h2b2*h2scale;
  
  TH2D* hcor=new TH2D(namc,titc, nb1,h1x1,h1x2, nb2,h2x1,h2x2);
  
  for(ix=1; ix<=384; ++ix){
    for(iy=1; iy<=312; ++iy){
      bool take=false;
      if(ix>=177 && ix<=208 && iy>=145 && iy<=168)take=false;
      else if(ix>=145 && ix<=240 && iy>=121 && iy<=192){if(1==ix%2 && 1==iy%2)take=true;}
      else if(ix>= 97 && ix<=288 && iy>= 97 && iy<=216){if(1==ix%3 && 1==iy%3)take=true;}
      else if(ix>=  1 && ix<=384 && iy>=  1 && iy<=312){if(1==ix%6 && 1==iy%6)take=true;}
      else take=false;
      if(take){
	double z1=h1->GetBinContent(ix,iy);
	double z2=h2->GetBinContent(ix,iy);
	hcor->Fill(z1,z2,1);
      }
    }
  }
  return hcor;
}

template <class HIST> void saveECALdir(const char* fnam, HIST* h[64][64][4], const char* dnam, const char* tnam){
  char nm[256];
  TFile* wfil=new TFile(fnam,"UPDATE");
  
  if(!gDirectory->cd(dnam)){
    gDirectory->mkdir(dnam);
    gDirectory->cd(dnam);
  }
  //gDirectory->pwd();
  
  if(!gDirectory->cd(tnam)){
    gDirectory->mkdir(tnam);
    gDirectory->cd(tnam);
  }
  //gDirectory->pwd();
  
  for(int iz=1; iz<4; ++iz){
    if(1==iz)strcpy(nm,"O");
    else if(2==iz) strcpy(nm,"M");
    else if(3==iz) strcpy(nm,"I");
    gDirectory->mkdir(nm);
    gDirectory->cd(nm);
    //gDirectory->pwd();
    for(int iy=0; iy<64; ++iy){
      Int_t nh=0;
      for(int ix=0; ix<64; ++ix){	if(isECALcell(iz,ix,iy)) nh++; }
      if(nh>0){
	sprintf(nm,"Y%2.2d",iy);
	gDirectory->mkdir(nm);
	gDirectory->cd(nm);
	//gDirectory->pwd();
	for(int ix=0; ix<64; ++ix){
	  if(isECALcell(iz,ix,iy)){
	    h[ix][iy][iz]->Write();
	  }
	}
	gDirectory->cd("../");
	//gDirectory->pwd();
      }
    }
    gDirectory->cd("../");
    //gDirectory->pwd();
  }
  
  gDirectory->cd();
  wfil->Close();
}

template<class NUM> TH2D* toECALmap(NUM* arr, const char* nam, const char* tit){
  TH2D* h=creECALmap(nam, tit);
  for(int iECAL=0; iECAL<NECELLS; ++iECAL){
    int cid=iECAL2cellID(iECAL);
    int z,x,y;
    zxy_E(cid,z,x,y);
    fillECALmapH(h, z,x,y, arr[iECAL]);
  }
  return h;
}

template<class NUM> TH1D* toECALPINhist(NUM* arr, const char* nam, const char* tit){
  TH1D* h=new TH1D(nam,tit,496,0.,124.);
  for(int iPIN=0; iPIN<124; ++iPIN){
    for(int iLED=0; iLED<4; ++iLED){
      int ipos=4*iPIN+iLED;
      double pinamp=arr[ipos];
      h->SetBinContent(ipos+1, pinamp);
    }
  }
  return h;
}

