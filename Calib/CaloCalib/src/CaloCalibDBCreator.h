// $Id: $
#ifndef CALOCALIBDBCREATOR_H
#define CALOCALIBDBCREATOR_H 1

// Include files
// from Gaudi
class StatusCode;
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloCellIDAsProperty.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <TFile.h>


/** @class CaloCalibDBCreator CaloCalibDBCreator.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2011-08-16
 */
class CaloCalibDBCreator : public GaudiAlgorithm {
public:
  /// Standard constructor
  CaloCalibDBCreator( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloCalibDBCreator( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:


  std::map<std::string,std::vector<double> > m_coeffs;
  std::map<std::string,std::vector<int> > m_zones;
  std::string m_det;
  std::map<LHCb::CaloCellID,double> m_cali;
  std::map<LHCb::CaloCellID,double> m_post;
};

#endif // CALOCALIBDBCREATOR_H
