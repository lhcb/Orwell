//
// $Id:
// ============================================================================
//CLHEP
// Gaudi
#include "GaudiKernel/SystemOfUnits.h"


#include "CaloDet/DeCalorimeter.h"

#include "Kernel/CaloCellID.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/ODIN.h" 

// Event/CaloEvent
#include "Event/CaloAdc.h" 
// local
#include "CaloADCChannel.h"


using namespace LHCb;
//-----------------------------------------------------------------------------
// Implementation file for class CaloADCChannel
//
// 25/03/2008 : Yasmine Amhis
//-----------------------------------------------------------------------------
DECLARE_COMPONENT( CaloADCChannel )

// Standard creator
CaloADCChannel::CaloADCChannel( const std::string& name, 
					ISvcLocator* pSvcLocator) 
  : GaudiTupleAlg ( name , pSvcLocator            ){ 
  declareProperty( "Detector"   , m_nameOfDetector );
  declareProperty( "ReadoutTool"   , m_readoutTool  = "CaloDataProvider" );
}
//=============================================================================
// Standard destructor
//=============================================================================
CaloADCChannel::~CaloADCChannel() {}
//=============================================================================
//=============================================================================
// Initialisation. Check parameters
//=============================================================================


StatusCode CaloADCChannel::initialize() {
  
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  
  debug() << " ====> Initialize" << endmsg;
  m_energyTool = tool<ICaloEnergyFromRaw>( "CaloEnergyFromRaw",  name() + "Tool",this );
  m_daq = tool<ICaloDataProvider>( m_readoutTool , m_nameOfDetector + "ReadoutTool" , this );
  m_odin = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder",this);
  
  m_normal =  "TAE" != context() && "" == rootInTES();   
  

  //-----------------------------------------
  if ( "Ecal" ==  m_nameOfDetector) {
    debug() << "Detector name == ECAL  " <<  m_nameOfDetector << endmsg; 
    m_calo    = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    
    if ( m_normal) {
      m_inputData      =  LHCb::CaloAdcLocation::Ecal ;
      debug() << "Input data  ==> Normal " <<m_inputData    <<endmsg;
    }
    //----
    if(! m_normal)  {
      
      m_inputData      =   LHCb::CaloAdcLocation::Ecal ;
      debug() << "Input  data  ==> TAE Context  " << m_inputData     << endmsg; }
  }//look at the ECAL
   //-----------------------------------------
  if ( "Hcal" ==  m_nameOfDetector) {
    debug() << " Detector Name == HCAL  " <<  m_nameOfDetector << endmsg; 
    m_calo    = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
    
    if ( m_normal) {
      m_inputData      =  LHCb::CaloAdcLocation::Hcal ;
      
      debug() << "Input data  ==> Normal  " <<m_inputData    <<endmsg;
    }
    //----
    if(! m_normal)  {
      
      m_inputData      = LHCb::CaloAdcLocation::Hcal ;
      
      debug () << "Input   data  ==> TAE Context  " << m_inputData     << endmsg; }
  }  
  //Look at the HCAL
  //-----------------------------------------
  if ( "Spd" ==  m_nameOfDetector) {
    debug() << "This is the Spd  " <<  m_nameOfDetector << endmsg; 
    m_calo    = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
    
    if ( m_normal) {
      m_inputData      =  LHCb::CaloAdcLocation::Spd ;
      debug() << "Input data  ==> Normal  " <<m_inputData    <<endmsg;
    }
    
    
    if(! m_normal)  {
      m_inputData  =  rootInTES() + LHCb::CaloAdcLocation::Spd ;
   
      debug() << "Input     data  ==> TAE Context  " << m_inputData     << endmsg; }
  }  
  //Look at the Spd
  //-----------------------------------------
  if ( "Prs" ==  m_nameOfDetector) {
    debug() << "This is the Prs  " <<  m_nameOfDetector << endmsg; 
    m_calo    = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
    if ( m_normal) {
      m_inputData      =  LHCb::CaloAdcLocation::Prs ;
      debug() << "Input data ==> Normal  " <<m_inputData    <<endmsg;
    }
    
    
    if(! m_normal)  {
      m_inputData      =  rootInTES() + LHCb::CaloAdcLocation::Prs ;
      // m_inputData      =  LHCb::CaloAdcLocation::Prs ;
      debug() << "Input data  ==> TAE Context  " << m_inputData     << endmsg; }
  }   
  //Look at the Prs
  //-----------------------------------------
  
  
  return StatusCode::SUCCESS; 
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloADCChannel::execute() {
  
  
  debug() << " ====> Execute" << endmsg;
  StatusCode sc = StatusCode::SUCCESS ;
  
  Tuple tuple = nTuple("ChannelADC");
  
  
  std::vector<double> ADC;
  std::vector<double> index;
  std::vector<double> x;
  std::vector<double> y;
  std::vector<double> z;
  std::vector<double> area;
  std::vector<double> col;
  std::vector<double> row;
  std::vector<double> calo;


  // get ODIN
  debug() << " Get ODIN object for reference slot " <<endmsg;
  m_odin->getTime();
  double event=0;
  double run=0 ;
  double bunch=0;
  
  if( exist<LHCb::ODIN>(LHCb::ODINLocation::Default) ){
    LHCb::ODIN* odin = get<LHCb::ODIN> (LHCb::ODINLocation::Default);
    always()  << " Event  ==>  " << odin->eventNumber() 
	      << " Run    ==>  " << odin->runNumber() 
	      << " Bunch  ==>  " << odin->bunchId()<< endmsg;
    run   = odin->runNumber();
    event =odin->eventNumber() ;
    bunch =odin->bunchId() ;
  
    tuple->column("Run" ,run).ignore();
    tuple->column("Event" ,event).ignore();
    tuple->column("Bunch" ,bunch).ignore();
  }




  // from display
  if( !m_daq->ok() )return StatusCode::SUCCESS;
  //  if( !m_daq->getBanks() )return StatusCode::SUCCESS;
  const CaloVector<LHCb::CaloAdc>& adcs = m_daq->adcs();
  if(adcs.size() == 0)return StatusCode::SUCCESS;
  //LHCb::CaloAdcs *adcs = get<LHCb::CaloAdcs>( m_inputData );//retrive the adcs
  CaloVector<LHCb::CaloAdc>::const_iterator iadc;
  //  LHCb::CaloAdcs::const_iterator iadc;  // LHCb Calo Digits
  if( adcs.size()!=0 ) { //paranoid?
    
    for ( iadc = adcs.begin() ; adcs.end() != iadc ; ++iadc ){
      
      double ADCValue  = (double) (*iadc).adc();
      LHCb::CaloCellID id    = (*iadc).cellID(); 
      
      ADC.push_back(ADCValue);
      x.push_back(m_calo->cellX(id));
      y.push_back(m_calo->cellY(id));
      z.push_back(m_calo->cellZ(id));
      index.push_back(id.index());  
      calo.push_back(id.calo());  
      area.push_back(id.area());
      col.push_back(id.col());
      row.push_back(id.row());
      
      
    }
  }
  //======================================================================
  tuple->array("ADC" ,ADC).ignore();
  tuple->array("index" ,index ).ignore();
  tuple->array("x",x).ignore();
  tuple->array("y",y).ignore();
  tuple->array("z",z).ignore();
  
  tuple->array("area", area).ignore();
  tuple->array("col", col).ignore();
  tuple->array("row", row).ignore();
  tuple->array("calo", calo).ignore();
  sc = tuple->write(); debug() << "written tuple" << endmsg ; 

  
  return sc ; 
  
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloADCChannel::finalize() {
  
  debug() << " ===> Finalize" << endmsg;
  return GaudiTupleAlg::finalize(); 
}

