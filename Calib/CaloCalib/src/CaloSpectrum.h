// $Id: CaloSpectrum.h,v 1.7 2009-11-08 18:46:20 odescham Exp $
#ifndef CaloSpectrum_H
#define CaloSpectrum_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
// from LHCb
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"


/** @class CaloSpectrum CaloSpectrum.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-03-26
 */
class CaloSpectrum : public GaudiHistoAlg {
public:
  /// Standard constructor
  CaloSpectrum( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloSpectrum( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  DeCalorimeter* m_calo;
  std::map<std::string,ICaloDataProvider*>  m_daq;
  std::map<std::string,bool> m_ok;
  //
  bool m_1D;
  int m_threshold;
  std::string m_detectorName;
  std::string m_readoutTool;
  bool m_split;
  bool m_2x2;
  std::vector<std::string> m_slots;
  std::string m_ref;
  int m_bin;
  double  m_min;
  double m_max;
  bool m_statusOnTES;
  bool m_bary;
  double m_bmin,m_bmax;
  int m_bbin;
};
#endif
