// $Id: CaloTiming.cpp,v 1.6 2009-10-27 16:49:40 odescham Exp $
// Include files 


// local
#include "CaloTiming.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloTiming
//
// 2008-08-19 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloTiming )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloTiming::CaloTiming( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
{
  declareProperty( "Detector"             ,  m_detectorName); 
  declareProperty( "ReadoutTool"          ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "Threshold"            ,  m_threshold    = -256 );
  declareProperty( "Binning"              ,  m_bin          = 100 );
  declareProperty( "SaturationThreshold"  ,  m_sat );
  declareProperty( "CrateMonitoring"      ,  m_crate = true);

  // set default detectorName
  int index = name.find_last_of(".") +1 ; 
  m_detectorName = name.substr( index , 4 );
  if ( name.substr(index,3) == "Prs" ) m_detectorName = "Prs";  
  if ( name.substr(index,3) == "Spd" ) m_detectorName = "Spd";

  if(  "Ecal" == m_detectorName ||  "Hcal" == m_detectorName ) m_sat = 3839.;
  else if ( "Prs" == m_detectorName )m_sat = 1023.;
  else m_sat = 999999;
}
//=============================================================================
// Destructor
//=============================================================================
CaloTiming::~CaloTiming() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloTiming::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // Get Detector Element
  if ( "Ecal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  } else if ( "Prs" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs ); // Spd readout via Prs FEB
  } else {
    error() << "Unknown detector name " << m_detectorName << endmsg;
    return StatusCode::FAILURE;
  }
  // get tools
  this->setProperty("RootInTES","").ignore(); // T0
  m_t0 = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutToolT0" , this );
  this->setProperty("RootInTES","Next1").ignore(); // Next1
  m_next = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutToolNext1" , this );
  this->setProperty("RootInTES","Prev1").ignore(); // Prev1
  m_prev = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutToolPrev1" , this );
  this->setProperty("RootInTES","").ignore(); // reset


  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloTiming::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;


  // Get Raw + Calo Bank
  if(!m_t0->ok() || !m_next->ok() || !m_prev->ok() ){
    debug()<<  "The Readout tool cannot load the TAE rawEvent sequence ==> STOP processing" << endmsg;
    return StatusCode::SUCCESS;
  }

  
  // run over T0
  const CaloVector<LHCb::CaloAdc>& adcs = m_t0->adcs() ;
  for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
    LHCb::CaloCellID id = (*iadc).cellID();
    if( id.isPin() )continue;
    double adc  = (double) (*iadc).adc();
    if( adc < m_threshold)continue;
    double adcN = (double) m_next->adc( id );
    double adcP = (double) m_prev->adc( id );
    if( adcN<0) adcN=0;
    if( adcP<0) adcP=0;
    
    // saturation check
    if( adc >= m_sat || adcN >= m_sat || adcP >= m_sat )continue;

    double sum    = (adcP+adcN+adc);
    if (sum<=0)continue;
    double kernel = (adcP + adcN)/sum;
    if(adcP > adcN)kernel = -kernel;
    double as1    = (adc-adcP)/(adc+adcP);
    double as2    = (adcN-adc)/(adc+adcN);


    // per calo
    plot1D( as1,  "Timing/Asymmetry/1" , m_detectorName + " (T0-Prev)/(Prev+T0) asymmetry "   , -1.01 , +1.01 , m_bin);
    plot1D( as2,  "Timing/Asymmetry/2" , m_detectorName + " (Next-T0)/(T0+Next) asymmetry "   , -1.01 , +1.01 , m_bin);
    plot1D( kernel,"Timing/Kernel/1", m_detectorName + " sgn(Next-Prev)*(Next+Prev)/(Prev+T0+Next) kernel", -1.01, +1.01 , m_bin);
    // per crate
    if( m_crate ){
      int card  = m_calo->cardNumber (id );
      int crate = m_calo->cardCrate( card );
      std::ostringstream scrate;
      scrate << "Crate" << format("%02i",crate) << "/" ;
      plot1D( as1,  "Timing/Asymmetry/"+ scrate.str() + "1" , 
              m_detectorName + scrate.str() + " (T0-Prev)/(Prev+T0) asymmetry "   , -1.01 , +1.01 , m_bin);
      plot1D( as2,  "Timing/Asymmetry/"+ scrate.str() + "2" , 
              m_detectorName + scrate.str() + " (Next-T0)/(T0+Next) asymmetry "   , -1.01 , +1.01 , m_bin);
      plot1D( kernel,"Timing/Kernel/"  + scrate.str() + "1", 
              m_detectorName + scrate.str() + " sgn(Next-Prev)*(Next+Prev)/(Prev+T0+Next) kernel", -1.01, +1.01 , m_bin); 
    }
    
    // per channel

    profile1D(  (double) id.index(), as1,  "Timing/Asymmetry/Profile/1",
               m_detectorName+" <(T0-Prev)/(Prev+T0)>  versus CaloCellID" , 0, 16384., 16384);
    profile1D(  (double) id.index(), as2, "Timing/Asymmetry/Profile/2",
               m_detectorName+" <(Next-T0)/(T0+Next)>  versus CaloCellID" , 0, 16384., 16384);    
    profile1D(  (double) id.index(), kernel, "Timing/Kernel/Profile/2",
               m_detectorName+" <sgn(Next-Prev)*(Next+Prev)/(Prev+T0+Next)>  versus CaloCellID" , 0, 16384., 16384);
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloTiming::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
