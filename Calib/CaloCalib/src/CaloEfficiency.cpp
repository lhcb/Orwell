// $Id: CaloEfficiency.cpp,v 1.3 2010-10-11 10:54:03 odescham Exp $
// Include files 
#include <fstream>
#include "GaudiUtils/Aida2ROOT.h"
// from LHCb
#include "Event/ODIN.h"
#include "CaloUtils/CaloAlgUtils.h"
// local
#include "CaloEfficiency.h"


using namespace LHCb;
using namespace Gaudi::Units;
using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : CaloEfficiency
//
// 2009-05-20 : Alessandro Camboni  ////  acamboni@ecm.ub.es
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloEfficiency )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloEfficiency::CaloEfficiency( const std::string& name,
			ISvcLocator* pSvcLocator)
  : Calo2Dview ( name , pSvcLocator ),
    m_currPat(-1),
    m_ok(true),
    m_nPattern(0)
{
  declareProperty( "ReadoutTool"    , m_readoutTool  = "CaloDataProvider" );
  declareProperty( "Detector"       , m_detectorName = "Spd" );
  declareProperty( "ADCThreshold"   , m_thresh = 0.5 );
  declareProperty( "StatusOnTES"    , m_statusOnTES = true);
  declareProperty( "CheckNCell"     , m_cellCheck = 50);
  declareProperty( "Sequence"       , m_sequence      );
  declareProperty( "SplitBXParity"  , m_splitParity = false );
  declareProperty( "ActiveCounters" , m_activeCounter = false ); // active debugging counter stat in finalize (offline)

  setOneDimension(true); // produce 1D views by default
  m_profile = true;
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name );
  if( "Spd" != m_detectorName)m_thresh = 30.0;
}


//=============================================================================
// Destructor
//=============================================================================
CaloEfficiency::~CaloEfficiency() {} 


//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloEfficiency::initialize() {
  StatusCode sc = Calo2Dview::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;



  //--- Get Detector Element
  m_det = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( m_detectorName ) );
  if( !m_det )return StatusCode::FAILURE;

    

  //--- Get Calo readout tool
  m_daq = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool", this );
  //--- Get Odin decoder
  m_odin = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder",this);

  m_nChannels = m_det->numberOfCells();
  // default sequence if not defined
  if( 0 == m_sequence.size() ){
    info() << "NO sequence defined : assume a single sequence with all channels" << endmsg;
    const CaloVector<CellParam>& cells = m_det->cellParams();
    for(CaloVector<CellParam>::const_iterator  ic = cells.begin() ; ic != cells.end() ; ++ic){
      const LHCb::CaloCellID id = (*ic).cellID();
      if( id.isPin() )continue;
      m_sequence["0"].push_back( id.index() );
    }
  } 
  
  //--- Read Pattern file -----------------------
  m_cellPattern.clear();
  //
  m_nPattern = m_sequence.size();

  unsigned int nCells = 0;
  for( std::map<std::string,std::vector<int> >::iterator it = m_sequence.begin() ; it != m_sequence.end(); ++it){
    std::istringstream is( ((*it).first).c_str() );
    int pattern ;
    is >> pattern;
    const std::vector<int>& cells = (*it).second;
    nCells += cells.size();
    for( std::vector<int>::const_iterator ic = cells.begin() ; ic != cells.end() ; ++ic){
      const LHCb::CaloCellID cellID = LHCb::CaloCellID(*ic);
      m_cellPattern.addEntry(pattern , cellID ); //gives what pattern a cell belongs to
    }
  }
  if( nCells != m_nChannels){
    m_ok = false;
    info() << "Sequence size : " << m_sequence.size() << " / Ncells :" << m_nChannels << endmsg;
    return Warning("Incomplete LED sequence -> No processing",StatusCode::SUCCESS);
  }  
  
  //----------------------------------------------
  std::string m_st = Gaudi::Utils::toString( m_thresh );
  // time slot
  m_timeSlot = rootInTES();
  if( "" == m_timeSlot )m_timeSlot = "T0";

  // booking histograms
  if( m_splitParity){
    bookCalo2D( "OddBX/flash"          , "Signal rate in " + m_timeSlot +" (Odd BX)" , m_detectorName);
    bookCalo2D("EvenBX/flash"          , "Signal rate in " + m_timeSlot + " (Even BX)" , m_detectorName);
    bookCalo2D("OddBX/unflash"         , "Noise rate in " + m_timeSlot + " (Odd BX)" ,m_detectorName);
    bookCalo2D("EvenBX/unflash"        , "Noise rate in " + m_timeSlot + " (Even BX)" ,m_detectorName);
    if( !m_profile){
      bookCalo2D("OddBX/flashCounter"    , "Signal rate  counter in " + m_timeSlot + " (Odd BX)" ,m_detectorName);
      bookCalo2D("EvenBX/flashCounter"   , "Signal rate  counter in " + m_timeSlot + " (Even BX)" ,m_detectorName);
      bookCalo2D("OddBX/unflashCounter"  , "Noise rate counter in " + m_timeSlot + " (Odd BX)" ,m_detectorName);
      bookCalo2D("EvenBX/unflashCounter" , "Noise rate counter in " + m_timeSlot + " (Even BX)" ,m_detectorName);
    } 
  }
  else{
    bookCalo2D("flash"          , "Signal rate  in " + m_timeSlot + " (threshold "+m_st+" ADC)", m_detectorName);
    bookCalo2D("unflash"        , "Noise rate in " + m_timeSlot  + " (threshold "+m_st+" ADC)" , m_detectorName);
    if( !m_profile){
    bookCalo2D("flashCounter"   , "Signal rate  counter in " + m_timeSlot   + " (threshold "+m_st+" ADC)" ,m_detectorName);
    bookCalo2D("unflashCounter" , "Noise rate counter in " + m_timeSlot + " (threshold "+m_st+" ADC)" , m_detectorName);
    } 
  }
  if(m_splitParity)h0 = book1D( "Counters/1" , m_detectorName + ": Event rate  (Even BX)" , 0 ,3 ,3 );
  else h0 = book1D( "Counters/1" , m_detectorName + ": Event rate  " , 0 ,3 ,3 );
  TH1D* th = Gaudi::Utils::Aida2ROOT::aida2root( h0 );
  th->GetXaxis()->SetBinLabel(1, std::string("Events").c_str() );
  th->GetXaxis()->SetBinLabel(2, std::string("<Signal>").c_str() );
  th->GetXaxis()->SetBinLabel(3, std::string("<Pedestal>").c_str() );

  h02 = NULL;
  if( m_splitParity){
    h02 = book1D( "Counters/2" , m_detectorName + ": Event rate (Odd BX)  " , 0 ,3 ,3 );
    TH1D* th2 = Gaudi::Utils::Aida2ROOT::aida2root( h02 );
    th2->GetXaxis()->SetBinLabel(1, std::string("Events").c_str() );
    th2->GetXaxis()->SetBinLabel(2, std::string("<Signal>").c_str() );
    th2->GetXaxis()->SetBinLabel(3, std::string("<Pedestal>").c_str() );
  }
  
  //
  h1 =NULL;
  h2 =NULL;
  h3 =NULL;
  h4 =NULL;
  p1 =NULL;
  p2 =NULL;


  return StatusCode::SUCCESS;
}
  



//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloEfficiency::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  if( !m_ok) return StatusCode::SUCCESS;


  StatusCode sc;
  // get adcs
  if(!m_daq->ok())return StatusCode::SUCCESS;  // OD getBanks no longer needed  
  const CaloVector<LHCb::CaloAdc>& adcs = m_daq->adcs();
  if(m_statusOnTES)  m_daq->putStatusOnTES(); 

  //--- Get ODIN
  m_odin->getTime();
  long bunch=0;
  std::string parity = "";
  if( exist<ODIN>(ODINLocation::Default) ){
    ODIN* odin = get<ODIN> (ODINLocation::Default);
    bunch = odin->bunchId();
    int bxParity = bunch%2;
    if(m_splitParity)parity =  (bxParity == 0) ? "EvenBX/" : "OddBX/";
    if(m_activeCounter)counter("Parity : " + parity)+=1;
    //    info() << odin->eventNumber() << " / " << bunch << " / " << parity << endmsg;
  }
  else if(m_splitParity)
    return Warning("ODIN bank not found - cannot analyze data per BX parity",StatusCode::SUCCESS);
  
  
  //------- DETERMINE CURRENT PATTERN -------------------------------------
  //-----------------------------------------------------------------------    
  m_currPat = findPattern(adcs);
  if( m_currPat < 0 ){
    if( adcs.size() > m_cellCheck){
      if(m_activeCounter)counter("Pattern not found") += 1;
      return Warning( "Pattern not found in non empty flash event",StatusCode::SUCCESS);
    } else
      if(m_activeCounter)counter("Empty flash event") += 1;
  }


  
  //-----------------------------------------------------------------------
  //
  
  //
  //------- FILL MONITORING HISTOGRAMS ------------------------------------
  //----------------------------------------------------------------------- 


  const HistoID uFlash  = HistoID(parity + "flash");
  const HistoID uUnflash =  HistoID(parity + "unflash");
  const HistoID uFlashCounter  = HistoID(parity + "flashCounter");
  const HistoID uUnflashCounter = HistoID( parity + "unflashCounter" );

  // preload histo to speed-up 1D filling
  if( m_1d){
    if( !m_profile ){
      h1 = histo1D( uFlash );
      h2 = histo1D( uUnflash );
      h3 = histo1D( uFlashCounter );
      h4 = histo1D( uUnflashCounter );
    }
    else{
      p1 = profile1D( uFlash );
      p2 = profile1D( uUnflash );
    }
  }
  

  // loop over all channels (as defined in the sequence)
  double aSig = 0.;
  double aPed = 0.;
  for( std::map<std::string,std::vector<int> >::const_iterator it = m_sequence.begin() ; it != m_sequence.end(); ++it){
    const std::vector<int>& cells = (*it).second;
    for( std::vector<int>::const_iterator ic = cells.begin() ; ic != cells.end() ; ++ic){
      const LHCb::CaloCellID& cellID = LHCb::CaloCellID( *ic );
      double adc = (double) m_daq->adc(cellID);
      double val = (adc > m_thresh) ? 1. : 0.;
      
      if(m_cellPattern[cellID] == m_currPat) {
        aSig+=1;
        if( m_profile && m_1d && NULL != p1)p1->fill((double) cellID.index() , val );     // flash  profile 1D          
        else if( m_profile && !m_1d)fillCalo2D( uFlash, cellID, val);                     // flash profile 2D
        else if(!m_profile && m_1d && NULL != h3)h3->fill((double) cellID.index() );      // counter 1D          
        else if(!m_profile && !m_1d)fillCalo2D( uFlashCounter, cellID, 1.);               // counter 2D
        if( !m_profile && val > 0.5 ){
          if(m_1d && NULL != h1)h1->fill((double) cellID.index() );    // flash 1D
          else fillCalo2D( uFlash, cellID, 1.);                        // flash 2D
        }
      }  
      else{
        aPed += 1.;
        if( m_profile && m_1d && NULL != p2)p2->fill((double) cellID.index() , val );     // NO flash  profile 1D          
        else if( m_profile && !m_1d)fillCalo2D( uUnflash, cellID, val);                   // NO flash profile 2D
        else if(!m_profile && m_1d && NULL != h4)h4->fill((double) cellID.index() );      // counter 1D          
        else if(!m_profile && !m_1d)fillCalo2D( uUnflashCounter, cellID, 1.);             // counter 2D

        if( !m_profile && val > 0.5 ){
          if(m_1d && NULL != h2)h2->fill((double) cellID.index() );    // NO flash 1D
          else fillCalo2D( uUnflash, cellID, 1.);                      // NO flash 2D
        }
      } 
    }
  }
  
  
  // counters
  aSig = ( m_nChannels == 0 ) ? 0. : aSig  / (double) m_nChannels;
  aPed = ( m_nChannels == 0 ) ? 0. : aPed  / (double) m_nChannels;
  if( !m_splitParity || parity == "EvenBX/"){
    fill(h0 , 0 , 1.);
    fill(h0 , 1 , aSig); 
    fill(h0 , 2 , aPed); 
  }
  if( m_splitParity && parity == "OddBX/" ){
    fill(h02 , 0 , 1.);
    fill(h02 , 1 , aSig); 
    fill(h02 , 2 , aPed); 
  }

  return StatusCode::SUCCESS;

}



//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloEfficiency::finalize() {

  
  StatusCode sc;
  

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  
  
  return Calo2Dview::finalize();  // must be called after all other actions
  
}

//=============================================================================
//  findPattern
//=============================================================================

int CaloEfficiency::findPattern(const CaloVector<LHCb::CaloAdc>& adcs) {
  if(m_activeCounter)counter("Pattern finder") += 1;
  if( m_sequence.size() == 0)return -1;
  if( m_sequence.size() == 1)return 0;  

  //Arrays for counting how many cells have a 1 in each pattern
  std::vector<unsigned int> countXPattern;
  for( int i = 0 ; i < m_nPattern ; ++i){ 
    countXPattern.push_back( 0 );  
  } 
  int initPattern = -1;
  for( CaloVector<LHCb::CaloAdc>::const_iterator iAdc = adcs.begin(); iAdc != adcs.end(); ++iAdc ) {
    if( (double) (*iAdc).adc() < m_thresh )continue;
    const LHCb::CaloCellID& cellID = (*iAdc).cellID();
    if(++countXPattern[m_cellPattern[cellID]]> m_cellCheck) {
      initPattern = m_cellPattern[cellID];
      break;
    }
  }
  //----------------------------------------------------------------------------
  //--------------- Debugging printout -----------------------------------------
  //----------------------------------------------------------------------------
  for(int i=0; i<m_nPattern; i++) {
    debug() << "Count in pattern " << i  << " : " << countXPattern[i] << endmsg;  
    if(m_activeCounter)counter("#Flash in pattern " + Gaudi::Utils::toString(i)) += (countXPattern[i] > m_cellCheck) ? 1 :0 ;
;
  }
  return initPattern;
}

