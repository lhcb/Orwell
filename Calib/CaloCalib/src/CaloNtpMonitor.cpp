// $Id: CaloNtpMonitor.cpp,v 1.21 2009-11-09 10:58:29 odescham Exp $
// Include files 

#include "GaudiKernel/SmartIF.h" 
//From LHCb
#include "Event/ODIN.h" 

// local
#include "CaloNtpMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloNtpMonitor
//
// 2007-04-13 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloNtpMonitor )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloNtpMonitor::CaloNtpMonitor( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : GaudiTupleAlg ( name , pSvcLocator )
{

  declareProperty( "Detector"             ,  m_detectorName); 
  declareProperty( "ReadoutTool"          ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "L0ReadoutTool"        ,  m_l0ReadoutTool  = "CaloL0DataProvider" );
  declareProperty( "Banks"                ,  m_banks);
  declareProperty( "Threshold"            ,  m_thresh=-256);
  declareProperty( "MaxAdcArray"          ,  m_max=-1);
  declareProperty( "LinkToPin"            ,  m_pin=false);
  declareProperty( "ReferenceTimeSlot"    ,  m_ref="T0"); 
  declareProperty( "AdditionalTimeSlots"  ,  m_slots);
  declareProperty( "PinTimeSlot"          ,  m_pinSlot);
  declareProperty( "L0ADC"                ,  m_l0=false);
  declareProperty( "StatusOnTES"          ,  m_statusOnTES = true);
  declareProperty( "ForceAdditionalSlots" ,  m_force = false);
  declareProperty( "InspectAllChannels"   ,  m_inspectAll = true);

  m_first = true;
  m_pinSlot = m_ref;
  // set default detectorName
  int index = name.find_last_of(".") +1 ; 
  m_detectorName = name.substr( index , 4 );
  if ( name.substr(index,3) == "Prs" ) m_detectorName = "Prs";  
  if ( name.substr(index,3) == "Spd" ) m_detectorName = "Spd"; 
}
//=============================================================================
// Destructor
//=============================================================================
CaloNtpMonitor::~CaloNtpMonitor() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloNtpMonitor::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;



  // Get Detector Element
  if ( "Ecal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  } else if ( "Prs" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
  } else {
    error() << "Unknown detector name " << m_detectorName << endmsg;
    return StatusCode::FAILURE;
  }  


  // Max ADC array size if not defined via options
  int num = (int) (m_calo->numberOfCells() + m_calo->numberOfPins());
  if(m_max<0 || m_inspectAll ){
    m_max = num;
    info() << " Max ADC array size is set to " << num << endmsg;
  }
  if(m_max < num){
    warning() << "the size of the ADC arrays is lower than the number of channels ( "
              << num << ")" << endmsg;
    warning() << " Possibly troubles filling the ntuple if ADC threshold is too low or if all banks are requested"<< endmsg;
  }
  
  bool pinSlotExist = false;
  // get DAQ tools
  // for the reference slot
  this->setProperty("RootInTES","").ignore();//reset
  if(m_ref != "T0")this->setProperty("RootInTES",m_ref).ignore();
  m_daq = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" , this );
  if(m_l0)m_l0daq = tool<ICaloL0DataProvider>( m_l0ReadoutTool , m_detectorName + "L0ReadoutTool" , this );
  m_odin = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder",this);
  info() << " -> DAQ tool for reference slot " << m_ref << " instantiated as "<<m_detectorName + "ReadoutTool" <<endmsg;
  if(m_l0)info() << " -> L0ADC DAQ tool for reference slot " << m_ref << " instantiated as "
                 <<m_detectorName + "L0ReadoutTool" <<endmsg;
  info()<< "   ---> RawEvent will be loaded from  root in TES: '"<< m_ref << "'" << endmsg;

  if(m_pinSlot == m_ref)pinSlotExist = true;

  // for additional slot
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if(m_pinSlot == *islot)pinSlotExist = true;
    if( *islot == m_ref )continue;
    m_ok[*islot]=true;
    m_adj[*islot]=true;
    m_l0ok[*islot] = (m_l0) ? true : false;
    m_l0adj[*islot]=  (m_l0) ? true : false ;
    this->setProperty("RootInTES","").ignore();//reset
    if(*islot != "T0")this->setProperty("RootInTES",*islot).ignore();
    m_daqs[*islot] = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" + *islot , this );
    if(m_l0)m_l0daqs[*islot] = tool<ICaloL0DataProvider>( m_l0ReadoutTool , m_detectorName + "L0ReadoutTool" + *islot , this );
    m_odins[*islot] = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder" + *islot ,this);
    info() << " -> DAQ tool for adjacent slot " << *islot << " instantiated as " 
           << m_detectorName + "ReadoutTool" + *islot << endmsg;
    if(m_l0)info() << " -> L0ADC DAQ tool for adjacent slot " << *islot << " instantiated as " 
                   << m_detectorName + "L0ReadoutTool" + *islot << endmsg;
    info()<< "   ---> RawEvent will be loaded from  root in TES: '"<< *islot << "'" << endmsg;
    // check roots in TES are uniquely defined
    for(std::vector<std::string>::iterator jslot = m_slots.begin(); jslot != islot ;  ++jslot){
      if( *jslot == *islot ){
        error() << "the  RootInTES " << *islot << " is  defined twice" << endmsg;
        return StatusCode::FAILURE;
      }
    }
  }
  this->setProperty("RootInTES","").ignore();//reset
  if(m_ref != "T0")this->setProperty("RootInTES",m_ref).ignore();

  if( !pinSlotExist){
    error() << "Requested time slot "<< m_pinSlot << " for PIN-diode linked to ADC channels in slot " << m_ref 
            << " MUST  be declared as  AdditionalTimeSlot" << endmsg;
    m_pin = false;
  }
  else{
    info() << "PIN-diode ADC in time slot " << m_pinSlot << " will be linked to ADC channel in slot " << m_ref << endmsg;
  }
  

  // PrintOut
  int nTell1s = m_calo->numberOfTell1s();
  if(m_banks.size() != 0 ){
    info() << "Full monitoring of " << m_banks.size() << " " << m_detectorName 
           << " Tell1 among " << nTell1s << " :  ids = ( " 
           << m_banks << ")" << endmsg;
  }
  else {
    info() << "Full monitoring of ALL " << nTell1s << " "  << m_detectorName <<" Tell1s " << endmsg;
  }
  this->setProperty("RootInTES","").ignore();//reset
  //
  if( 0 == m_banks.size() )m_banks.push_back(-1); // decode all banks
  //
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloNtpMonitor::execute() {

  debug() << "==> Execute" << endmsg;


  Tuple ntp = nTuple(500,m_detectorName + "NtpMonitor" ,CLID_ColumnWiseTuple);
  StatusCode sc;




  // get ODIN bank
  debug() << " Get ODIN object for reference slot " <<endmsg;
  m_odin->getTime();
  std::string pref;
  if(m_ref != "T0")pref =  m_ref  + "/";
  if( exist<LHCb::ODIN>(pref + LHCb::ODINLocation::Default) ){
    LHCb::ODIN* odin = get<LHCb::ODIN> (pref + LHCb::ODINLocation::Default);
    debug() << " EVENT " << odin->eventNumber() << endmsg;
    sc=ntp->column("run"  +m_ref , odin->runNumber()         );
    sc=ntp->column("event"+m_ref , (long) odin->eventNumber()       );
    sc=ntp->column("bunch"+m_ref , odin->bunchId()           );
    sc=ntp->column("orbit"+m_ref, odin->orbitNumber()       );
    sc=ntp->column("time" +m_ref , (long) odin->gpsTime()           );
    sc=ntp->column("ttype"+m_ref , odin->triggerType()       );
    sc=ntp->column("btype"+m_ref , odin->bunchCrossingType() );
    sc=ntp->column("etype"+m_ref , odin->eventType() );
    sc=ntp->column("step" +m_ref , odin->calibrationStep() );
    sc=ntp->column("window" +m_ref , odin->timeAlignmentEventWindow() );
  }else{
    Warning( "Emtpy location for ODIN '"+ pref + LHCb::ODINLocation::Default +"'" ).ignore();
  }

  // Get Raw + Calo Bank
  if(!m_daq->ok()){
    Error( "The Readout tool cannot load the rawEvent for the reference time slot " 
           + m_ref + "  ==> STOP processing" , StatusCode::SUCCESS).ignore();
    return StatusCode::SUCCESS;
  }

  if(m_l0 && !m_l0daq->ok()){
    Warning( "The L0-ADC Readout tool cannot load the rawEvent for the reference time slot " 
             + m_ref , StatusCode::SUCCESS).ignore();
    m_l0=false;
    m_l0ok[m_ref]=false;
  }
    
        

  // Additional slots
  for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){
    m_ok[*islot]= m_daqs[*islot]->ok(); 
    if( !m_ok[*islot] )Warning("Readout tool cannot load the rawEvent for slot " + *islot ).ignore() ;
    m_l0ok[*islot] = (m_l0) ? m_l0daqs[*islot]->ok()  : false ;
    if(m_l0 && !m_l0ok[*islot] )Warning("L0 Readout tool cannot load the rawEvent for slot " + *islot ).ignore() ;
    if(m_first && !m_force){ // first event decide if additional slots are available
      m_adj[*islot] = m_ok[*islot];
      m_l0adj[*islot] = m_l0ok[*islot];
      debug() << "Time slot " << *islot << " --> " << m_adj[*islot] << endmsg;
    }
  }
  if(m_first)m_first  = false;


  for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){

    if(!m_ok[*islot])continue; // temp    

    debug() << " Get ODIN object for slot " << *islot << endmsg;
    m_odins[*islot]->getTime();    
    std::string pref;
    if(*islot != "T0")pref = *islot + "/";
    if( exist<LHCb::ODIN>(pref +LHCb::ODINLocation::Default) ){
      LHCb::ODIN* odin=get<LHCb::ODIN> (pref + LHCb::ODINLocation::Default);
      debug() << " Retrieve ODIN bank for slot " << *islot << endmsg;
      sc=ntp->column("run"   +*islot  , odin->runNumber()         );
      sc=ntp->column("event" +*islot  , (long) odin->eventNumber() );
      sc=ntp->column("bunch" +*islot  , odin->bunchId()           );
      sc=ntp->column("orbit" +*islot  , odin->orbitNumber()       );
      sc=ntp->column("time"  +*islot  , (long) odin->gpsTime()  );
      sc=ntp->column("ttype" +*islot  , odin->triggerType()       );
      sc=ntp->column("btype" +*islot  , odin->bunchCrossingType() );
      sc=ntp->column("etype" +*islot  , odin->eventType() );
      sc=ntp->column("step"  +*islot  , odin->calibrationStep() );
      sc=ntp->column("window" +*islot , odin->timeAlignmentEventWindow() );
    }
    else{ 
      Warning( " Emtpy location for ODIN '" + pref + LHCb::ODINLocation::Default  + "'" ).ignore();
    } 
  }
  
  // put the relevant list of CaloADC in a container
  std::vector<LHCb::CaloAdc> adcCont ;
  if( m_inspectAll ){
    const CaloVector<CellParam>& cells = m_calo->cellParams();
    for(CaloVector<CellParam>::const_iterator icel = cells.begin(); icel != cells.end() ; ++icel){
      LHCb::CaloCellID id = (*icel).cellID();
      int card =  (*icel).cardNumber();
      int tell1 = m_calo->cardToTell1( card ) ;
      bool keep = false;
      for(std::vector<int>::iterator bank = m_banks.begin() ; bank != m_banks.end() ; ++bank){
        if( *bank == tell1 || *bank < 0){
          keep = true;
          break;
        }
      }
      if(keep){
        int adc = m_daq->adc( id );
        LHCb::CaloAdc caloAdc( id , adc );
        if ( adc > m_thresh )adcCont.push_back( caloAdc );
      }
    }
  }else{
    for(std::vector<int>::iterator bank = m_banks.begin() ; bank != m_banks.end() ; ++bank){
      const CaloVector<LHCb::CaloAdc>& caloAdcs = (m_daq->adcs(*bank));
      for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = caloAdcs.begin();iadc!= caloAdcs.end();++iadc){
        if ( (*iadc).adc() < m_thresh )continue;
        adcCont.push_back( *iadc );
      }
    }    
  }
  //  Loop over selected caloADCs
  std::map<std::string , std::vector<int> > adcs,l0adcs;
  std::vector<int> ids,febs,tell1s,pins,chans;
  for(std::vector<LHCb::CaloAdc>::iterator iadc = adcCont.begin() ; iadc != adcCont.end() ; ++iadc){
    if ( (*iadc).adc() < m_thresh )continue;
    LHCb::CaloCellID cellID = (*iadc).cellID();
    int febChannel = m_calo->cellParam(cellID).cardRow() * 8 + m_calo->cellParam(cellID).cardColumn();
    chans.push_back(febChannel);
    adcs[m_ref].push_back( (*iadc).adc() );
    ids.push_back(  cellID.all()  );
    int feb   = m_calo->cellParam( cellID ).cardNumber();
    febs.push_back(    m_calo->cardParam(feb).code() );
    tell1s.push_back(  m_calo->cardToTell1( feb )    );
    
    // l0-ADCs
    if( m_l0 )l0adcs[m_ref].push_back( m_l0daq->l0Adc(cellID) );
    
    // Fill the arrays for the other times slot
    for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){
      if( m_ref == *islot )continue;
      if( m_adj[*islot] )
        (m_ok[*islot]) ? adcs[*islot].push_back(m_daqs[*islot]->adc( cellID ) ) : adcs[*islot].push_back( -999 );
      if( m_l0adj[*islot])
          (m_l0ok[*islot])?l0adcs[*islot].push_back( m_l0daqs[*islot]->l0Adc(cellID) ):l0adcs[*islot].push_back( -999 ); 
    }
    // Fill Pin ADC
    if( m_pin ){
      const std::vector<int>& leds = m_calo->cellLeds(cellID);
      int pinMax = -256;
      for(std::vector<int>::const_iterator iled = leds.begin() ; iled != leds.end() ; ++iled){
        LHCb::CaloCellID pinID = m_calo->caloLeds()[*iled].pin();
        int pinADC = -999;
        if(m_pinSlot == m_ref ){
          pinADC = m_daq->adc( pinID );
        }else{
          if( m_adj[m_pinSlot] )
            pinADC = (m_ok[m_pinSlot]) ?  m_daqs[m_pinSlot]->adc( pinID ) : -999 ;
        }
        if(pinADC > pinMax )pinMax = pinADC;
      }
      pins.push_back(pinMax); 
    }
  }
  debug() << "#Channels in reference time slot : " << adcs[m_ref].size() << endmsg;
  sc=ntp->farray("cellid" , ids      ,"Nchannels",m_max);
  sc=ntp->farray("feb"    , febs     ,"Nchannels",m_max); 
  sc=ntp->farray("tell1"  , tell1s   ,"Nchannels",m_max); 
  sc=ntp->farray("febchannel", chans    ,"Nchannels",m_max); 
  debug() << "insert adc in NTP" << endmsg;
  
  // ADC arrays
  sc=ntp->farray("adc" +m_ref+"Ref"    , adcs[m_ref]  ,"Nchannels",m_max);   
  debug() << "insert l0 adc in NTP" << endmsg;
  if(m_l0)sc=ntp->farray("L0Adc" +m_ref+"Ref"    , l0adcs[m_ref]  ,"Nchannels",m_max);   

  for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){
    std::string name = "adc" + *islot; 
    if(m_adj[*islot]){
      sc=ntp->farray(name    , adcs[*islot]  ,"Nchannels",m_max);   
      debug() << "NChannels in slot " << *islot << " : " <<  adcs[m_ref].size() << endmsg;
    }
    if(m_l0 && m_l0adj[*islot])sc=ntp->farray("L0" + name    , l0adcs[*islot]  ,"Nchannels",m_max);   
  }   
  
  if( m_pin ){
    debug() << "NChannels in for PINS " << pins.size() << endmsg;
    sc=ntp->farray("pinADC"  , pins   ,"Nchannels",m_max); 
  }
  
  //
  debug() << "Write NTP " << endmsg;
  sc=ntp->write();

  // put Readout Status on TES
  if(m_statusOnTES){
    m_daq->putStatusOnTES();
    if(m_l0 && NULL != m_l0daq )m_l0daq->putStatusOnTES();
    for( std::vector<std::string>::iterator islot = m_slots.begin() ; islot != m_slots.end() ; ++islot){
      if(NULL!=m_daqs[*islot]) m_daqs[*islot]->putStatusOnTES();
    } 
  }


  
  return sc;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloNtpMonitor::finalize() {

  debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
