// $Id: CaloSpectrum.cpp,v 1.11 2010-10-11 10:54:51 odescham Exp $
// Include files 

#include "GaudiKernel/SystemOfUnits.h"
// local
#include "CaloSpectrum.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloSpectrum
//
// 2008-03-26 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloSpectrum )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloSpectrum::CaloSpectrum( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
{
  declareProperty( "Plot1D"        , m_1D = true);
  declareProperty( "Threshold"     , m_threshold = -257);
  declareProperty( "Detector"      ,  m_detectorName); 
  declareProperty( "ReadoutTool"   ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "SplitAreas"    ,  m_split=false);
  declareProperty( "Cluster2x2"    ,  m_2x2=false);
  declareProperty( "AddSlots"      ,  m_slots);
  declareProperty( "RefSlot"       ,  m_ref ="T0"); 
  declareProperty( "NBins"         ,  m_bin=4096);
  declareProperty( "Min"           ,  m_min=-256);
  declareProperty( "Max"           ,  m_max=3840);
  declareProperty( "StatusOnTES"   ,  m_statusOnTES = true);
  declareProperty( "Asymmetries"   ,  m_bary = false);
  declareProperty( "MinBary"       ,  m_bmin=-1000.*Gaudi::Units::mm);
  declareProperty( "MaxBary"       ,  m_bmax=+1000.*Gaudi::Units::mm);
  declareProperty( "NBinsBary"     ,  m_bbin=100);

  // set default detectorName
  int index = name.find_last_of(".") +1 ; 
  m_detectorName = name.substr( index , 4 );
  if ( name.substr(index,3) == "Prs" ) m_detectorName = "Prs";  
  if ( name.substr(index,3) == "Spd" ) m_detectorName = "Spd";



  if ( "Ecal" == m_detectorName ) {
    m_bin = 4096;
    m_min = -256;
    m_max = 3840;
  } else if ( "Hcal" == m_detectorName ) {
    m_bin = 4096;
    m_min = -256;
    m_max = 3840;
  } else if ( "Prs" == m_detectorName ) {
    m_bin = 1024;
    m_min = 0;
    m_max = 1024;
  } else if ( "Spd" == m_detectorName ) {
    m_bin = 2;
    m_min = 0;
    m_max = 2;
  }

  setHistoDir( name );

}
//=============================================================================
// Destructor
//=============================================================================
CaloSpectrum::~CaloSpectrum() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloSpectrum::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiHistoAlg

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;


  //

  // Get Detector Element
  if ( "Ecal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  } else if ( "Prs" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs ); // Spd readout via Prs FEB
  } else {
    error() << "Unknown detector name " << m_detectorName << endmsg;
    return StatusCode::FAILURE;
  }
  // store cell position to avoid multiple m_calo->cellCenter() accesses


  // get tools
  m_slots.push_back(m_ref);
  int nArea = m_calo->numberOfAreas();

  for(std::vector<std::string>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
    std::string slot = *is;
    m_ok[slot]=true;
    if(slot != "T0"){
      this->setProperty("RootInTes", slot).ignore();
    }else{
      this->setProperty("RootInTes", "").ignore();
    }
    m_daq[slot] = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" + slot , this );
  }

  // reset
  this->setProperty("RootInTes", "").ignore();

  // book histos :
  std::string tit = m_detectorName ;
  std::string s = m_ref ;
  for(std::vector<std::string>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
    if( *is == m_ref)continue;
    s += "," + *is;
  }
  

  if(m_2x2)tit += " 2x2 " ;
  tit += " ADC spectrum for time slots (" +  s +") ";

  if( m_1D){
    book1D( "1" , tit ,m_min, m_max, m_bin);    
    if( m_split ){
      for(int iarea = 0 ; iarea<nArea;iarea++){
        int icalo = CaloCellCode::caloNum( m_detectorName );
        std::string pref = CaloCellCode::caloArea(icalo ,iarea);
        std::string title  = tit + " region = " + pref;
        book1D( pref + "/1" , title ,m_min, m_max, m_bin);    
      }
    }
  }
  
  if(m_bary){
    book2D(  "barycenter/1"
             , m_detectorName + " overall (x,y) barycenter "
             , m_bmin , m_bmax,  m_bbin, m_bmin , m_bmax, m_bbin );
    book2D(  "barycenter/2"
             , m_detectorName + " (left/right, top/bottom) asymmetry"
             , -1. , +1. ,  m_bbin, -1. , +1. , m_bbin );
  }
  

  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloSpectrum::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  setFilterPassed(true);

  for(std::vector<std::string>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
    std::string slot = *is;
    if(!m_daq[slot]->ok()){
      debug()<< "The Readout tool cannot load the rawEvent for slot " + slot << endmsg;
      m_ok[slot]=false;
    }else
      m_ok[slot]=true;
  }
  
  if( !m_ok[m_ref] )return StatusCode::SUCCESS;


  double left  = 0;
  double right = 0;
  double up  = 0;
  double down = 0;
  double xx =0;
  double yy =0;
  
  const CaloVector<CellParam>& cells =  m_calo->cellParams();
  const CaloVector<LHCb::CaloAdc>& adcs = m_daq[m_ref]->adcs();
  for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
    LHCb::CaloCellID id = (*iadc).cellID();
    if( id.isPin())continue;

    double a = (*iadc).adc();
    double x = cells[id].x();    
    double y = cells[id].y();
    if(x<0) left  += a;
    if(x>=0) right += a;
    if(y>=0) up  += a;
    if(y<0) down += a;
    xx += x*a;
    yy += y*a;
    

    int adc = 0;
    unsigned int icl = 1;
    if(m_2x2)icl  = 2;
    for(unsigned int kr = 0 ; kr < icl ; kr++){
      for(unsigned int kc = 0 ; kc < icl ; kc++){
        LHCb::CaloCellID temp = id;
        if (m_2x2){
          temp = LHCb::CaloCellID(id.calo(), id.area() , id.row() + kr ,  id.col()+ kc);
          if( !m_calo->valid(temp) )continue;
        }
        adc += (*iadc).adc(); // Ref adc/
        for(std::vector<std::string>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
          std::string slot = *is;
          if( slot == m_ref)continue;
          if(!m_ok[slot])continue;
          adc += m_daq[slot]->adc( temp );
        }
      }
    }
    
    
    if(adc < m_threshold)continue;
    if(m_1D){
      plot1D( (double) adc , "1"  , "" ,m_min, m_max, m_bin);
      if( m_split ){
        std::string sArea =  id.areaName() ;
        std::string unit =  sArea + "/1";
        plot1D( (double) adc , unit , "" ,m_min, m_max, m_bin);
      }      
    }    
  } 
  //
  if(m_statusOnTES){
    for(std::vector<std::string>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
      m_daq[*is]->putStatusOnTES();
    }
  }


  if(m_bary && (right+left)>0 ){
    // Centrality and barycentric parameters
    double ax = (right-left)/(right+left);
    double ay = (up-down)/(up+down);
    double bx = xx/(up+down);
    double by = yy/(up+down);
    plot2D( bx , by , "barycenter/1" , "" 
    , m_bmin , m_bmax, m_bmin , m_bmax, m_bbin, m_bbin );
    plot2D( ax , ay , "barycenter/2" , ""
    , -1. , +1. , -1. , +1. , m_bbin, m_bbin );
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloSpectrum::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
