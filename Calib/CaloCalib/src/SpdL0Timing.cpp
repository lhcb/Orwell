// $Id: SpdL0Timing.cpp,v 1.6 2009-11-09 13:58:05 odescham Exp $
// Include files 

// local
#include "SpdL0Timing.h"

//-----------------------------------------------------------------------------
// Implementation file for class : SpdL0Timing
//
// 2008-07-09 : Hugo Ruiz
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SpdL0Timing )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SpdL0Timing::SpdL0Timing( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : Calo2Dview ( name , pSvcLocator )
{
  declareProperty( "ReadoutTool"   , m_readoutTool  = "CaloDataProvider" );

}

//=============================================================================
// Destructor
//=============================================================================
SpdL0Timing::~SpdL0Timing() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode SpdL0Timing::initialize() {
  StatusCode sc = Calo2Dview::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiHistoAlg
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  m_fromRaw     = tool<IL0DUFromRawTool> ( "L0DUFromRawTool" , "L0DUFromRawTool",this);
  m_fromRaw->fillDataMap();
  m_daq         = tool<ICaloDataProvider>( m_readoutTool ,     "SpdReadoutTool" ,    this );

  book1D( "1"  ,  "SpdMult(L0DU)-#SpdHits(Calo)"   ,  -50. , 50. ,  100);
  bookCalo2D( "2" , "Spd hits when SpdMult(L0DU) != #SpdHits(Calo)", "Spd" );
  bookCalo2D( "3" ,  "L0DUMultiplicity==0 && DAQMultiplicity==1" , "Spd"   );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode SpdL0Timing::execute() {
  
  // Get SPD hits
  if( !m_daq->ok()    )return StatusCode::SUCCESS;
  const CaloVector<LHCb::CaloAdc>& adcs    = m_daq->adcs();
  int multDAQ    = adcs.size();
  debug() << "DAQ :    "     << multDAQ << endmsg ;
  
  // Get the L0DU Spd multiplicity    
  m_fromRaw->decodeBank();
  int multL0    = m_fromRaw->data("Spd(Mult)");
  debug() << "L0   : " << multL0 << endmsg;


  fill( histo1D(HistoID("1")), multL0-multDAQ , 1  );
  
  if(multL0 != multDAQ){
    for(CaloVector<LHCb::CaloAdc>::const_iterator iadc0 = adcs.begin() ; adcs.end() != iadc0 ; ++iadc0 ){
      fillCalo2D( "2" , *iadc0 );
      // Clean discrepancy found
      if ( multL0 == 0 && multDAQ == 1 )fillCalo2D( "3" , *iadc0 );
    }    
  };
  
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode SpdL0Timing::finalize() {
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  return Calo2Dview::finalize();  // must be called after all other actions
}

//=============================================================================
