// $Id: CaloHisto.h,v 1.11 2010-01-29 11:07:57 odescham Exp $
#ifndef CALOHISTO_H
#define CALOHISTO_H 1

// Include files (CaloCellID/CaloCellIDAsProperty should be first)
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloCellIDAsProperty.h"
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
//
#include "CaloDet/DeCalorimeter.h"

static const InterfaceID IID_CaloHisto ( "CaloHisto", 1, 0 );

/** @class CaloHisto CaloHisto.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-02-28
 */
class CaloHisto : public GaudiTool {
public:

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_CaloHisto; }

  /// Standard constructor
  CaloHisto( const std::string& type,
             const std::string& name,
             const IInterface* parent);

  virtual ~CaloHisto( ); ///< Destructor
  StatusCode initialize( ) override;
  bool histo(){return m_histo;}
  double rate(){return m_rate;}


  void setDetector(std::string det){m_det = det;}
  void setHisto(bool ok){    m_histo = ok; }
  bool splitLeds(){return m_split;}
  std::string hName(){return m_hName;}   // histo name
  std::string pName(){return m_pName;}  // profile name
  //Default range & bin & window
  double min(){return (m_ranges[LHCb::CaloCellID()].size() == 3)? *(m_ranges[LHCb::CaloCellID()].begin()) : 0.;}
  double max(){return (m_ranges[LHCb::CaloCellID()].size() == 3)? *(m_ranges[LHCb::CaloCellID()].begin()+1): 0 ;}
  int bins(){
    int nbin = m_defBin;
    if(  m_ranges[LHCb::CaloCellID()].size() == 3 )nbin = (*(m_ranges[LHCb::CaloCellID()].begin()+2));
    return (nbin>0) ? nbin : m_defBin;
  }

  // per cell
  double min(LHCb::CaloCellID id){
    if ( m_ranges[id].size() == 3 )return *(m_ranges[id].begin() );
    else if( id.isPin() && m_ranges[LHCb::CaloCellID(16383)].size() == 3 )return *(m_ranges[LHCb::CaloCellID(16383)].begin() );
    return min();
  }
  double max(LHCb::CaloCellID id){
    if ( m_ranges[id].size() == 3 )return *(m_ranges[id].begin() +1 );
    else if( id.isPin() && m_ranges[LHCb::CaloCellID(16383)].size() == 3 )return *(m_ranges[LHCb::CaloCellID(16383)].begin() +1 );

    return max();
  }
  int bins(LHCb::CaloCellID id)  {
    if ( m_ranges[id].size() == 3 )return (int) *(m_ranges[id].begin() +2 );
    else if( id.isPin() && m_ranges[LHCb::CaloCellID(16383)].size() == 3 )
      return (int) *(m_ranges[LHCb::CaloCellID(16383)].begin() +2);
    return bins();
  }


  double scale(){return m_scale;}
  // Summary histo
  int    order(){return m_order;}
  bool profile(){return m_profile;}
  bool isInWindow(double value , LHCb::CaloCellID id=LHCb::CaloCellID()){
    const std::vector<double>& windows = m_windows[id];
    if( !windows.empty() )return inWindow(value,id);
    else if( id.isPin() && m_windows[LHCb::CaloCellID(16383)].size() != 0)return inWindow(value,LHCb::CaloCellID(16383));
    return inWindow(value,LHCb::CaloCellID());
  }


private:
  bool inWindow(double value, LHCb::CaloCellID id){
    const std::vector<double>& windows  = m_windows[id];
    if( windows.size()%2 != 0)return false;
    std::vector<double>::const_iterator it = windows.begin();
    for(unsigned int i = 0; i < windows.size()/2;++i){
      int ind = i*2;
      if( value >= *(it+ind) && value <= *(it+ind+1) )return m_invert ? false : true;
    }
    return m_invert ? true : false;
  }
  bool m_histo;
  std::string m_name;
  std::string m_hName;
  std::string m_pName;
  bool m_split;
  double m_scale;
  int m_order;
  std::map<LHCb::CaloCellID  , std::vector<double>  > m_ranges;
  std::map<LHCb::CaloCellID , std::vector<double>  > m_windows;
  bool m_profile;
  double m_count;
  double m_rate;
  bool m_invert;
  std::string m_det;
  DeCalorimeter* m_calo;
  bool m_useDBR;
  bool m_useDBW;
  int m_defBin;
  std::string m_typ;
  int m_nsig;
};
#endif // CALOHISTO_H
