
// $Id: CaloCalib.h,v 1.19 2010-10-11 10:54:03 odescham Exp $
#ifndef CALOCALIB_H
#define CALOCALIB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IEventTimeDecoder.h"
// from LHCb
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloChannelHisto.h"
#include "CaloSummaryHisto.h"
#include "CaloHisto.h"
#include <TH1.h>
#include <TAxis.h>

/** @class CaloCalib CaloCalib.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-02-08
 */
class CaloCalib : public GaudiHistoAlg {

public:
  /// Standard constructor
  CaloCalib( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloCalib( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization
  StatusCode fullDecoding();
  StatusCode bookHistos(std::string slot);
  void fillHisto(const LHCb::CaloCellID& id  , int led, double val  , double ref , bool fired , bool ratio);
  typedef std::vector<LHCb::CaloCellID> CellIds;
  inline int whichLedIsFired(const std::vector<int>& leds);
  inline int ledIndex(const LHCb::CaloCellID& id , int led);
  inline bool isInBankList(const LHCb::CaloCellID& id);
  const std::string getChannelUnit(const std::string& type , const std::string& slot, int led = -1,
                             const  LHCb::CaloCellID& id = LHCb::CaloCellID(), bool split=false);
  const std::string getSummaryUnit(const std::string& type , const std::string& slot
                                   , int led = -1, int ord=0,bool split=false);
  bool firedNeighbor( const LHCb::CaloCellID& id, bool skipFEB=false);
protected:
  //

private:
  typedef std::map<std::string,CaloHisto*> toolMap;
  void fillSummaryHisto(const LHCb::CaloCellID& id,
                        double val, int led ,CaloSummaryHisto::Type type,toolMap& tMap ,
                        const std::string& slot, double norm = 0.);
  void bookSummaryHisto(CaloSummaryHisto::Type type, toolMap& tMap, const std::string& slot);
  void bookChannelHisto(CaloChannelHisto* cHisto ,CaloChannelHisto::Type type, toolMap& tMap , const std::string& slot);
  void fillChannelHisto( double val,int index ,
                         CaloChannelHisto* cHisto ,CaloChannelHisto::Type type,toolMap& tMap , const std::string& slot
                         , double norm = 0.);
  StatusCode setStatus(StatusCode sc=StatusCode::SUCCESS);
  int m_pCount;
  int m_lCount;
  std::map<int,int> m_cCount;
  std::map<int,int> m_sCount;
  int m_skip;
  // CaloHisto Tools
  toolMap m_Sig; // adjacent slots (signal histos)
  toolMap m_All; // adjacent slots (full histos)
  toolMap m_Ped; // adjacent slots (pedestal histos)
  toolMap m_Rat; // adjacent slots (PMT/PIN ratio histos)
  toolMap  m_SumPed; // adjacent slots (pedestal summaries)
  toolMap m_SumSig; // adjacent slots (signal summaries)
  toolMap m_SumRat; // adjacent slots (PMT/PIN ratio summary)
  toolMap m_SumAll; // adjacent slots (full range summary)

  // Service
  IRndmGenSvc* m_rndSvc ;
  Rndm::Numbers* m_rnd;

  // set-up
  DeCalorimeter* m_calo;
  std::string m_detectorName;
  std::string m_readoutTool;
  std::vector<LHCb::CaloCellID> m_pinChannels;

  // Properties
  bool m_pinDriven;
  bool m_useEmpty;
  double m_threshold;
  int m_cellCheck;
  std::vector<int> m_banks;
  double m_pedRate;
  double m_taeRate;
  double m_detRate;
  double m_checkRate;
  double m_shoot;
  int m_led;
  bool m_xt;
  int m_sbSel;
  // statistics
  unsigned int m_nCells;
  unsigned int m_min;
  unsigned int m_max;
  unsigned int m_minFire;
  unsigned int m_maxFire;
  double m_pShift;
  double m_ppShift;

  // downscaling
  bool m_ped;
  bool m_tae;
  bool m_det;
  bool m_check;
  //
  std::map<std::string,bool> m_ok;

  // Histogram holders
  std::map<std::string,AIDA::IHistogram1D*> m_countHistos;
  CaloVector<CaloChannelHisto*> m_cHistos;
  CaloSummaryHisto* m_sHisto ;

  // Data providers
  ICaloDataProvider*  m_daq;
  std::map<std::string,ICaloDataProvider*>  m_daqs;
  std::string m_ref;
  std::vector<std::string> m_slots;
  bool m_statusOnTES;
  bool m_loc;
  std::string m_prof;
  std::map<std::string,bool> m_booked;
  std::map<std::string,bool> m_histo;
  std::vector<int> m_febs;
  IEventTimeDecoder* m_odin;
  AIDA::IHistogram1D* m_hTimeInit;
  AIDA::IHistogram1D* m_hTimeEnd;
  longlong m_timeInit;
  longlong m_timeEnd;
};
#endif // CALOCALIB_H




inline bool CaloCalib::isInBankList(const LHCb::CaloCellID& id){

  if(m_banks.size() == 0 )return true;
  if(m_banks.size() ==1 && *(m_banks.begin()) < 0)return true;
  int feb   = m_calo->cellParam( id ).cardNumber();
  int tell1 = m_calo-> cardToTell1( feb );
  bool ok = false;
  for(std::vector<int>::iterator ib =  m_banks.begin() ; ib != m_banks.end() ; ++ ib){
    if( *ib == tell1 || *ib == -1 ){ok = true;break;}
  }
  return ok;
}

//-------------------------------------------------------
inline int CaloCalib::whichLedIsFired(const std::vector<int>& leds ){

  //if( 1 == leds.size() )return *(leds.begin());

  // majority voting
  int maxLed = -1;
  int maxSum = 0;
  int nFlash = 0;
  if ( msgLevel( MSG::DEBUG) ) debug() << " whichLed ? : #led " << leds.size() << " -> " << leds << endmsg;
  for(std::vector<int>::const_iterator led = leds.begin();led!=leds.end();++led){
    //if ( msgLevel( MSG::DEBUG) ) debug() << " whichLed ? : led " << *led << endmsg;
    int sum = 0;
    CaloLed cLed = m_calo->caloLed(*led);
    const CellIds& cells = cLed.cells();
    /*
   // check if another connected LED is firing (Hcal configuration)
    bool anotherLed = false;
    const LHCb::CaloCellID& firstCell = *(cells.begin());
    const CellIds& pins = m_calo->cellParams()[firstCell].pins();
    if( pins.size() > 1 ){
      for( CellIds::const_iterator id = pins.begin() ; id != pins.end() ; ++id){
        if( *id == pinId )continue;
        if( m_daq->adc(*id) >= m_threshold )anotherLed = true;
      }
    }
    if( anotherLed )return -1;
    */
    for(CellIds::const_iterator cell = cells.begin();cell!=cells.end();++cell){
      if( cell-cells.begin() >= m_cellCheck)break;
      int adc = m_daq->adc(*cell);
      if ( msgLevel( MSG::DEBUG) ) debug() << " Led " << *led << " adc : " << adc << " id = " <<  *cell <<endmsg;
      if(adc>m_threshold)sum++;
    }
    if( sum > maxSum ){
      maxLed = *led;
      maxSum= sum;
    }
    if( sum == maxSum && maxSum > 0)nFlash++;
    if ( msgLevel( MSG::DEBUG) ) debug() << " Led " << *led
                                         << "maxSum / sum / maxLed " << maxSum << " / " << sum << " / " << maxLed <<endmsg;
  }
  if( nFlash > 1){
    int led = *(leds.begin());
    CaloLed cLed = m_calo->caloLed( led );
    std::stringstream sid("");
    sid << nFlash << " LEDs connected to PIN-diode " << cLed.pin() << " are flashed simultaneously";
    Warning(sid.str()).ignore();
    counter("Unexpected simultaneous LED flashes") += 1;
  }

  return maxLed;
}
//-------------------------------------------------------
inline int CaloCalib::ledIndex(const LHCb::CaloCellID& id , int led){


  // Find the led index in the vector of leds connected to cell id
  int index = -1 ;
  // no PIN for Spd/Prs -> No led defined in DeCalorimeter
  std::vector<int> leds = m_calo->cellParam(id).leds();
  if(m_detectorName == "Spd" || m_detectorName == "Prs"){
    leds.push_back(0);
  }

  for(std::vector<int>::iterator iLed = leds.begin(); iLed!=leds.end();++iLed){
    if( led == *iLed){
      index = iLed - leds.begin();
      break;
    }
  }
  if(index < 0){
    error() << "the LED " << led << " is NOT found to be connected to cell : " << id << endmsg;
  }
  return index;
}
