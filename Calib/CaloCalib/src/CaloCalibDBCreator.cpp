// $Id: $
// Include files 

// local
#include "CaloCalibDBCreator.h"
#include <string>
#include "CaloDet/DeCalorimeter.h"
#include "CaloUtils/CaloAlgUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloCalibDBCreator
//
// 2011-08-16 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloCalibDBCreator )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloCalibDBCreator::CaloCalibDBCreator( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  
  declareProperty("Coefficients",m_coeffs); 
  declareProperty("Zone",m_zones); 
  declareProperty("Calo",m_det="Ecal");
  declareProperty("TopCalibration",m_cali);
  declareProperty("PostCalibration",m_post);
}
//=============================================================================
// Destructor
//=============================================================================
CaloCalibDBCreator::~CaloCalibDBCreator() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloCalibDBCreator::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;


  DeCalorimeter* calo = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( m_det ) );
  const CaloVector<CellParam>& cells = calo->cellParams();
  int t=0;
  int p=0;
  for(std::map<std::string,std::vector<double> >::iterator ic = m_coeffs.begin(); m_coeffs.end() != ic ; ++ic ){

    std::string fill=ic->first;
    std::vector<double> coeffs = ic->second;
    if( coeffs.size() != m_zones.size() ){
      Warning("Zone/Coeff unbalanced").ignore();
      continue;
    }
    CaloVector<double> table;
    t = 0;
    p = 0;
    for(CaloVector<CellParam>::const_iterator icel = cells.begin(); icel != cells.end() ; ++icel){
      LHCb::CaloCellID id = icel->cellID(); 
      if(!calo->valid(id))continue; 
      if( id.isPin() ) continue;

      int k = -1;
      int ok = 0;
      for(std::map<std::string,std::vector<int> >::iterator izon = m_zones.begin() ; m_zones.end() != izon; ++izon){
        std::string name=izon->first;
        std::vector<int> zone=izon->second;
        k++;    
        double coeff=coeffs[k];        
        
        //info() << " -- Fill " << fill << " zone " << name << "coeff : " << coeff << endmsg;
        if( zone.size()%5 != 0){
          Warning("Bad zone definition").ignore();
          continue;
        }
        //bool inZone = false;
        for ( unsigned int kk = 0; zone.size()/5 > kk  ; ++kk ) {
          int ll = 5*kk;
          unsigned int area = zone[ll];
          unsigned int fr    = zone[ll+1];
          unsigned int lr    = zone[ll+2];
          unsigned int fc    = zone[ll+3];
          unsigned int lc    = zone[ll+4];
          if( id.area() == area && id.col() >= fc && id.col() <= lc && id.row() >= fr && id.row() <= lr ){
            //inZone=true;
            ok++;

            
            if( m_cali.size() > 0 ){
              double top = m_cali[id];
              if( top > 0) {
                coeff*=top;
                t++;
              }
            }
            if( m_post.size() > 0 ){
              double post = m_post[id];
              if( post > 0) {
                coeff*=post;
                p++;
              }
            }

            table.addEntry( coeff , id);
            break;
          }
        }// loop over sub-zones
      } // loop over zones
      if( ok != 1)warning() << "Cell " << id << " has " << ok << " associated zones " << endmsg;
    }  // loop over cells
    counter("Applying top calibration")+=t;
    counter("Applying post calibration")+=p;
    info() << "Fill " << fill << " : coefficient table created for " << table.size() << " channels " << endmsg;


    // create Calibration.xml 

    // header
    std::vector<std::string> head;
    std::vector<std::string> foot;
    std::string q=std::string("\"");
    head.push_back("<?xml version="+q+"1.0"+q+" encoding="+q+"ISO-8859-1"+q+"?>");    
    head.push_back("<!--- $Id: -->");
    head.push_back("<!--- Author  : D. Savrina & O. Deschamps -->");
    head.push_back("<!--- Created : 2011-08-16 -->");
    head.push_back("<!DOCTYPE DDDB SYSTEM "+q+ "conddb:/DTD/structure.dtd"+q+">");
    head.push_back("<DDDB>");
    head.push_back("<condition name = "+q+"Calibration"+q+">");
    head.push_back("<param name = "+q+"size"+q+"       type = "+q+"int"+q+">   2 </param>");
    head.push_back("<paramVector name = "+q+"data"+q+" type = "+q+"double"+q+">");
    head.push_back("<!-- Calibration 2011 : pi0 Kali calibration per zone for fill "+fill+" -->");
    head.push_back("<!---   channel   dG  -->");
    foot.push_back("</paramVector>");
    foot.push_back("</condition>");
    foot.push_back("</DDDB>");
    


    FILE* out = fopen(std::string("Calibration.xml_"+fill).c_str(), "w");
    for(std::vector<std::string>::iterator h=head.begin();head.end()!=h;h++){
      fprintf(out,"%-100s\n",h->c_str());
    }
    for(CaloVector<CellParam>::const_iterator icel = cells.begin(); icel != cells.end() ; ++icel){
      LHCb::CaloCellID id = icel->cellID(); 
      if(!calo->valid(id))continue; 
      if( id.isPin() ) continue; 
      fprintf( out , "%10.0f %15.8f \n",
               double(id.index()),table[id]);
    }
    for(std::vector<std::string>::iterator f=foot.begin();foot.end()!=f;f++){
      fprintf(out,"%-100s\n",f->c_str());
    }
    fclose( out );






  }  // loop over fills  
  
  
  return StatusCode::SUCCESS; 
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloCalibDBCreator::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloCalibDBCreator::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
