// $Id: CaloChannelCounter.h,v 1.7 2010-10-11 10:54:03 odescham Exp $
#ifndef CALOCHANNELCOUNTER_H
#define CALOCHANNELCOUNTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
// from LHCb
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"


/** @class CaloChannelCounter CaloChannelCounter.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-03-26
 */
class CaloChannelCounter : public GaudiHistoAlg {
public:
  /// Standard constructor
  CaloChannelCounter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloChannelCounter( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  DeCalorimeter* m_calo;
  std::map<std::string,ICaloDataProvider*>  m_daq;
  std::map<std::string,int> m_slots;
  unsigned int m_nCells;
  //
  long m_threshold;
  std::string m_detectorName;
  std::string m_readoutTool;
  int m_bin;
  int m_min,m_max;
  bool m_1d, m_2d,m_prof;
  int m_nChan;
  bool m_statusOnTES;
  int m_minMultFilter;
  int m_maxMultFilter;
  std::string m_filterSlot;
  double m_sumMax;
  double m_sumBin;
};
#endif // CALOCHANNELCOUNTER_H
