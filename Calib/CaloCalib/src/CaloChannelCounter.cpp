// $Id: CaloChannelCounter.cpp,v 1.17 2010-10-11 10:54:03 odescham Exp $
// Include files 


// local
#include "CaloChannelCounter.h"
#include "CaloUtils/CaloAlgUtils.h"
//-----------------------------------------------------------------------------
// Implementation file for class : CaloChannelCounter
//
// 2008-03-26 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloChannelCounter )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloChannelCounter::CaloChannelCounter( const std::string& name,
                                        ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
{
  declareProperty( "Threshold"     , m_threshold = -257);
  declareProperty( "Detector"      ,  m_detectorName); 
  declareProperty( "ReadoutTool"   ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "TimeSlots"     ,  m_slots);
  declareProperty( "NBins"         ,  m_bin=0);
  declareProperty( "Histo1D"       ,  m_1d=false);
  declareProperty( "Histo2D"       ,  m_2d=false);
  declareProperty( "Profile"       ,  m_prof=true);
  declareProperty( "MaxChannel"    ,  m_nChan=-1);
  declareProperty( "StatusOnTES"   ,  m_statusOnTES = true);
  declareProperty( "MinMultiplicityFilter", m_minMultFilter =-1 );
  declareProperty( "MaxMultiplicityFilter", m_maxMultFilter =-1 );
  declareProperty( "FilterSlot", m_filterSlot ="T0" );
  declareProperty( "SumMax", m_sumMax = 20000 );
  declareProperty( "SumBin", m_sumBin = 100 );
  
  //
  m_slots["Prev7"] = -7;
  m_slots["Prev6"] = -6;
  m_slots["Prev5"] = -5;
  m_slots["Prev4"] = -4;
  m_slots["Prev3"] = -3;
  m_slots["Prev2"] = -2;
  m_slots["Prev1"] = -1;
  m_slots["T0"]    = 0;
  m_slots["Next1"] = +1;
  m_slots["Next2"] = +2;
  m_slots["Next3"] = +3;
  m_slots["Next4"] = +4;
  m_slots["Next5"] = +5;
  m_slots["Next6"] = +6;
  m_slots["Next7"] = +7;


  // set default detectorName
  m_detectorName = LHCb::CaloAlgUtils::CaloNameFromAlg( name );
  if ( "Prs" == m_detectorName ) m_sumMax = 50000;
  if ( "Spd" == m_detectorName ) m_sumMax = 600;
  setHistoDir( name );

}
//=============================================================================
// Destructor
//=============================================================================
CaloChannelCounter::~CaloChannelCounter() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloChannelCounter::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiHistoAlg

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;


  //
  if( !m_1d && !m_2d && !m_prof){
    Error("No histo will be produced - please configure properly or remove").ignore();
    return StatusCode::FAILURE;
  }

  // Get Detector Element
  if ( "Ecal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  } else if ( "Prs" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs ); // Spd readout via Prs FEB
  } else {
    error() << "Unknown detector name " << m_detectorName << endmsg;
    return StatusCode::FAILURE;
  }

  // get tools
  m_min = (*(m_slots.begin())).second;
  m_max = m_min;
  for(std::map<std::string,int>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
    std::string slot = (*is).first;
    if(slot != "T0"){
      this->setProperty("RootInTes", slot).ignore();
    }else{
      this->setProperty("RootInTes", "").ignore();
    }
    m_daq[slot] = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" + slot , this );
    if( (*is).second > m_max )m_max = (*is).second;
    if( (*is).second < m_min )m_min = (*is).second;
  }
  m_max += 1;

  // init
  if(m_nChan > 0){
    m_nCells = m_nChan+1;
  }else{
    m_nCells  = m_calo->numberOfCells()+1;
  }
  if(m_bin <= 0) m_bin = m_nCells;

  // reset
  this->setProperty("RootInTes", "").ignore();


  return StatusCode::SUCCESS;

}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloChannelCounter::execute() {


  setFilterPassed( true );  


  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
    std::string st = Gaudi::Utils::toString( m_threshold );
    std::string s = "# of " + m_detectorName + " channels above threshold (" + st +  " ADC) ";
    std::string ss = "ADC sum for " + m_detectorName + " channels above threshold (" + st + " ADC) ";

  std::map<std::string,int> count,sum;
  for(std::map<std::string,int>::iterator is = m_slots.begin(); is != m_slots.end(); is++){
    std::string slot = (*is).first;    
    count[slot] = 0;
    sum[slot]=0;
    if(!m_daq[slot]->ok())continue;
    const CaloVector<LHCb::CaloAdc>& adcs = m_daq[slot]->adcs();
    if(m_statusOnTES)m_daq[slot]->putStatusOnTES();
    for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
      LHCb::CaloCellID id = (*iadc).cellID();
      if( id.isPin() )continue;
      if( (*iadc).adc() > m_threshold ) {
        count[slot]++;
        sum[slot] += (*iadc).adc();
      } 
    }

    if( count[slot] > (int)m_nCells )count[slot]=(int)m_nCells-1;

    std::string id = Gaudi::Utils::toString( m_slots[slot]-m_min+1);
    if(m_1d){
      plot1D( (double) count[slot], "Counter1D/" + id, s + " for slot = " + slot, 0., (double) m_nCells, m_bin);
      plot1D( (double) sum[slot], "Sum1D/" + id, ss + " for slot = " + slot, 0., m_sumMax, m_sumBin);
    }
    
    if(m_2d){
      plot2D( (double) m_slots[slot], (double) count[slot] , "Counter2D/1", 
              s + " versus slot", 
              (double) m_min-0.5, (double) m_max-0.5, 0., (double)m_nCells, m_max-m_min , m_bin  );
      plot2D( (double) m_slots[slot], (double) sum[slot] , "Sum2D/1", 
              ss + " versus slot", 
              (double) m_min-0.5, (double) m_max-0.5, 0., m_sumMax, m_max-m_min , m_sumBin  );
    }
    if(m_prof){
      profile1D( (double) m_slots[slot] , (double) count[slot] , "Profile/1", s + " versus slot (profile)",
                 (double) m_min-0.5, (double) m_max-0.5, m_max-m_min);
      profile1D( (double) m_slots[slot] , (double) sum[slot] , "Profile/2", ss + " versus slot (profile)",
                 (double) m_min-0.5, (double) m_max-0.5, m_max-m_min);      
    }    
  }


  // Filtering

  if( m_minMultFilter >= 0 && count[m_filterSlot] < m_minMultFilter )setFilterPassed(false);
  if( m_maxMultFilter >= 0 && count[m_filterSlot] > m_maxMultFilter )setFilterPassed(false);
    
  debug() << " Filter " << m_minMultFilter << " <= " << count[m_filterSlot] << " => " << m_maxMultFilter << " ?  "
          << filterPassed()<<endmsg;


  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloChannelCounter::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
