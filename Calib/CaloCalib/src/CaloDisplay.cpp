// $Id: CaloDisplay.cpp,v 1.14 2009-11-08 18:46:20 odescham Exp $
// Include files 

// from LHCb
#include "Event/ODIN.h" 
// local
#include "CaloDisplay.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloDisplay
//
// 2008-01-18 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloDisplay )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloDisplay::CaloDisplay( const std::string& name,
                          ISvcLocator* pSvcLocator)
  : Calo2Dview ( name , pSvcLocator )
    ,m_sequenceCounter(0)
    ,m_refreshCounter(0)
    ,m_scaleCounter(0.)
    ,m_tit()
{
  declareProperty( "Views"         , m_sequence=1);
  declareProperty( "Refresh"       , m_refresh=1);
  declareProperty( "PreScale"      , m_scale=100.);// %
  declareProperty( "ReadoutTool"   , m_readoutTool  = "CaloDataProvider" );
  declareProperty( "L0ReadoutTool" , m_l0readoutTool  = "CaloL0DataProvider" );
  declareProperty( "Detector"      , m_detectorName );
  declareProperty( "L0Threshold"   , m_l0Threshold = 0.0);
  declareProperty( "L0Offset"      , m_l0Offset    = 0.0);
  declareProperty( "StatusOnTES"   ,  m_statusOnTES = true);
  declareProperty( "Filter"        ,  m_filter = -1);
  declareProperty( "DynamicTitle"  ,  m_dynTitle = false);

  // set default detectorName
  int index = name.find_last_of(".") +1 ; 
  m_detectorName = name.substr( index , 4 );
  if ( name.substr(index,3) == "Prs" ) m_detectorName = "Prs";  
  if ( name.substr(index,3) == "Spd" ) m_detectorName = "Spd"; 

  if( m_detectorName == "Ecal" || m_detectorName == "Hcal")setPinView(true);
  setHistoDir( name );
}
//=============================================================================
// Destructor
//=============================================================================
CaloDisplay::~CaloDisplay() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloDisplay::initialize() {
  debug() << "==> Initialize" << endmsg;
  StatusCode sc = Calo2Dview::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  m_odin = tool<IEventTimeDecoder>("OdinTimeDecoder","OdinDecoder",this);
  m_daq = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" , this );
  m_l0daq = tool<ICaloL0DataProvider>( m_l0readoutTool , m_detectorName + "L0ReadoutTool" , this );

  // pre-book 2D views
  std::string slot = (rootInTES() == "") ?  " (T0) " : " (" +rootInTES() + ")" ;
  if( m_sequence == 1){
    bookCalo2D( "DAQ/1" , m_detectorName +" 2D view " + slot , m_detectorName);
    if(m_l0)bookCalo2D( "L0DAQ/1", m_detectorName +" 2D L0DAQ view " + slot ,  m_detectorName);
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloDisplay::execute() {
  if ( msgLevel(MSG::DEBUG) )debug() << "==> Execute" << endmsg;

  setFilterPassed(true);

  //get the ADC bank
  if( !m_daq->ok() )return StatusCode::SUCCESS;
  const CaloVector<LHCb::CaloAdc>& adcs = m_daq->adcs();

  if ( msgLevel(MSG::DEBUG) )debug() << " Container size : " << adcs.size() << endmsg;

  // put status On TES
  if(m_statusOnTES){
    m_daq->putStatusOnTES();
    if(m_l0)m_l0daq->putStatusOnTES();
  }


  if(adcs.size() == 0){
    if(m_filter >= 0)setFilterPassed(false);
    return StatusCode::SUCCESS;
  }
  // filtering
  if(m_filter >= 0){
    setFilterPassed(false);
    for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
      if( (*iadc).adc()+m_offset >= m_filter){
        setFilterPassed(true);  
        break;
      }      
    }
    if(!filterPassed())return StatusCode::SUCCESS;
  }

  // get ODIN
  if ( msgLevel(MSG::DEBUG) )debug() << " Get ODIN object for reference slot " <<endmsg;
  m_odin->getTime();
  long event=0;
  long run=0 ;
  long bunch=0;
  if( exist<LHCb::ODIN>(LHCb::ODINLocation::Default) ){
    LHCb::ODIN* odin = get<LHCb::ODIN> (LHCb::ODINLocation::Default);
    if ( msgLevel(MSG::DEBUG) )debug() << " RUN" << odin->runNumber() <<" EVENT " << odin->eventNumber() << endmsg;
    run   = odin->runNumber();
    event =odin->eventNumber() ;
    bunch =odin->bunchId() ;
  }

  
  

  // Prescaling
  m_scaleCounter += m_scale;
  if(m_scale <  100.){
    return StatusCode::SUCCESS;
  }else{
    m_scaleCounter = 0;
  }
  
     
   // Counters & refresh
  if(m_refresh == m_refreshCounter){
    m_refreshCounter =0 ;
    m_sequenceCounter++;
  }
  if(m_sequenceCounter == m_sequence){
    m_sequenceCounter = 0;
    for(int i = 1 ; i <= m_sequence ; i++){
      std::string lun = Gaudi::Utils::toString( i );
      reset( "DAQ/" + lun, "" );
      if(m_l0)reset( "L0DAQ/" + lun, "" ); 
    } 
  }


  // Titl'ing
  std::string xrun = Gaudi::Utils::toString( run );
  std::string xevt = Gaudi::Utils::toString( event );
  std::string xbunch = Gaudi::Utils::toString( bunch );
  if(m_refreshCounter == 0){
    m_tit = m_detectorName + " 2D view " + Gaudi::Utils::toString(m_sequenceCounter+1 );
    m_tit += (rootInTES() == "") ? " (T0) " :  " (" + rootInTES() + ")" ;
    m_tit += (m_refresh == -1) ? " with all event" :  " with " + Gaudi::Utils::toString( m_refresh) + " event";
    if(m_refresh != 1)m_tit += "s" ;
    if(m_refresh != 1 && m_dynTitle)m_tit += " from " ;
    if(m_dynTitle)m_tit += "  (" + xrun + "," + xevt + "," + xbunch + ")";
  }
  std::string title = m_tit;  
  if(m_refresh !=1 && m_dynTitle) title +=  " to  (" + xrun + "," + xevt + "," + xbunch + ")";

  
  m_refreshCounter++;
  
  // Fill view
  std::string unit = Gaudi::Utils::toString( m_sequenceCounter+1);
  const HistoID lun = HistoID( "DAQ/" + unit);
  for(CaloVector<LHCb::CaloAdc>::const_iterator iadc = adcs.begin();iadc!= adcs.end();++iadc){
    if ( msgLevel(MSG::VERBOSE) )verbose() << "Fill 2D view with ADC = " << (*iadc).adc() 
                                           << "  for cell = " << (*iadc).cellID() << endmsg;
    fillCalo2D( lun , (*iadc) , title );
  }
  // resetTitle
  if(m_dynTitle)resetTitle( lun , title);


  // L0-view
  if( !m_l0 ) return StatusCode::SUCCESS;

  // special setting for L0DAQ
  double threshold = m_threshold;
  double offset    = m_offset;

  setThreshold( m_l0Threshold);
  setOffset(m_l0Offset);

  //get the L0-ADC bank
  if( !m_l0daq->ok() )return StatusCode::SUCCESS;
  const CaloVector<LHCb::L0CaloAdc>& l0adcs = m_l0daq->l0Adcs();
  if(l0adcs.size() == 0)return StatusCode::SUCCESS;
  const HistoID l0lun = HistoID( "L0DAQ/" + unit);
  for(CaloVector<LHCb::L0CaloAdc>::const_iterator iadc = l0adcs.begin();iadc!= l0adcs.end();++iadc){
    fillCalo2D( l0lun , (*iadc) , title + " (L0DAQ)" );
  }
  if(m_dynTitle)resetTitle( l0lun , title + " (L0DAQ)");

  // reset for 12bit DAQ
  setThreshold( threshold );
  setOffset( offset );


  
  return StatusCode::SUCCESS;
} 

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloDisplay::finalize() {

  debug() << "==> Finalize" << endmsg;

  return Calo2Dview::finalize();  // must be called after all other actions
}

//=============================================================================
