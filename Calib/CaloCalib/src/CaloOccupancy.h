// $Id: CaloOccupancy.h,v 0.0 2014-11-28 09:42:03 jmarchan Exp $
#ifndef CALOEFFICIENCY_H
#define CALOEFFICIENCY_H 1

// Include files
// from Gaudi
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloCellIDAsProperty.h" // MUST BE FIRST
//
#include "GaudiAlg/GaudiTupleAlg.h"
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDAQ/ICaloL0DataProvider.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/L0DUReport.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"
#include "AIDA/IProfile2D.h"
#include "AIDA/IHistogram2D.h"
#include "CaloUtils/Calo2Dview.h"
#include "CaloKernel/CaloVector.h"
// from Calo
#include "CaloDet/DeCalorimeter.h"

#include <string>


/** @class CaloOccupancy CaloOccupancy.h
 *
 *
 *  @author Jean-Francois Marchand  ////  jean-francois.marchand@cern.ch
 *  @date   2014-11-28
 */

class CaloOccupancy : public Calo2Dview {
 public:

  /// Standard constructor
  CaloOccupancy( const std::string& name, ISvcLocator* pSvcLocator );


  virtual ~CaloOccupancy( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization


 protected:

 private:

  int findPattern(const CaloVector<LHCb::CaloAdc>& adcs);

  std::string m_readoutTool;
  std::map<std::string,int> m_slots;

  DeCalorimeter* m_det;

  IEventTimeDecoder* m_odin;

  ICaloDataProvider*  m_daq;


  int m_currPat;
  unsigned int m_cellCheck;
  bool m_statusOnTES;
  bool m_splitParity;
  bool m_profile2D;
  bool m_ok;
  int m_nPattern;
  bool m_activeCounter;
  int m_timeRange;
  std::vector<int>  m_threshold;


  std::string m_detectorName;

  CaloVector<int> m_cellPattern;
  //
  std::map<std::string  , std::vector<int>  > m_sequence;
  std::string m_timeSlot;

  // predefined histograms to speed-up filling
  AIDA::IProfile2D* hOccADC ;
  AIDA::IProfile1D* hOcc[6] ;
  AIDA::IHistogram1D* hCell ;
  AIDA::IHistogram1D* hTimeInit ;
  AIDA::IHistogram1D* hTimeEnd ;

  unsigned int m_nChannels;
};
#endif
