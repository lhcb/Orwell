// Include files 

#include "AIDA/IHistogram1D.h"
// local
#include "CaloPMSignal.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloPMSignal
//
// 2007-02-08 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloPMSignal )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloPMSignal::CaloPMSignal( const std::string& name,
                    ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
    ,m_k ()
    ,m_evt( 0 )
{

  declareProperty( "Detector"             ,  m_detectorName); 
  declareProperty( "ReadoutTool"          ,  m_readoutTool  = "CaloDataProvider" );
  declareProperty( "TimeSlots"            ,  m_slots);
  declareProperty( "RefreshRate"          ,  m_refresh = 100);
  declareProperty( "Average"              ,  m_average = true);
  declareProperty( "SplitDrivers"         ,  m_split   = true); // only for Prs & Spd


  m_slots.push_back("Prev1");
  m_slots.push_back("T0");
  m_slots.push_back("Next1");  

  // set default detectorName
  int index = name.find_last_of(".") +1 ; 
  m_detectorName = name.substr( index , 4 );
  if ( name.substr(index,3) == "Prs" ) m_detectorName = "Prs";  
  if ( name.substr(index,3) == "Spd" ) m_detectorName = "Spd";

  m_driver[ 24]=std::make_pair(  0,  1);
  m_driver[ 25]=std::make_pair(  0,  2);
  m_driver[ 16]=std::make_pair(  0,  3);
  m_driver[ 17]=std::make_pair(  0,  4);
  m_driver[  8]=std::make_pair(  0,  5);
  m_driver[  9]=std::make_pair(  0,  6);
  m_driver[  0]=std::make_pair(  0,  7);
  m_driver[  1]=std::make_pair(  0,  8);
  m_driver[ 26]=std::make_pair(  0,  9);
  m_driver[ 27]=std::make_pair(  0, 10);
  m_driver[ 18]=std::make_pair(  0, 11);
  m_driver[ 19]=std::make_pair(  0, 12);
  m_driver[ 10]=std::make_pair(  0, 13);
  m_driver[ 11]=std::make_pair(  0, 14);
  m_driver[  2]=std::make_pair(  0, 15);
  m_driver[  3]=std::make_pair(  0, 16);
  m_driver[ 60]=std::make_pair(  1,  1);
  m_driver[ 61]=std::make_pair(  1,  2);
  m_driver[ 52]=std::make_pair(  1,  3);
  m_driver[ 53]=std::make_pair(  1,  4);
  m_driver[ 44]=std::make_pair(  1,  5);
  m_driver[ 45]=std::make_pair(  1,  6);
  m_driver[ 36]=std::make_pair(  1,  7);
  m_driver[ 37]=std::make_pair(  1,  8);
  m_driver[ 62]=std::make_pair(  1,  9);
  m_driver[ 63]=std::make_pair(  1, 10);
  m_driver[ 54]=std::make_pair(  1, 11);
  m_driver[ 55]=std::make_pair(  1, 12);
  m_driver[ 46]=std::make_pair(  1, 13);
  m_driver[ 47]=std::make_pair(  1, 14);
  m_driver[ 38]=std::make_pair(  1, 15);
  m_driver[ 39]=std::make_pair(  1, 16);
  m_driver[ 28]=std::make_pair(  2,  1);
  m_driver[ 29]=std::make_pair(  2,  2);
  m_driver[ 20]=std::make_pair(  2,  3);
  m_driver[ 21]=std::make_pair(  2,  4);
  m_driver[ 12]=std::make_pair(  2,  5);
  m_driver[ 13]=std::make_pair(  2,  6);
  m_driver[  4]=std::make_pair(  2,  7);
  m_driver[  5]=std::make_pair(  2,  8);
  m_driver[ 30]=std::make_pair(  2,  9);
  m_driver[ 31]=std::make_pair(  2, 10);
  m_driver[ 22]=std::make_pair(  2, 11);
  m_driver[ 23]=std::make_pair(  2, 12);
  m_driver[ 14]=std::make_pair(  2, 13);
  m_driver[ 15]=std::make_pair(  2, 14);
  m_driver[  6]=std::make_pair(  2, 15);
  m_driver[  7]=std::make_pair(  2, 16);
  m_driver[ 56]=std::make_pair(  3,  1);
  m_driver[ 57]=std::make_pair(  3,  2);
  m_driver[ 48]=std::make_pair(  3,  3);
  m_driver[ 49]=std::make_pair(  3,  4);
  m_driver[ 40]=std::make_pair(  3,  5);
  m_driver[ 41]=std::make_pair(  3,  6);
  m_driver[ 32]=std::make_pair(  3,  7);
  m_driver[ 33]=std::make_pair(  3,  8);
  m_driver[ 58]=std::make_pair(  3,  9);
  m_driver[ 59]=std::make_pair(  3, 10);
  m_driver[ 50]=std::make_pair(  3, 11);
  m_driver[ 51]=std::make_pair(  3, 12);
  m_driver[ 42]=std::make_pair(  3, 13);
  m_driver[ 43]=std::make_pair(  3, 14);
  m_driver[ 34]=std::make_pair(  3, 15);
  m_driver[ 35]=std::make_pair(  3, 16);

  
  setHistoDir( name );
 }
//=============================================================================
// Destructor
//=============================================================================
CaloPMSignal::~CaloPMSignal() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloPMSignal::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize " << name() << " Det = "<< m_detectorName << endmsg;

  // Get Detector Element
  if ( "Ecal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  } else if ( "Hcal" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
  } else if ( "Prs" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Prs );
  } else if ( "Spd" == m_detectorName ) {
    m_calo     = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd ); 
  } else {
    error() << "Unknown detector name " << m_detectorName << endmsg;
    return StatusCode::FAILURE;
  }

  m_cells  = m_calo->numberOfCells()+ m_calo->numberOfPins();
  m_cards  = m_calo->numberOfCards();
  debug() << "#Cards " << m_cards << " #Cells " << m_cells << endmsg;

  // get DAQ tools
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    m_daqs[*islot] = tool<ICaloDataProvider>( m_readoutTool , m_detectorName + "ReadoutTool" + *islot , this );
    if(*islot != "T0")m_daqs[*islot]->_setProperty("RootInTES", *islot +"/").ignore();
    info() << " -> DAQ tool for additional slot " << *islot <<  " instantiated as " 
           << m_detectorName + "ReadoutTool" + *islot << endmsg;
    info()<< "   ---> RawEvent will be loaded from  root in TES: '"<< *islot << "'" << endmsg;
    // check roots in TES are uniquely defined
    for(std::vector<std::string>::iterator jslot = m_slots.begin(); jslot != islot ;  ++jslot){
      if( *jslot == *islot ){
        error() << "the  RootInTES " << *islot << " is  defined twice" << endmsg;
        return StatusCode::FAILURE;
      }
    }
    if( !bookHistos ( *islot ).isSuccess() ) return StatusCode::FAILURE ;
  }
  // init  
  m_evt = 0 ;  
  return StatusCode::SUCCESS;
}


//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloPMSignal::execute() {

  // Get Raw + Calo Bank
  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    m_ok[*islot]= m_daqs[*islot]->ok(); 
  }

  // Reset histo every m_evt events
  if( m_refresh == m_evt ){
    debug() << "Refreshing histos " << m_evt << "/" << m_refresh << endmsg;
    m_evt = 0;
    for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){        
      for( unsigned int icard = 0 ; icard < m_cards ; icard++){
        int tell1 = m_calo->cardToTell1( icard );
        int pm    = m_calo->cardParam( icard ).number();
        std::string  lun = "TELL1_" + Gaudi::Utils::toString( tell1 ) + "/PM_" + Gaudi::Utils::toString( pm ) + "/" + *islot ;
        if( histoExists((HistoID) lun ) )histo1D((HistoID) lun )->reset();
      }
    }
  }
  m_evt++;
  debug() << "Filling  histos" << m_evt << endmsg;


  for(std::vector<std::string>::iterator islot = m_slots.begin(); islot != m_slots.end() ;  ++islot){
    if( !m_ok[*islot] ) continue;
    for( unsigned int icell = 0 ; icell < m_cells ; icell++){
      LHCb::CaloCellID id = m_calo->cellIdByIndex( icell );    
      int adc = 0;
      if(m_ok[*islot]) adc = m_daqs[*islot]->adc( id );
      int pm = m_calo->cardNumber( id );
      int tell1 = m_calo->cardToTell1( pm );
      std::string lun;
      int pos = m_calo->cardRow( id ) * 8 + m_calo->cardColumn( id );
      int bin = 0;
      if(m_split && ( m_detectorName == "Prs" || m_detectorName == "Spd" ) ){        
        int idriv = m_driver[pos].first;
        lun = "TELL1_" + Gaudi::Utils::toString(tell1) + "/PM_" + Gaudi::Utils::toString( pm ) + "/Driver_" 
          + Gaudi::Utils::toString(idriv) + "/" + (*islot) ;
        bin = m_driver[pos].second-1;
      }else{
        lun = "TELL1_" + Gaudi::Utils::toString(tell1) + "/PM_" + Gaudi::Utils::toString( pm ) + "/" + *islot;
        bin = pos;
      }
      
      //
      if( m_average ){
        AIDA::IHistogram1D* histo = histo1D(HistoID( lun ));
        // check
        double avem = histo->binHeight( bin );
        double delta = ( (double) adc - avem );
        delta /= (double) m_evt;
        fill(  histo1D(HistoID( lun )), bin , delta  );
        /*
        if( tell1 == 0 && pm == 0 && *islot == "T0" ){
          verbose() << m_evt<< " " << id << " adc = " << adc 
                  << " <adc>(" <<m_evt-1<<") = " << avem
                  << " -->  delta = " <<delta << " --> "
                  << " <adc>("<<m_evt<<") = " <<   histo->binHeight( pos )
                  << endmsg;
        }
        */
      }else{
        fill(  histo1D(HistoID( lun )), bin , (double) adc  );
      }      
    } 
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloPMSignal::finalize() {
  

  debug() << "==> Finalize" << endmsg;
  info() << "Produced " << m_k[1] << " histogram(s)   " << endmsg;

  return GaudiHistoAlg::finalize();  // must be called after all other actions
}

//=============================================================================
StatusCode  CaloPMSignal::bookHistos(std::string slot){
  debug()<<  "Booking histograms for time slot " << slot << endmsg;
  m_k[0] = 0;

  int level = msgLevel();  
  //setOutputLevel(MSG::FATAL);
  setProperty("OutputLevel", MSG::FATAL ).ignore();
  for( unsigned int icard = 0 ; icard < m_cards ; icard++){
    int tell1 = m_calo->cardToTell1( icard );
    int pm    = m_calo->cardParam( icard ).number();

    if(m_split && ( m_detectorName == "Prs" || m_detectorName == "Spd" ) ){
      for ( unsigned int idriv = 0 ; idriv < 4 ; idriv++){
        std::string lun = "TELL1_" + Gaudi::Utils::toString(tell1) + "/PM_" + Gaudi::Utils::toString( pm ) + "/Driver_" 
          + Gaudi::Utils::toString(idriv) + "/" + slot ;
        std::string tit = m_detectorName + " signal for PMT = " + Gaudi::Utils::toString( pm ) + " LED driver = " 
          + Gaudi::Utils::toString(idriv) + " (TELL1 sourceID = " + Gaudi::Utils::toString(tell1) + ") in time slot " + slot;
        CardParam cCard =  m_calo->cardParam( icard );
        unsigned int size = cCard.ids().size()/4;
        book1D( lun , tit , 0. , (double) size , size );
        m_k[0]++;
        m_k[1]++;
      }
    }
    else{
      std::string lun = "TELL1_" + Gaudi::Utils::toString(tell1) + "/PM_" + Gaudi::Utils::toString( pm ) + slot ;
      std::string tit = m_detectorName + " signal for PMT = " + Gaudi::Utils::toString( pm )
        + " (TELL1 sourceID = " + Gaudi::Utils::toString(tell1) + ") in time slot " + slot;   
      CardParam cCard = m_calo->cardParam( icard );
      unsigned int size = cCard.ids().size()-1;
      book1D( lun , tit , 0. , (double) size , size );
      m_k[0]++;
      m_k[1]++;
    }
  }
  setProperty("OutputLevel", level ).ignore();
  //setOutputLevel( level ); // reset outputlevel

  debug() << "Booked " << m_k[0] << " PMT histogram(s)  for time slot  " << slot << endmsg;

  return StatusCode::SUCCESS;
}
