// $Id: CaloChannelHisto.h,v 1.1 2009-10-27 16:49:40 odescham Exp $
#ifndef CALOCHANNELHISTO_H 
#define CALOCHANNELHISTO_H 1

// Include files
#include "CaloKernel/CaloVector.h"
#include "AIDA/IHistogram1D.h"

/** @class CaloChannelHisto CaloChannelHisto.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-10-12
 */
class CaloChannelHisto {
public: 
  /// constructor
  CaloChannelHisto(LHCb::CaloCellID id = LHCb::CaloCellID(), bool booked = false){
    m_booked=booked;
    m_id = id;
    m_histos.clear();
  }
  // destructor
  ~CaloChannelHisto() {} ;

  enum Type{
    Pedestal = 0,
    Signal   = 1,
    Ratio    = 2,
    All      = 3
  };

  void setBooked(bool booked){m_booked = booked;  }
  
  bool                        booked(){return m_booked; };
  const LHCb::CaloCellID&     id()    {return m_id;     };
  inline AIDA::IHistogram1D*  histo(Type type, const std::string& slot="T0", unsigned int index=0) ;
  inline std::vector<AIDA::IHistogram1D* >&  histos(Type type, const std::string& slot="T0") ;
  inline void addHisto(AIDA::IHistogram1D* histo, Type,const std::string& slot) ;


protected:
  typedef std::vector< AIDA::IHistogram1D*>       Vec;
  typedef std::map<const std::string, Vec >       MapV;
  typedef std::map<int, MapV >                    MapMapV;

private:
  MapMapV m_histos;
  bool    m_booked;
  Vec     m_dummyVec;
  LHCb::CaloCellID m_id;
};
#endif // CALOCHANNELHISTO_H


inline  AIDA::IHistogram1D* CaloChannelHisto::histo(CaloChannelHisto::Type type,
                                                         const std::string& slot, unsigned int index) {
  Vec& hs = histos(type,slot);
  if( index < hs.size() )return hs[index];
  return NULL;
}

inline std::vector<AIDA::IHistogram1D*>& CaloChannelHisto::histos(CaloChannelHisto::Type type, 
                                                                        const std::string& slot) {
  MapMapV::iterator it = m_histos.find( type );
  if(it == m_histos.end())return m_dummyVec;
  MapV& t = (*it).second;  
  MapV::iterator itt = t.find( slot );
  if( itt == t.end() )return m_dummyVec;
  return (*itt).second;
}
//-----
inline void CaloChannelHisto::addHisto(AIDA::IHistogram1D* histo,
                                       CaloChannelHisto::Type type, const std::string& slot) {
  //  MapV& m = m_histos[type];
  //Vec&  v = m[slot];
  //v.push_back(histo);
  m_histos[type][slot].push_back(histo);
}

