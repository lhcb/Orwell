#pragma once

#include <math.h>
#include "TH1D.h"
#include "TH2D.h"
#include "TH2F.h"
#include "TFile.h"

#define NHCELLS 1488

extern double stepX_hcal;
extern double stepX_hcal;

int xpos(int modnum); // 0 for side C, 31 for side A
int ypos(int modnum); // 3...28
int prodnum(int side, int ypos); // side==0 : C, else A

bool isHCALcell(int z, int x, int y);
bool zxy(int cellid, int& z, int& x, int& y);
bool zxy_H(int cellid, int& z, int& x, int& y);
int cellid(int z, int x, int y);
int cellid_H(int z, int x, int y);

int iPINofcellID(int cellID);
int cellIDofPIN(int iPIN);

int iHCAL2cellID(int iHCAL);
int cellID2iHCAL(int cellID);

int zxy2iHCAL(int iz, int ix, int iiy);

int iHCAL2iPIN(int iHCAL);
int cellID2iPIN(int cellID);
int zxy2iPIN(int &z, int &x, int &y);
bool iPIN2zxy(int iPIN, int& z, int& x, int& y);

TH2D* creHCALmap(const char* name, const char* title);
TH2F* creHCALmapF(const char* name, const char* title);
bool isHCALmapH(TH2* hmap);
bool isHCALmapO(TObject* ob);

void makevarH(TH2D* h, TProfile* t[32][32][3]);
void makeavrmsH(TH2D* h0, TH2D* h1, TH2D* h2);
void saveHCALallH(const char* fnam, ...);

int FEBwd2in(int wd);
int FEBin2wd(int in);

bool HCALcell2xy(int z, int x, int y, double& xc, double& yc);
bool HCALxy2cell(int& z, int& x, int& y, double xc, double yc);

template<class HIST> void zlim_HCALmapH(HIST* hmap){
  double zmax=-1e23, zmin=1e23;
  for(int iz=1; iz<3; ++iz){
    for(int iy=0; iy<32; ++iy){
      for(int ix=0; ix<32; ++ix){
        if(isHCALcell(iz,ix,iy)){
          double v=getval_HCALmapH(hmap, iz, ix, iy);
          if(v!=0){
            if(zmax<v)zmax=v;
            if(zmin>v)zmin=v;
          }
        }
      }
    }
  }
  hmap->GetZaxis()->SetRangeUser(zmin, zmax);
}

template<class HIST> double getval_HCALmapH(HIST* hmap, int iz, int ix, int iy){
  if(!isHCALcell(iz,ix,iy))return 0;
  if(4==iz)return 0;
  
  int ibinx=2*ix+1; 
  int ibiny=2*iy-5;
  if(2==iz){
    ibinx=ix+17;
    ibiny=iy+11;
  }
  double bval = (double)hmap->GetBinContent(ibinx,ibiny);
  return bval;
}

template<class HIST> double geterr_HCALmapH(HIST* hmap, int iz, int ix, int iy){
  if(!isHCALcell(iz,ix,iy))return 0;
  int ibinx=2*ix+1; 
  int ibiny=2*iy-5;
  if(2==iz){
    ibinx=ix+17;
    ibiny=iy+11;
  }
  double bval = (double)hmap->GetBinError(ibinx,ibiny);
  return bval;
}

template<class HIST> double getsum_HCALmapH(HIST* hmap){
  double sumh=0;
  for(int iz=1; iz<3; ++iz){
    for(int iy=0; iy<32; ++iy){
      for(int ix=0; ix<32; ++ix){
        if(isHCALcell(iz,ix,iy)){
          int ibinx=2*ix+1; 
          int ibiny=2*iy-5;
          if(2==iz){
            ibinx=ix+17;
            ibiny=iy+11;
          }
          double bval = (double)hmap->GetBinContent(ibinx,ibiny);
          sumh+=bval;
        }
      }
    }
  }
  return sumh;
}

template<class HIST> double getsumcentre_HCALmapH(HIST* hmap){
  double sumh=0;
  int iz=2;
  for(int iy=12; iy<=19; ++iy){
    for(int ix=8; ix<=23; ++ix){
      if(isHCALcell(iz,ix,iy)){
        int ibinx=2*ix+1; 
        int ibiny=2*iy-5;
        if(2==iz){
          ibinx=ix+17;
          ibiny=iy+11;
        }
        double bval = (double)hmap->GetBinContent(ibinx,ibiny);
        sumh+=bval;
      }
    }
  }
  return sumh;
}

template<class HIST> double getsumnpe_HCALmapH(HIST* hmap){
  double sumh=0;
  for(int iz=1; iz<3; ++iz){
    for(int iy=0; iy<32; ++iy){
      for(int ix=0; ix<32; ++ix){
        if(isHCALcell(iz,ix,iy)){
          int ibinx=2*ix+1; 
          int ibiny=2*iy-5;
          if(2==iz){
            ibinx=ix+17;
            ibiny=iy+11;
          }
          double bval = (double)hmap->GetBinContent(ibinx,ibiny);
          double berr = (double)hmap->GetBinError(ibinx,ibiny);
          if(berr>0)sumh+=bval*bval/berr/berr;
        }
      }
    }
  }
  return sumh;
}

template<class HIST> void fillHCALmapH(HIST* hmap, int iz, int ix, int iy, double val, double err=0){
  if(!hmap)return;
  if(!isHCALcell(iz,ix,iy))return;
  if(2==iz){
    int ibinx=ix+17;
    int ibiny=iy+11;
    hmap->SetBinContent(ibinx,ibiny,val);
    if(err>0)hmap->SetBinError(ibinx,ibiny,err);
  }else{
    for(int ibinx=2*ix+1; ibinx<=2*ix+2; ++ibinx){
      for(int ibiny=2*iy-5; ibiny<=2*iy-4; ++ibiny){
        hmap->SetBinContent(ibinx,ibiny,val);
        if(err>0)hmap->SetBinError(ibinx,ibiny,err);
      }
    }
  }
  hmap->SetEntries(hmap->GetEntries()+1);
}

template<class HIST> void fillHCALhitmapH(HIST* hmap, int iz, int ix, int iy, double wt){
  if(!isHCALcell(iz,ix,iy))return;
  if(!hmap)return;
  if(2==iz){
    int ibinx=ix+17;
    int ibiny=iy+11;
    double cx=hmap->GetXaxis()->GetBinCenter(ibinx);
    double cy=hmap->GetYaxis()->GetBinCenter(ibiny);
    hmap->Fill(cx,cy,wt);
  }else{
    for(int ibinx=2*ix+1; ibinx<=2*ix+2; ++ibinx){
      for(int ibiny=2*iy-5; ibiny<=2*iy-4; ++ibiny){
        double cx=hmap->GetXaxis()->GetBinCenter(ibinx);
        double cy=hmap->GetYaxis()->GetBinCenter(ibiny);
        hmap->Fill(cx,cy,wt);
      }
    }
  }
}

template<class HIST> TH1D* makeHCALmap_1H(HIST* hmap, double zmin=0, double zmax=0){
  int ix,iy;
  
  if(!hmap)return 0;
  
  double mmax=-1000000, mmin=1000000;
  if(zmin>=zmax){
    //int ixmax=0, iymax=0, ixmin=0, iymin=0;
    for(ix=1; ix<=64; ++ix){
      for(iy=1; iy<=52; ++iy){
        if(ix>=31&&ix<=34 && iy>=25&&iy<=28)continue;
        double g=hmap->GetBinContent(ix,iy);
        if(0!=g){
          if(mmax<g){
            mmax=g;
            //ixmax=ix;
            //iymax=iy;
          }
          if(mmin>g){
            mmin=g;
            //ixmin=ix;
            //iymin=iy;
          }
        }
      }
    }
  }else{
    mmax=zmax;
    mmin=zmin;
  }
  mmax+=0.001*(mmax-mmin);
  
  double lmscale=floor(log10(mmax-mmin));
  double mscale=pow((double)10,lmscale);
  if( ((mmax-mmin)/mscale)<3 )mscale/=10;
  double mb1=floor(mmin/mscale);
  double mb2=ceil(mmax/mscale);
  int mbins=(mb2-mb1)*10;
  double mx1=mb1*mscale;
  double mx2=mb2*mscale;
  
  char name_1[256]; 
  strncpy(name_1, hmap->GetName(), 240);
  strcat(name_1, "_1");
  
  char title_1[256]; 
  strncpy(title_1, hmap->GetTitle(), 240);
  strcat(title_1, "_1");
  
  TH1D* hmap_1=new TH1D(name_1, title_1, mbins,mx1,mx2);
  hmap->GetZaxis()->SetRangeUser(mx1, mx2);
  
  for(ix=1; ix<=64; ++ix){
    for(iy=1; iy<=52; ++iy){
      bool take=false;
      if(ix>=31 && ix<=34 && iy>=25 && iy<=28){
        take=false;
      }else if(ix>=17 && ix <=48 && iy>=13 && iy<=40){
        take=true;
      }else if(1==ix%2 && 1==iy%2){
        take=true;
      }
      if(take){
        hmap_1->Fill(hmap->GetBinContent(ix,iy));
      }
    }
  }
  return hmap_1;
}

template<class HIST> TH1D* makeHCALmap_1C(HIST* hmap, double zmin=0, double zmax=0, int *custom_map=0){
  if(!hmap)return 0;
  
  double mmax=-1e23, mmin=1e23;
  if(zmin>=zmax){
    for(int i=0; i<NHCELLS; ++i){
      int cellid=iHCAL2cellID(i);
      int iz,ix,iy;
      zxy(cellid, iz, ix, iy);
      double v=getval_HCALmapH(hmap, iz, ix, iy);
      bool accpt=true;
      if(custom_map){
        if(0==custom_map[i])accpt=false;
      }
      if(accpt && (v!=0) ){
        if(mmax<v)mmax=v;
        if(mmin>v)mmin=v;
      }
    }
  }else{
    mmax=zmax;
    mmin=zmin;
  }
  mmax+=0.001*(mmax-mmin);
  
  double lmscale=floor(log10(mmax-mmin));
  double mscale=pow((double)10,lmscale);
  if( ((mmax-mmin)/mscale)<3 )mscale/=10;
  double mb1=floor(mmin/mscale);
  double mb2=ceil(mmax/mscale);
  int mbins=(mb2-mb1)*10;
  double mx1=mb1*mscale;
  double mx2=mb2*mscale;
  
  char name_1[256]; 
  strncpy(name_1, hmap->GetName(), 240);
  strcat(name_1, "_1");
  
  char title_1[256]; 
  strncpy(title_1, hmap->GetTitle(), 240);
  strcat(title_1, "_1");
  
  TH1D* hmap_1=new TH1D(name_1, title_1, mbins,mx1,mx2);
  hmap->GetZaxis()->SetRangeUser(mx1, mx2);
  
  for(int i=0; i<NHCELLS; ++i){
    int cellid=iHCAL2cellID(i);
    int iz,ix,iy;
    zxy(cellid, iz, ix, iy);
    double v=getval_HCALmapH(hmap, iz, ix, iy);
    
    bool accpt=true;
    if(custom_map){
      if(0==custom_map[i])accpt=false;
    }
    if(accpt)hmap_1->Fill(v);
  }
  
  return hmap_1;
}

template<class HIST> TH2D* makecorr_HCALmapsH(HIST* h1, HIST* h2, char* namc, char* titc){
  if( (!h1) || (!h2) ) return 0; 
  if(!isHCALmapH(h1) ) return 0;
  if(!isHCALmapH(h2) ) return 0;
  
  int ix, iy;
  double h1min=999999, h1max=-999999, h2min=999999, h2max=-999999;
  for(ix=1; ix<=64; ++ix){
    for(iy=1; iy<=52; ++iy){
      double g1=h1->GetBinContent(ix,iy);
      double g2=h2->GetBinContent(ix,iy);
      if( (0!=g1) && (0!=g2) ){
        if(h1max<g1)h1max=g1;
        if(h1min>g1)h1min=g1;
        if(h2max<g2)h2max=g2;
        if(h2min>g2)h2min=g2;
      }
    }
  }
  
  double lh1scale=floor(log10(h1max-h1min));
  double h1scale=pow((double)10,lh1scale);
  if( ((h1max-h1min)/h1scale)<3 )h1scale/=10;
  double h1b1=floor(h1min/h1scale);
  double h1b2=ceil(h1max/h1scale);
  int nb1=(h1b2-h1b1)*10;
  double h1x1=h1b1*h1scale;
  double h1x2=h1b2*h1scale;
  
  double lh2scale=floor(log10(h2max-h2min));
  double h2scale=pow((double)10,lh2scale);
  if( ((h2max-h2min)/h2scale)<3 )h2scale/=10;
  double h2b1=floor(h2min/h2scale);
  double h2b2=ceil(h2max/h2scale);
  int nb2=(h2b2-h2b1)*10;
  double h2x1=h2b1*h2scale;
  double h2x2=h2b2*h2scale;
  
  TH2D* hcor=new TH2D(namc,titc, nb1,h1x1,h1x2, nb2,h2x1,h2x2);
  
  for(ix=1; ix<=64; ++ix){
    for(iy=1; iy<=52; ++iy){
      bool take=false;
      if(ix>=31 && ix<=34 && iy>=25 && iy<=28){
        take=false;
      }else if(ix>=17 && ix <=48 && iy>=13 && iy<=40){
        take=true;
      }else if(1==ix%2 && 1==iy%2){
        take=true;
      }
      if(take){
        double z1=h1->GetBinContent(ix,iy);
        double z2=h2->GetBinContent(ix,iy);
        hcor->Fill(z1,z2,1);
      }
    }
  }
  return hcor;
}

template <class HIST> void saveHCALdir(const char* fnam, HIST* h[32][32][3], const char* dnam, const char* tnam){
  char nm[256];
  TFile* wfil=new TFile(fnam,"UPDATE");
  
  if(!gDirectory->cd(dnam)){
    gDirectory->mkdir(dnam);
    gDirectory->cd(dnam);
  }
  //gDirectory->pwd();
  
  if(!gDirectory->cd(tnam)){
    gDirectory->mkdir(tnam);
    gDirectory->cd(tnam);
  }
  //gDirectory->pwd();
  
  for(int iz=1; iz<3; ++iz){
    if(1==iz)strcpy(nm,"O");
    else if(2==iz) strcpy(nm,"I");
    gDirectory->mkdir(nm);
    gDirectory->cd(nm);
    //gDirectory->pwd();
    for(int iy=0; iy<32; ++iy){
      Int_t nh=0;
      for(int ix=0; ix<32; ++ix){ if(isHCALcell(iz,ix,iy)) nh++; }
      if(nh>0){
        sprintf(nm,"Y%2.2d",iy);
        gDirectory->mkdir(nm);
        gDirectory->cd(nm);
        //gDirectory->pwd();
        for(int ix=0; ix<32; ++ix){
          if(isHCALcell(iz,ix,iy)){
            h[ix][iy][iz]->Write();
          }
        }
        gDirectory->cd("../");
        //gDirectory->pwd();
      }
    }
    gDirectory->cd("../");
    //gDirectory->pwd();
  }
  
  gDirectory->cd();
  wfil->Close();
}

template <class HIST> void savePINdir(char* fnam, HIST* h[2][32], char* dnam, char* tnam=0){
  //char nm[256];
  TFile* wfil=new TFile(fnam,"UPDATE");
  
  if(!gDirectory->cd(dnam)){
    gDirectory->mkdir(dnam);
    gDirectory->cd(dnam);
  }
  //gDirectory->pwd();
  
  if(tnam){
    if(!gDirectory->cd(tnam)){
      gDirectory->mkdir(tnam);
      gDirectory->cd(tnam);
    }
  }
  //gDirectory->pwd();
  
  for(int is=0; is<2; ++is){
    for(int iy=0; iy<32; ++iy){
      if(isHCALcell(1,1,iy)){
        h[is][iy]->Write();
      }
    }
  }
  
  gDirectory->cd();
  wfil->Close();
}

template<class NUM> TH2D* toHCALmap(NUM* arr, const char* nam, const char* tit){
  TH2D* h=creHCALmap(nam, tit);
  for(int iHCAL=0; iHCAL<NHCELLS; ++iHCAL){
    int cid=iHCAL2cellID(iHCAL);
    int z,x,y;
    zxy_H(cid,z,x,y);
    fillHCALmapH(h, z,x,y, arr[iHCAL]);
  }
  return h;
}

template<class NUM> TH1D* toHCALPIN(NUM* arr, const char* nam, const char* tit){
  TH1D* h=new TH1D(nam,tit,64,0.,64.);
  for(int iPIN=0; iPIN<64; ++iPIN){
    double pinamp=arr[iPIN];
    h->SetBinContent(iPIN+1,pinamp);
  }
  return h;
}
