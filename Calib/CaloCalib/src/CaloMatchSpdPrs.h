// $Id: CaloMatchSpdPrs.h,v 1.1 2008-10-15 16:31:26 odescham Exp $
#ifndef CALOMATCHSPDPRS_H
#define CALOMATCHSPDPRS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "CaloDAQ/ICaloDataProvider.h"

/** @class CaloMatchSpdPrs CaloMatchSpdPrs.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-08-18
 */
class CaloMatchSpdPrs : public GaudiHistoAlg {
public:
  /// Standard constructor
  CaloMatchSpdPrs( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~CaloMatchSpdPrs( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

  double m_max;
  int m_bin;
  int m_prsThreshold;
  ICaloDataProvider* m_spd;
  ICaloDataProvider* m_prs;
  std::string m_readoutTool;
};
#endif // CALOMATCHSPDPRS_H
