/** \mainpage notitle
 *  \anchor orwelldoxygenmain
 *
 * <b>The classes used by Orwell are documented in the following reference manuals:</b>
 *
 * \li \ref onlinedoxygenmain "OnlineSys documentation (LHCb packages for running Online)"
 * \li \ref lhcbdoxygenmain  "LHCbSys documentation (LHCb core packages)"
 * \li \ref gaudidoxygenmain "Gaudi documentation (Framework packages)"
 * \li \ref externaldocs     "Related external libraries"
 *
 * <hr>
 * \htmlinclude new_release.notes
 * <hr>

 * <b>Additional information:</b>
 * \li <a href="../release.notes"><b>Release notes history</b></a><p>

 */
